#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "RyserSylvanM.h"
#include "MatrixUtils.h"
#include "RandomBits.h"


using namespace sylvan;
using std::endl;
using std::vector;
using std::cout;

struct Mat{
	uint32_t ** beg;
	uint32_t* sizes;
	uint32_t n;
	uint32_t ne;   //num edges
};

typedef struct Mat Mat;

Mat* readMat(FILE **fp, uint32_t n){
	Mat* m = (Mat*) malloc(sizeof(Mat));
	uint8_t line1 = 1;
	m->n = n;
	m->beg = (uint32_t**)malloc(sizeof(uint32_t*)*m->n);
	m->sizes = (uint32_t*)calloc(m->n,sizeof(uint32_t));
	memset(m->sizes,0,m->n);
	m->ne = 0;
	char* lineptr = NULL;
	size_t len;
	for (uint32_t i = 0; i<m->n; i++){
		//printf("Reading line %u of %u\n",i,m->n);
		lineptr = NULL;
		if(getline(&lineptr, &len, *fp)<0){
			printf("Expected %d rows but found %d. Exiting..",m->n,i);
			exit(1);
		}
		//printf("Read line is %s\n",lineptr);
		char *pEnd = lineptr;
		while(1){
			char *pEnd2;
			uint32_t target = strtoul(pEnd,&pEnd2,10);
			if((*pEnd) == '\0' || pEnd == pEnd2){
				break;
			}
			pEnd = pEnd2;
			if (m->sizes[i]==0){
				m->beg[i] = (uint32_t*)malloc(sizeof(uint32_t));
			}
			else{
				m->beg[i] = (uint32_t*)realloc(m->beg[i],sizeof(uint32_t)*(m->sizes[i]+1));
			}
			m->sizes[i]++;
			//printf("%u <-> %u and pEnd is %p and lineptr is %p and *pEnd is %d\n",i,target,pEnd,lineptr,*pEnd);
			m->beg[i][m->sizes[i]-1] = target;
			m->ne++;
		}
		/*printf("Recorded cluster is \n");
		for (uint32_t k = 0; k<m->sizes[i]; k++){
			printf("%d ",m->beg[i][k]);
		}
		printf("\n");*/
		free(lineptr);
	}
	printf("Matrix read. Returning..\n");
	return m;
}

Mat* readProcessOutput(char* cmd) {
    FILE *fp;
    if ((fp = popen(cmd, "r")) == NULL) {
        printf("Error opening cnf file to input to addmc_print!\n");
        exit(1);
    }
	char* lineptr = NULL;
	size_t len, bytesRead;
	uint32_t linesRead = 0;
	//printf("Reading 15 lines..\n");
	while((bytesRead = getline(&lineptr, &len, fp))!=-1){
        linesRead++;
		if (linesRead>14){
			printf("Last line read before processing clusters:\n");
			printf("%s",lineptr);
			free(lineptr);
			lineptr = NULL;
			break;
		}
		free(lineptr);
		lineptr = NULL;
	}
	uint32_t numClusters = 0;
	fgetc(fp); // read 1st dummy character
	fscanf(fp,"%u",&numClusters);
	printf("numClusters is %u\n",numClusters);
	if(getline(&lineptr, &len, fp)<0){
		printf("Could not read first line fully. Exiting..");
		exit(1);
	} // read till end of first line
	free(lineptr);
	Mat* m_pointer = readMat(&fp, numClusters);
    if(pclose(fp))  {
        printf("Error while closing process. Command not found or exited with error status. Ignoring..\n");
        //exit(1);
    }
    return m_pointer;
}
int writeCNF(vector<vector<uint32_t>> sMat, char* filename){
	//printf("Writing to %s\n",filename);
	FILE* fp = fopen(filename,"w");
	//printf("Done1\n");
	if (fp == NULL){
		printf("Error opening cnf file for writing!\n");
		exit(1);
	}
	fprintf(fp, "p cnf %zd %zd\n", sMat.size(), sMat.size());
	for (uint32_t i = 0; i<sMat.size();i++){
		//printf("Writing row %d\n",i);
		for (uint32_t j = 0; j<sMat[i].size(); j++){
			fprintf(fp, "%d ", (sMat[i][j]+1));  //variables are 1-indexed
		}
		fprintf(fp, "0\n");
	}
	fclose(fp);
	return(1);
}

void RyserSylvanM::count(){
	Mat* clustering;
	if (ch>=3){
		char *dir = getenv("TMP"); //check if getenv returns null
		if(dir == NULL){
			return;
		}
		string fnam = "rs_cluster_order_addmc"+std::to_string(get_time_id())+".cnf";
		//char fname[] = "rs_cluster_order_addmc.cnf";
		char const* fname = fnam.c_str();
		char *fpath = NULL; 
		fpath = (char*) malloc(strlen(dir) + strlen(fname) + 1);
		strcpy(fpath,dir); 
		strcat(fpath, fname);
		//printf("## %s\n", fpath);
		printf("Writing cnf for obtaining clusters..\n");
		writeCNF(sMat,fpath);
		printf("Cnf written. Running addmc_print..\n");
		/*for(uint32_t i = 0; i<sMat->n;i++){
			printf("%u ",sMat->sizes[i]);
		}*/
		//printf("\n");
		char *dirADDMC = getenv("PERM");
		char const* exe = "/addmc_print ";
		//char options[] = " 1 5 5 4";
		char options[13];
		snprintf(options,13," 1 %d %d %d",ch,cvo,dvo);
		char* cmd = (char*)malloc(strlen(dirADDMC)+strlen(exe)+strlen(fpath)+strlen(options)+1);
		strcpy(cmd,dirADDMC);
		strcat(cmd,exe);
		strcat(cmd,fpath);
		strcat(cmd,options);
		printf("Running command %s\n", cmd);
		// *cmd = "./addmc_print $TMP/rs_cluster_order_addmc.cnf 1 5 5 4";    
		clustering = readProcessOutput(cmd);
	}
	else if (ch==2){ //Linear
		clustering = (Mat*)malloc(sizeof(Mat));
		clustering->n = n;
		clustering->sizes = (uint32_t*)malloc(sizeof(uint32_t)*n);
		clustering->beg = (uint32_t**)malloc(sizeof(uint32_t*)*n);
		for (uint32_t i = 0; i<n; i++){
			clustering->sizes[i] = 1;
			clustering->beg[i] = (uint32_t*)malloc(sizeof(uint32_t));
			clustering->beg[i][0] = i;
		}
	}
	else if (ch == 1){ // Monolithic
		clustering = (Mat*)malloc(sizeof(Mat));
		clustering->n = n;
		clustering->sizes = (uint32_t*)malloc(sizeof(uint32_t)*n);
		clustering->beg = (uint32_t**)malloc(sizeof(uint32_t*)*n);
		clustering->sizes[0] = n;
		clustering->beg[0] = (uint32_t*)malloc(sizeof(uint32_t)*n);
		for (uint32_t i = 0; i<n; i++){
			clustering->beg[0][i] = i;
		}
		for (uint32_t i = 1; i<n; i++){
			clustering->sizes[i] = 0;
			clustering->beg[i] = NULL;
		}
	}
	else { // greedy
		cout<<"Wrong cluster partition option "<<ch<<" . Exiting.."<<endl;
		exit(1);
	}
	// Initialize the Lace framework for <n_workers> workers.
	lace_init(n_workers, deque_size);
	lace_startup(program_stack_size, NULL,NULL);
	LACE_ME;
	sylvan_set_limits(memGB*1000*1000*1000L,3,2);
	sylvan_init_package();
	sylvan_set_granularity(3); 
    // Initialize the BDD module with granularity 1 (cache every operation)
    // A higher granularity (e.g. 6) often results in better performance in practice
    sylvan_init_mtbdd();
	gmp_init();

	printf("Inited packages!\n");
	mpq_t mg, og, zg;
	mpq_inits(mg,og,zg,NULL);
	mpq_set_si(mg,-1,1);
	mpq_set_ui(og,1,1);
	mpq_set_ui(zg,0,1);
	//int n = atoi(argv[1]);
	
	MTBDD m_ = mtbdd_gmp(mg);
	MTBDD o_ = mtbdd_gmp(og);
	MTBDD z_ = mtbdd_gmp(zg);
    Mtbdd m = Mtbdd(m_);
	Mtbdd o = Mtbdd(o_);
	Mtbdd z = Mtbdd(z_);
	// Now we can do some simple stuff using the C++ objects.
      vector<Mtbdd> vars;
    for (uint32_t i =0 ; i<n;i++){
		vars.push_back(Mtbdd::mtbddVar(i));
	}
	
	cout<<"Constructing parity.."<<endl;
	Mtbdd par = Mtbdd::mtbddZero();
	for (uint32_t i =0; i<n;i++){
		par = par.Times(!vars[i]).Plus((!par).Times(vars[i]));
	}
    
	par = par.Ite(m,o);
	cout<<"Constructed parity. Multiplying with clusters.."<<endl;
    size_t maxNodeCount = 0;
    for (uint32_t i = 0; i<clustering->n;i++){
		cout<<"Doing cluster "<<i<<" of size "<<clustering->sizes[i]<<" out of "<< clustering->n <<std::flush;
		vector<uint32_t> clusteringVars;
		for (uint32_t j = 0; j<clustering->sizes[i]; j++){
			uint32_t row = clustering->beg[i][j];
			//cout<<"Doing row "<<row<<" in cluster "<<i<<endl;
			Mtbdd sum = Mtbdd(z);
			cout<<"Row "<<row<<" "<<std::flush;
			for (uint32_t k =0; k<sMat[row].size(); k++){
				sMatT[sMat[row][k]].erase(row);
				clusteringVars.push_back(sMat[row][k]);
				Mtbdd t_ = vars[sMat[row][k]].Ite(o,z);
				//cout<<"Constructed ite"<<endl;
				sum = Mtbdd(gmp_plus(sum.GetMTBDD(),t_.GetMTBDD()));
			}
			//cout<<"Multiplying with existing parity.."<<endl;
			par = Mtbdd(gmp_times(par.GetMTBDD(),sum.GetMTBDD()));
		}
		if (clustering->sizes[i]>0){
			size_t curNodeCount = par.NodeCount();
			//cout<<"CurNodeCOunt="<<curNodeCount<<endl;
			maxNodeCount = maxNodeCount > curNodeCount? maxNodeCount: curNodeCount; 
		}
		
		cout<<endl;
		MTBDD temp = mtbdd_true;
		mtbdd_protect(&temp);
		bool haveVars = false;
		for(uint j = 0; j<clusteringVars.size();j++){
			if (sMatT[clusteringVars[j]].empty()){
				haveVars = true;
				temp = mtbdd_times(temp,vars[clusteringVars[j]].GetMTBDD());
			}
		}
		if(haveVars){
			cout<<"Abstracting out vars.."<<endl;
			par = Mtbdd(gmp_abstract_plus(par.GetMTBDD(), temp));
		}
	}
	cout<<"Constructed prod!."<<endl;
	cout<<"maxNodeCount="<<maxNodeCount<<endl;
	mpq_ptr Rs = (mpq_ptr)(size_t)mtbdd_getvalue(par.GetMTBDD());
	mpq_ptr res;
	mpq_init(res);
	mpq_abs(res,Rs);
	gmp_printf("Total wt is %Qd\n",res);
    // Report statistics (if SYLVAN_STATS is 1 in the configuration)
    sylvan_stats_report(stdout);

    // Following line is used to Quit, freeing memory. But it sometimes leads to "free(): invalid next size (fast):" errors
	// i.e. memory corruption, so we comment it out since anyway we are exiting after that.
    //sylvan_quit();
	lace_exit();
}

int main(int argc, char* argv[]){
	double startTime = cpuTimeTotal();
	double startWallTime = get_wall_time();
	cout<< "Starting RyserSylvanM at ";
	print_wall_time();
	cout<<endl;
	
	if (!(argc==8)){
		cout<<"Usage: RyserSylvanM <n> <s/u/r/f/a'filename'/b'filename'/c/k'filename'/t/d> <ch> <cvo> <dvo> <memGB> <n_workers>"<<endl<<"where n is size of mat,  s if perfect matching should be present u otherwise r for random matrix f for full or 'a'+'filename' to read from file (a concatenated with filename) 'k'+filename to read from konect file, t for upper triangular d for diag+super-diagonal matrix"<<endl;
		cout<<"memGB is size of memory in GB, n_workers is number of parallel tasks (0 for auto-detect, 1 for sequential)."<<endl;
		cout<<endl<<endl<<"Different values of clustering heuristic, cluster var order, diagram var order are given below. Weight format is fixed as unweighted."<<endl;
		cout<< "Cluster partition:\n 1: Monolithic\n 2: Linear\n 3/4: Bucket\n 5/6: Bouquet\n"<<endl;
		cout<< "Cluster Var order:\n 1: APPEARANCE \n 2:DECLARATION\n 3: RANDOM\n 4:MCS\n 5: LEXP\n 6: LEXM\n"<<endl;
		cout<< "Diagram Var order:\n 1: APPEARANCE \n 2:DECLARATION\n 3: RANDOM\n 4:MCS\n 5: LEXP\n 6: LEXM\n"<<endl;
		/*
		string s = string(getenv("PERM")) + string("/addmc_print");
		system(s.c_str());*/
		exit(1);
		
	}
	uint32_t n = stoi(argv[1],NULL);
	std::string f = std::string(argv[2]);
	int32_t ch_ = stoi(argv[3],NULL);
	int32_t cvo_ = stoi(argv[4],NULL);
	int32_t dvo_ = stoi(argv[5],NULL);
	uint32_t memGB_ = stoi(argv[6],NULL);
	uint32_t n_workers_ = stoi(argv[7],NULL);

	cout<<"RyserSylvanM invoked with n = "<<n<<" sat / unsat = " <<f<<endl<<endl;
	RyserSylvanM *HK;
	MatrixParser mp;
	uint32_t nedges;
	vector<vector<uint32_t>> mt;
	uint32_t m = n; 
	if(f[0]=='s'){
		//HK = new HopcroftKarp(n,m,true);
		mp.genJustSAT(n,m);
		mt = mp.getSparse(n,m,nedges);
		vector<unordered_set<uint32_t>> mtt;
		MatrixUtils::transposeSparseToSet(mt,m,mtt);
		HK = new RyserSylvanM(n,mt,mtt,ch_,cvo_,dvo_, memGB_, n_workers_);
	}
	else if (f[0] == 'u'){
		//HK = new HopcroftKarp(n,m,false);
		mp.genJustUNSAT(n,m);
		mt = mp.getSparse(n,m,nedges);
		vector<unordered_set<uint32_t>> mtt;
		MatrixUtils::transposeSparseToSet(mt,m,mtt);
		HK = new RyserSylvanM(n,mt,mtt,ch_,cvo_,dvo_, memGB_, n_workers_);
	}
	else if (f[0] == 'c'){
		std::string::size_type sz;
		mp.genCorrelatedRows(n,m,stod(f.substr(sz+2),NULL),stod(f.substr(1),&sz));
		mt = mp.getSparse(n,m,nedges);
		cout<<"Generated correlated matrix is.. "<<endl;
		MatrixUtils::printMat(mt);		
		vector<unordered_set<uint32_t>> mtt;
		MatrixUtils::transposeSparseToSet(mt,m,mtt);
		HK = new RyserSylvanM(n,mt,mtt,ch_,cvo_,dvo_, memGB_, n_workers_);
	}
	else if (f[0] == 'r'){
		//HK = new HopcroftKarp(n,m,false);
		mp.genRandom(n,m,stod(f.substr(1),NULL));
		mt = mp.getSparse(n,m,nedges);		
		vector<unordered_set<uint32_t>> mtt;
		MatrixUtils::transposeSparseToSet(mt,m,mtt);
		HK = new RyserSylvanM(n,mt,mtt,ch_,cvo_,dvo_, memGB_, n_workers_);
	}
	else if (f[0] == 'f') {
		//HK = new HopcroftKarp(n,m,false);
		mp.genFull(n,m);
		mt = mp.getSparse(n,m,nedges);
		vector<unordered_set<uint32_t>> mtt;
		MatrixUtils::transposeSparseToSet(mt,m,mtt);
		HK = new RyserSylvanM(n,mt,mtt,ch_,cvo_,dvo_, memGB_, n_workers_);
	}
	else if (f[0] == 'a'){
		mp.readDense(f.substr(1));
		mt = mp.getSparse(n,m,nedges);
		vector<unordered_set<uint32_t>> mtt;
		MatrixUtils::transposeSparseToSet(mt,m,mtt);
		HK = new RyserSylvanM(n,mt,mtt,ch_,cvo_,dvo_, memGB_, n_workers_);
	}
	else if (f[0] == 'b'){
		mp.readSparse(f.substr(1));
		mt = mp.getSparse(n,m,nedges);
		vector<unordered_set<uint32_t>> mtt;
		MatrixUtils::transposeSparseToSet(mt,m,mtt);
		HK = new RyserSylvanM(n,mt,mtt,ch_,cvo_,dvo_, memGB_, n_workers_);
	}
	else if (f[0] == 'k'){
		mp.readKonect(f.substr(1));
		cout<<"Read graph"<<endl;
		mp.balanceFull();
		uint32_t dummy1, dummy2;
		//mt = mp.getSparse(dummy1,dummy2,nedges);
		mt = mp.getSparse(n,m,nedges);
		//MatrixUtils::trim(mt,dummy1,dummy2,n);
		//MatrixUtils::printMat(mt);
		vector<unordered_set<uint32_t>> mtt;
		MatrixUtils::transposeSparseToSet(mt,m,mtt);
		HK = new RyserSylvanM(n,mt,mtt,ch_,cvo_,dvo_, memGB_, n_workers_);
	}
	else{
		cout<<"Could not recognize 2nd argument. Exiting.."<<endl;
		exit(1);
	}
	HK->count();
	cout<<endl<<endl<< "RyserSylvanD ended at ";
	print_wall_time();
	cout<<"Time Taken:"<<(cpuTimeTotal()-startTime)<<endl;
	cout<<"WallTime Taken:"<<(get_wall_time()-startWallTime)<<endl;
	return 0;
}