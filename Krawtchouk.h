#ifndef KRAWTCHOUK_H_
#define KRAWTCHOUK_H_

#include <gmpxx.h>
#include <gmp.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

// nomenclature based on https://www.cs.cmu.edu/~venkatg/teaching/codingtheory/notes/notes5a.pdf
using std::cout;
using std::endl;
using std::string;
using std::vector;
class Krawtchouk{
	public:
		Krawtchouk(uint32_t n_, uint32_t l_, uint32_t r_);
		void computeAndPrintMcW(string filename);
	private:
		uint32_t n,l,r;
		vector<mpz_class> values;
		mpz_t total;
		
};

#endif
