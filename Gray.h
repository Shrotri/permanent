#ifndef GRAY_H_
#define GRAY_H_

#include <vector>
#include <iostream>

using std::vector;
class Gray{
	private:
		uint32_t numBits;
		vector<bool> a;
		vector<uint32_t> f;
	public:
		Gray(uint32_t numBits_): numBits(numBits_){
			a = vector<bool>(numBits);
			f = vector<uint32_t>(numBits+1);
			for (int i = 0; i<numBits;i++){
				a[i] = false;
				f[i] = i;
			}
			f[numBits] = numBits;
		}
		
		uint32_t getNextPosition();
};

#endif
