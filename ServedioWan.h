#ifndef SERVEDIOWAN_H_
#define SERVEDIOWAN_H_
 
#include <cassert>
#include <vector>
#include <iostream>
#include <gmp.h>
#include <unordered_set>
#include "Gray.h"
#include "MatrixUtils.h"

using std::cout;
using std::endl;
using std::stoi;
using std::vector;
using std::string;
using std::unordered_set;

class ServedioWan{
	public:
		ServedioWan(uint32_t n_, vector<vector<uint32_t>> sMat_, uint32_t M_): n(n_), sMat(sMat_), M(M_){
			cout<<"ServedioWan initialized!"<<endl;
			MatrixUtils::printMat(sMat);
		}
		uint32_t computeC();
		void computeMRows(uint32_t, vector<int32_t>&, vector<uint32_t>&, vector<uint32_t>&);
		void count();
		//void count2();
		//static void printADD(ADD, string, string);
	private:
		uint32_t n;
		uint32_t M;
		vector<vector<uint32_t>> sMat;
};

#endif
