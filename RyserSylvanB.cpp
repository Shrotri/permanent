#include "RyserSylvanB.h"
#include "MatrixUtils.h"
#include "RandomBits.h"

using namespace sylvan;
using std::endl;
using std::vector;
using std::cout;

void RyserSylvanB::count(){
	// Initialize the Lace framework for <n_workers> workers.
	lace_init(n_workers, deque_size);
	lace_startup(program_stack_size, NULL,NULL);
	LACE_ME;
	sylvan_set_limits(8*1024*1024L,3,2);
	sylvan_init_package();
	sylvan_set_granularity(3); 
    // Initialize the BDD module with granularity 1 (cache every operation)
    // A higher granularity (e.g. 6) often results in better performance in practice
    sylvan_init_mtbdd();
	gmp_init();

	printf("Inited packages!\n");
	mpq_t mg, og, zg;
	mpq_inits(mg,og,zg,NULL);
	mpq_set_si(mg,-1,1);
	mpq_set_ui(og,1,1);
	mpq_set_ui(zg,0,1);
	//int n = atoi(argv[1]);
	
	MTBDD m_ = mtbdd_gmp(mg);
	MTBDD o_ = mtbdd_gmp(og);
	MTBDD z_ = mtbdd_gmp(zg);
    Mtbdd m = Mtbdd(m_);
	Mtbdd o = Mtbdd(o_);
	Mtbdd z = Mtbdd(z_);
	// Now we can do some simple stuff using the C++ objects.
      vector<Mtbdd> vars;
    for (uint32_t i =0 ; i<n;i++){
		vars.push_back(Mtbdd::mtbddVar(i));
	}
	
	cout<<"Constructing parity.."<<endl;
	Mtbdd par = Mtbdd::mtbddZero();
	for (uint32_t i =0; i<n;i++){
		par = par.Times(!vars[i]).Plus((!par).Times(vars[i]));
	}
    
	par = par.Ite(m,o);
	cout<<"Constructed parity. Constructing prod.."<<endl;
    
    
    Mtbdd prod = Mtbdd(o);
	
	for(uint32_t i = 0; i<n; i++){
		Mtbdd r = Mtbdd(z);
		//cout<<"Doing row "<<i<<endl;
		for (uint32_t j =0; j<sMat[i].size(); j++){
			Mtbdd t_ = vars[sMat[i][j]].Ite(o,z);
			//cout<<"Constructed ite"<<endl;
			r = Mtbdd(gmp_plus(r.GetMTBDD(),t_.GetMTBDD()));
		}
		prod = Mtbdd(gmp_times(prod.GetMTBDD(),r.GetMTBDD()));
	}
	cout<<"Constructed prod!."<<endl;
	prod = Mtbdd(gmp_times(prod.GetMTBDD(),par.GetMTBDD()));
	cout<<"Constructed prod* par!"<<endl;
	cout<<"Number of sat asnmts of prod are "<<prod.SatCount(n)<<endl;
	
	MTBDD temp = mtbdd_true;
	mtbdd_protect(&temp);
	for (int i = 0; i<n;i++){
		temp = mtbdd_times(temp,vars[i].GetMTBDD());
	}
	MTBDD sup = mtbdd_support(temp);

	printf("Doing exist abstract..\n");
	MTBDD rs = gmp_abstract_plus(prod.GetMTBDD(), temp);
	mpq_ptr Rs = (mpq_ptr)(size_t)mtbdd_getvalue(rs);
	gmp_printf("Total wt is %Qd\n",Rs);
    // Report statistics (if SYLVAN_STATS is 1 in the configuration)
    sylvan_stats_report(stdout);

    // And quit, freeing memory
    sylvan_quit();
	lace_exit();
}

int main(int argc, char* argv[]){
	cout<< "Starting RyserSylvanB at ";
	print_wall_time();
	cout<<endl;
	
	if (!(argc==3)){
		cout<<"Usage: RyserSylvanB <n> <s/u/r/f/a'filename'/k'filename'/t/d>"<<endl<<"where n is size of mat,  s if perfect matching should be present u otherwise r for random matrix f for full or 'a'+'filename' to read from file (a concatenated with filename) 'k'+filename to read from konect file, t for upper triangular d for diag+super-diagonal matrix"<<endl;
		exit(1);
	}
	uint32_t n = stoi(argv[1],NULL);
	
	std::string f = std::string(argv[2]);
	
	cout<<"RyserSylvanB invoked with n = "<<n<<" sat / unsat = " <<f<<endl<<endl;
	RyserSylvanB *HK;
	MatrixParser mp;
	uint32_t nedges;
	vector<vector<uint32_t>> mt;
	uint32_t m = n; 
	if(f[0]=='s'){
		//HK = new HopcroftKarp(n,m,true);
		mp.genJustSAT(n,m);
		mt = mp.getSparse(n,m,nedges);
		HK = new RyserSylvanB(n,mt);
	}
	else if (f[0] == 'u'){
		//HK = new HopcroftKarp(n,m,false);
		mp.genJustUNSAT(n,m);
		mt = mp.getSparse(n,m,nedges);
		HK = new RyserSylvanB(n,mt);
	}
	else if (f[0] == 'r'){
		//HK = new HopcroftKarp(n,m,false);
		mp.genRandom(n,m,stod(f.substr(1),NULL));
		mt = mp.getSparse(n,m,nedges);		
		HK = new RyserSylvanB(n,mt);
	}
	else if (f[0] == 'f') {
		//HK = new HopcroftKarp(n,m,false);
		mp.genFull(n,m);
		mt = mp.getSparse(n,m,nedges);
		HK = new RyserSylvanB(n,mt);
	}
	else if (f[0] == 'a'){
		mp.readDense(f.substr(1));
		mt = mp.getSparse(n,m,nedges);
		HK = new RyserSylvanB(n,mt);
	}
	else if (f[0] == 'b'){
		mp.readSparse(f.substr(1));
		mt = mp.getSparse(n,m,nedges);
		HK = new RyserSylvanB(n,mt);
	}
	else if (f[0] == 'k'){
		mp.readKonect(f.substr(1));
		cout<<"Read graph"<<endl;
		mp.balanceFull();
		uint32_t dummy1, dummy2;
		//mt = mp.getSparse(dummy1,dummy2,nedges);
		mt = mp.getSparse(n,m,nedges);
		//MatrixUtils::trim(mt,dummy1,dummy2,n);
		//MatrixUtils::printMat(mt);
		HK = new RyserSylvanB(n,mt);
	}
	else{
		cout<<"Could not recognize 2nd argument. Exiting.."<<endl;
		exit(1);
	}
	HK->count();
	cout<<endl<<endl<< "RyserSylvanB ended at ";
	print_wall_time();
	cout<<endl;
	
	return 0;
}