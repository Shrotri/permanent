
//#include <stdlib>
#include "Gray.h"

using std::endl;
using std::cout;
uint32_t Gray::getNextPosition(){
	uint32_t j = f[0];
	f[0] = 0;
	if(j == numBits){
		return (-1);
	}
	f[j] = f[j+1];
	f[j+1] = j+1;
	a[j] = !a[j];
	return (j);
}

/*int main(){
	int n = 6;
	Gray g = Gray(n);
	vector<bool> V = vector<bool>(n);
	while(true){
		for(int i=0;i<n;i++){
			cout<<V[i]<<" ";
		}
		cout<<endl;
		uint32_t pos = g.getNextPosition();
		if (pos == -1){
			break;
		}
		V[pos] = !V[pos];
	}
}*/
