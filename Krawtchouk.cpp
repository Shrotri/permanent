#include "Krawtchouk.h"
#include "Timers.h"

using std::string;
using std::cout;
using std::endl;
using std::ifstream;
using std::stoi;

// nomenclature based on https://www.cs.cmu.edu/~venkatg/teaching/codingtheory/notes/notes5a.pdf
Krawtchouk::Krawtchouk(uint32_t n_, uint32_t l_, uint32_t r_): n(n_), l(l_), r(r_){
	
	values.resize(n+1);
	
	if (l==0){
		for(uint32_t i = 0; i<n+1; i++){
			mpz_set_ui(values[i].get_mpz_t(),1);
		}
		return;
	}
	if (l==1){
		for(uint32_t i = 0; i<n+1; i++){
			mpz_set_si(values[i].get_mpz_t(),n-2*i);
		}
		return;
	}
	vector<mpz_class> coeffs;
	coeffs.resize(l+1);
	cout<<"Calculating coeffs.."<<endl;
	for (uint32_t x=0;x<n+1;x++){
		for (uint32_t j=0;j<l+1;j++){
			if ( (j>x) || (l-j)>(n-x) ){
				mpz_set_ui(coeffs[j].get_mpz_t(),0);
				continue;
			}
			//cout<<"After first if.."<<endl;
			if (mpz_cmp_ui(coeffs[j].get_mpz_t(),0)!=0){
				mpz_mul_ui(coeffs[j].get_mpz_t(),coeffs[j].get_mpz_t(),x*(n-x-l+j+1.0));
				mpz_cdiv_q_ui(coeffs[j].get_mpz_t(),coeffs[j].get_mpz_t(),(x-j)*(n-x+1));
				mpz_add(values[x].get_mpz_t(),values[x].get_mpz_t(),coeffs[j].get_mpz_t());
				continue;
			}
			//cout<<"After second if.."<<endl;
			if ((j>0) && (mpz_cmp_ui(coeffs[j-1].get_mpz_t(),0)!=0)){
				mpz_mul_si(coeffs[j].get_mpz_t(),coeffs[j-1].get_mpz_t(),(-1)*(x-j+1.0)*(l-j+1));
				mpz_cdiv_q_ui(coeffs[j].get_mpz_t(),coeffs[j].get_mpz_t(),((j)*(n-x-l+j)));
				mpz_add(values[x].get_mpz_t(),values[x].get_mpz_t(),coeffs[j].get_mpz_t());
				continue;
			}
			//cout<<"After third if.."<<endl;
			mpz_bin_uiui(coeffs[j].get_mpz_t(),x,j);
			mpz_t temp;
			mpz_init(temp);
			mpz_bin_uiui(temp,n-x,l-j);
			if (j%2 == 1){
				mpz_mul_si(temp,temp,-1);
			}
			mpz_mul(coeffs[j].get_mpz_t(),temp,coeffs[j].get_mpz_t());
			mpz_clear(temp);
			mpz_add(values[x].get_mpz_t(),values[x].get_mpz_t(),coeffs[j].get_mpz_t());
			//cout<<"After last.."<<endl;
		}
	}	
}

void Krawtchouk::computeAndPrintMcW(string filename){
	ifstream inputFile;
	inputFile.open(filename);
	
	if(!inputFile.is_open()){
		cout<<"Error opening file "<<filename<<endl;
		exit(1);
	}
	
	string line;
	bool flag=false;
	int i = 0;
	vector<int64_t> a(0);
	mpz_t total;
	mpz_init(total);
	while(getline(inputFile,line)){
		mpz_class strval;
		if (line[0]=='s'){
			string line1 = line.substr(1);
			uint32_t fir = stoi(line1,NULL);
			getline(inputFile,line);
			uint32_t sec = stoi(line,NULL);
			mpz_ui_pow_ui(strval.get_mpz_t(),2,sec);
			mpz_mul_ui(strval.get_mpz_t(),strval.get_mpz_t(),fir);
		}
		else{
			mpz_set_str(strval.get_mpz_t(),line.c_str(),10);
		}
		gmp_printf("value is: %Zd\n",values[i].get_mpz_t());
		mpz_addmul(total,strval.get_mpz_t(),values[i].get_mpz_t());
		i++;
	}
	mpz_t numCodes;
	mpz_init(numCodes);
	mpz_ui_pow_ui(numCodes,2,r);
	mpz_cdiv_q(total,total,numCodes);
	mpz_clear(numCodes);
	if (i!=n+1){
		cout<<"Weight distribution expected size : "<<n+1<<" found size: "<<i<<endl;
	}
	gmp_printf("Number is: %Zd\n",total);
	mpz_clear(total);
}

int main(int argc, char* argv[]){
	cout<< "Starting Krawtchouk at ";
	print_wall_time();
	cout<<endl;
	
	if (!(argc==5)){
		cout<<"Usage: Krawtchouk <codelength> <target_weight> <rank> <inputfilename>"<<endl;
		exit(1);
	}
	uint32_t n = stoi(argv[1],NULL);
	uint32_t l = stoi(argv[2],NULL);
	uint32_t r = stoi(argv[3],NULL);
	std::string inFile = std::string(argv[4]);
	
	cout<<"Krawtchouk invoked with codelength="<<n<<" target_weight="<<l<<" rank="<<r<<" inFile="<<inFile<<endl<<endl;
	Krawtchouk K(n,l,r);
				
	cout<<endl<< "Krawtchouk created. Reading and processing file.. "<<endl;
	K.computeAndPrintMcW(inFile);
	cout<<endl<<endl<< "Krawtchouk ended at ";
	print_wall_time();
	cout<<endl;
	
	return 0;
}	
