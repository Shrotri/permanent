#ifndef MATRIXUTILS_H_
#define MATRIXUTILS_H_
 
#include <cassert>
#include <vector>
#include <unordered_set>

#include "HopcroftKarp.h"

using std::vector;
using std::cout;
using std::endl;

class MatrixUtils{
	public:
		static uint32_t purge(vector<vector<uint32_t>>&);
		//static void purge(vector<vector<bool>>&);
		static void sortAsc(vector<vector<uint32_t>>&);
		//static void sortAsc(vector<vector<bool>>&);
		static void printMat(vector<vector<bool>>&);
		static void printMat(vector<vector<uint32_t>>&);
		static void writeDense(vector<vector<bool>>&, string);
		static void writeDense(vector<vector<uint32_t>>&, uint32_t, string); //m is number of columns
		static void writeSparse(vector<vector<bool>>&, string);
		static void writeSparse(vector<vector<uint32_t>>&, uint32_t, string); //m is number of columns
		static void trim(vector<vector<uint32_t>>& sMat, uint32_t n, uint32_t m, uint32_t nNew);
		static void transposeSparse(vector<vector<uint32_t>>& sMat,uint32_t m, vector<vector<uint32_t>>& sMatT);
		static void transposeSparseToSet(vector<vector<uint32_t>>& sMat,uint32_t m, vector<unordered_set<uint32_t>>& sMatT);
};
#endif