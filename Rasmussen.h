#ifndef RASMUSSEN_H_
#define RASMUSSEN_H_
 
#include <cassert>
#include <vector>
#include <gmpxx.h>

#include "RandomBits.h"
#include "HopcroftKarp.h"
#include "MatrixUtils.h"

using std::cout;
using std::endl;
using std::stoi;
using std::vector;
using std::string;

class Rasmussen{
	public:
		Rasmussen(uint32_t n_, vector<vector<uint32_t>> sparseMat_): n(n_), sparseMat(sparseMat_), hk(n,n,sparseMat){
			rb.SeedEngine();
			MatrixUtils::purge(sparseMat);
			MatrixUtils::sortAsc(sparseMat);
			mpz_init(total);
		}
		void estimate(uint32_t N);
		void estimateWithElim(uint32_t N);
		void printEstimate();
		
	private:
			
		uint32_t n;
		vector<vector<uint32_t>> sparseMat;
		RandomBits rb;
		unordered_set<uint32_t> sr, sl;
		HopcroftKarp hk;
		mpz_t total;
};
#endif
