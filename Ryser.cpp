#include <gmp.h>
#include <gmpxx.h>

#include "Ryser.h"
#include "Timers.h"
#include "MatrixUtils.h"
#include "MatrixParser.h"

using std::cout;
using std::endl;

uint32_t Ryser::getN(){
	return n;
}

void Ryser::calculatePermanent(){
	mpz_t temp;
	mpz_init(temp);
	mpz_t rowsumprod;
    mpz_init(rowsumprod);
    uint64_t rowsum[n];
    int32_t bitPos = g.getNextPosition();
    
    vector<bool> pos(n);
	
    
    for(int i = 0;i<n;i++){
		rowsum[i]=0;
		pos[i] = false;
	}
	pos[bitPos] = true;
	
	bool minus = true;
    while(true)
    {
        mpz_set_ui(rowsumprod,1);
        
        for (int m = 0; m < n; m++)
        {
            if (pos[bitPos]){
				rowsum[m] = rowsum[m]+mat[m][bitPos];
            }
            else{
				rowsum[m] = rowsum[m]-mat[m][bitPos];
			}
            
            mpz_mul_ui(rowsumprod,rowsumprod,rowsum[m]);    
		}        
        //cout<<minus<<" "<<rowsumprod<<endl;
        if(minus){
			mpz_sub(temp,temp,rowsumprod);
        }
        else{
			mpz_add(temp,temp,rowsumprod);
		}
                
		minus = 1-minus;
		
		bitPos = g.getNextPosition();
		if (bitPos==-1){
			break;
		}
		pos[bitPos] = 1-pos[bitPos];
		
    }    

	mpz_abs(temp,temp);
    gmp_printf("Permanent is %Zd\n",temp);
}


int main(int argc, char* argv[]){
	double startTime = cpuTimeTotal();
	cout<< "Starting Ryser at ";
	print_wall_time();
	cout<<endl;
	
	if (!(argc==3)){
		cout<<"Usage: Ryser <n> <s/u/r/f/a'filename'/k'filename'/t/d>"<<endl<<"where n is size of mat,  s if perfect matching should be present u otherwise r for random matrix f for full or 'a'+'filename' to read from file (a concatenated with filename) 'k'+filename to read from konect file, t for upper triangular d for diag+super-diagonal matrix"<<endl;
		exit(1);
	}
	uint32_t n = stoi(argv[1],NULL);
	std::string f = std::string(argv[2]);
	
	cout<<"Ryser invoked with n = "<<n<<" sat / unsat = " <<f<<endl<<endl;
	Ryser *HK;
	MatrixParser mp;
	uint32_t nedges;
	vector<vector<bool>> mt;
	uint32_t m = n; 
	if(f[0]=='s'){
		//HK = new HopcroftKarp(n,m,true);
		mp.genJustSAT(n,m);
		mt = mp.getDense(n,m,nedges);
		HK = new Ryser(n,mt);
	}
	else if (f[0] == 'u'){
		//HK = new HopcroftKarp(n,m,false);
		mp.genJustUNSAT(n,m);
		mt = mp.getDense(n,m,nedges);
		HK = new Ryser(n,mt);
	}
	else if (f[0] == 'r'){
		//HK = new HopcroftKarp(n,m,false);
		mp.genRandom(n,m,stod(f.substr(1),NULL));
		mt = mp.getDense(n,m,nedges);		
		HK = new Ryser(n,mt);
	}
	else if (f[0] == 'f') {
		//HK = new HopcroftKarp(n,m,false);
		mp.genFull(n,m);
		mt = mp.getDense(n,m,nedges);
		HK = new Ryser(n,mt);
	}
	else if (f[0] == 'a'){
		mp.readDense(f.substr(1));
		mt = mp.getDense(n,m,nedges);
		HK = new Ryser(n,mt);
	}
	else if (f[0] == 'b'){
		mp.readSparse(f.substr(1));
		mt = mp.getDense(n,m,nedges);
		MatrixUtils::printMat(mt);
		HK = new Ryser(n,mt);
	}
	else if (f[0] == 'k'){
		mp.readKonect(f.substr(1));
		cout<<"Read graph"<<endl;
		mp.balanceFull();
		uint32_t dummy1, dummy2;
		//mt = mp.getSparse(dummy1,dummy2,nedges);
		mt = mp.getDense(n,m,nedges);
		//MatrixUtils::trim(mt,dummy1,dummy2,n);
		//MatrixUtils::printMat(mt);
		HK = new Ryser(n,mt);
	}
	else{
		cout<<"Could not recognize 2nd argument. Exiting.."<<endl;
		exit(1);
	}
	cout<<"Calculating permanent for matrix with n = "<<HK->getN()<<endl;
	HK->calculatePermanent();
	cout<<endl<<endl<< "Ryser ended at ";
	print_wall_time();
	cout<<"Time Taken:"<<(cpuTimeTotal()-startTime)<<endl;
	
	return 0;
}
