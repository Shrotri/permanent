#include <random>
#include <algorithm>
#include "MatrixParser.h"

using std::sort;
using std::ofstream;
void MatrixParser::readDense(string filename){
	ifstream inputFile;
	inputFile.open(filename);
	
	if(!inputFile.is_open()){
		cout<<"Error opening file "<<filename<<endl;
		exit(1);
	}
	
	string line;
	if (!getline(inputFile,line)){
		cout<<"Could not read header. Exiting.. "<<endl;
		exit(1);
	}
	std::string::size_type sz;
	numRows = stoi(line.substr(1),&sz,10); //ignore char at beginning of header
	numCols = stoi(line.substr(sz+1),nullptr,10);
	numEdges = 0;
	bool flag=false;
	uint32_t i = 0;
	
	while(getline(inputFile,line)){
		bool edgePresent;
		stringstream sline(line);
		sparseMat.push_back(vector<uint32_t>());
		denseMat.push_back(vector<bool>());
		for(uint32_t j=0;j<numCols;j++){
			sline>>edgePresent;
			if(edgePresent){
				sparseMat[i].push_back(j);
				denseMat[i].push_back(true);
				numEdges++;
			}
			else{
				denseMat[i].push_back(false);
			}
		}
		i++;
	}
	if (i<numRows){
		cout<<"Number of rows: "<<numRows<<" Number of lines: "<<i<<endl;
		exit(1);
	}
	inputFile.close();
}

void MatrixParser::readSparse(string filename){
	ifstream inputFile;
	inputFile.open(filename);
	
	if(!inputFile.is_open()){
		cout<<"Error opening file "<<filename<<endl;
		exit(1);
	}
	
	string line;
	if (!getline(inputFile,line)){
		cout<<"Could not read header. Exiting.. "<<endl;
		exit(1);
	}
	std::string::size_type sz;
	numRows = stoi(line.substr(1),&sz,10); //ignore char at beginning of header
	numCols = stoi(line.substr(sz+1),nullptr,10);
	numEdges = 0;
	bool flag=false;
	uint32_t i = 0;
	
	while(getline(inputFile,line)){
		uint32_t target = -1;
		stringstream sline(line);
		sparseMat.push_back(vector<uint32_t>());
		denseMat.push_back(vector<bool>());
		while (1){
			uint32_t prevTarget = target+1;
			sline>>target;
			if(sline.eof()){
				break;
			}
			sparseMat[i].push_back(target);
			for (uint32_t k = prevTarget; k<target;k++){
				denseMat[i].push_back(false);
			}
			denseMat[i].push_back(true);
			numEdges++;
		}
		while(denseMat[i].size()<numCols){
			denseMat[i].push_back(false);
		}
		i++;
	}
	if (i<numRows){
		cout<<"Number of rows: "<<numRows<<" Number of lines: "<<i<<endl;
		exit(1);
	}
	inputFile.close();
}

void MatrixParser::readKonect(string filename){
	ifstream inputFile;
	inputFile.open(filename);
	
	if(!inputFile.is_open()){
		cout<<"Error opening file "<<filename<<endl;
		exit(1);
	}
	
	string line;
	getline(inputFile,line); // skip first line
	if (!getline(inputFile,line)){
		cout<<"Could not read header. Exiting.. "<<endl;
		exit(1);
	}
	std::string::size_type sz1,sz2;
	numEdges = stoi(line.substr(2),&sz1,10); //ignore char at beginning of header
	numRows = stoi(line.substr(sz1+2),&sz2,10);
	numCols = stoi(line.substr(sz1+sz2+2),nullptr,10);
	bool flag=false;
	cout<<numRows<<" "<<numCols<<" "<<numEdges<<endl;
	for (uint32_t i = 0; i<numRows; i++){
		sparseMat.push_back(vector<uint32_t>());
		denseMat.push_back(vector<bool>(numCols));
	}
	uint32_t i = 0;
	
	while(getline(inputFile,line)){
		uint32_t l,r;
		stringstream sline(line);
		sline>>l;
		sline>>r;
		//cout<<l<<" "<<r<<endl;
		sparseMat[l-1].push_back(r-1);
		denseMat[l-1][r-1]=true;
		i++;
	}
	if (i<numEdges){
		cout<<"Number of rows: "<<numRows<<" Number of lines: "<<i<<endl;
		exit(1);
	}
	for(uint32_t i = 0;i<numRows;i++){
		sort(sparseMat[i].begin(), sparseMat[i].end());
	}
	inputFile.close();
}

void MatrixParser::balanceFull(){
	if (numRows==numCols){
		return;
	}
	else if (numRows<numCols){
		uint32_t numNew = numCols - numRows;
		for(uint32_t i=0;i<numNew;i++){
			sparseMat.push_back(vector<uint32_t>());
			denseMat.push_back(vector<bool>());
			for(uint32_t j = 0;j<numRows+numNew;j++){
				sparseMat[numRows+i].push_back(j);
				denseMat[numRows+i].push_back(true);
				numEdges++;
			}
		}
	}
	else{
		uint32_t numNew = numRows - numCols;
		for(uint32_t i=0;i<numRows;i++){
			for(uint32_t j = 0;j<numNew;j++){
				sparseMat[i].push_back(numCols+j);
				denseMat[i].push_back(true);
				numEdges++;
			}
		}
	}
}

vector<vector<bool>> MatrixParser::getDense(uint32_t &n, uint32_t &m, uint32_t &e){
	n=numRows;
	m=numCols;
	e=numEdges;
	return denseMat;
}

vector<vector<uint32_t>> MatrixParser::getSparse(uint32_t &n, uint32_t &m, uint32_t &e){
	n=numRows;
	m=numCols;
	e=numEdges;
	return sparseMat;
}

void MatrixParser::genFull(uint32_t n, uint32_t m){
	cout<<"Generating full matrix.."<<endl;
	numRows = n;
	numCols = m;
	numEdges = 0;
	for(uint32_t i = 0; i<n;i++){
		sparseMat.push_back(vector<uint32_t>());
		denseMat.push_back(vector<bool>());
		//cout<<"entering first row"<<endl;
		for (uint32_t j =0 ;j<m;j++){
			sparseMat[i].push_back(j);
			denseMat[i].push_back(true);
			numEdges++;
		}
	}
}

void MatrixParser::genJustSAT(uint32_t n, uint32_t m){
	cout<<"Generating JustSAT matrix.."<<endl;
	numRows = n;
	numCols = m;
	numEdges = 0;
	uint32_t add = 0;
	uint32_t i = 0;
	for(i = 0; i<ceil(n/2.0)-add;i++){
		sparseMat.push_back(vector<uint32_t>());
		denseMat.push_back(vector<bool>());
		for (uint32_t j =0 ;j<m;j++){
			sparseMat[i].push_back(j);
			denseMat[i].push_back(true);
			numEdges++;
		}
		//cout<<"entered first row"<<endl;
	}
	for(; i<n;i++){
		sparseMat.push_back(vector<uint32_t>());
		denseMat.push_back(vector<bool>());
		for (uint32_t j =0 ;j<m;j++){
			if (j<m/2){
				sparseMat[i].push_back(j);
				denseMat[i].push_back(true);
				numEdges++;
			}
			else{
				denseMat[i].push_back(false);
			}
		}
		//cout<<"entered first row"<<endl;
	}
}

void MatrixParser::genJustUNSAT(uint32_t n, uint32_t m){
	cout<<"Generating JustUNSAT matrix.."<<endl;
	numRows = n;
	numCols = m;
	numEdges = 0;
	uint32_t add = 1;
	uint32_t i;
	for(i = 0; i<ceil(n/2.0)-add;i++){
		sparseMat.push_back(vector<uint32_t>());
		denseMat.push_back(vector<bool>());
		for (uint32_t j =0 ;j<m;j++){
			sparseMat[i].push_back(j);
			denseMat[i].push_back(true);
			numEdges++;
		}
		//cout<<"entered first row"<<endl;
	}
	for(; i<n;i++){
		sparseMat.push_back(vector<uint32_t>());
		denseMat.push_back(vector<bool>());
		for (uint32_t j =0 ;j<m;j++){
			if (j<m/2){
				sparseMat[i].push_back(j);
				denseMat[i].push_back(true);
				numEdges++;
			}
			else{
				denseMat[i].push_back(false);
			}
		}
		//cout<<"entered first row"<<endl;
	}
}

void MatrixParser::genCorrelatedRows(uint32_t n, uint32_t m,double_t flipProb, double_t startDensity){
	cout<<"Generating correlated matrix with startDensity "<<startDensity<<" flipProb "<<flipProb<<endl;
	numRows = n;
	numCols = m;
	numEdges = 0;
	RandomBits rb;
	rb.SeedEngine2();
	std::uniform_real_distribution<double_t> unif(0, 1);
	denseMat.push_back(vector<bool>());
	for (uint32_t i = 0; i<n; i++){
		if (rb.getRandReal(unif)>1-startDensity){
			denseMat[0].push_back(true);
			numEdges++;
		}
		else{
			denseMat[0].push_back(false);
		}
	}
	for(uint32_t i = 1; i<n;i++){
		denseMat.push_back(vector<bool>(denseMat[0]));
		for (uint32_t j =0 ;j<m;j++){
			if (rb.getRandReal(unif)>1-flipProb){
				if(denseMat[i][j]){
					denseMat[i][j] = false;
				}
				else{
					denseMat[i][j] = true;
					numEdges++;
				}
			}
			else{
				numEdges += denseMat[i][j]? 1 : 0;
			}
		}
	}
	for(uint32_t i = 0; i<n; i++){
		sparseMat.push_back(vector<uint32_t>());
		for (uint32_t j =0; j<m; j++){
			if (denseMat[i][j]){
				sparseMat[i].push_back(j);
			}
		}
	}
	cout<<"Correlated rows matrix generated!"<<endl;
}

void MatrixParser::genRandom(uint32_t n, uint32_t m, double_t p){
	cout<<"Generating Random matrix with probability "<<p<<" .."<<endl;
	numRows = n;
	numCols = m;
	numEdges = 0;
	RandomBits rb;
	rb.SeedEngine2();
	 std::uniform_real_distribution<double_t> unif(0, 1);
	for(uint32_t i = 0; i<n;i++){
		sparseMat.push_back(vector<uint32_t>());
		denseMat.push_back(vector<bool>());
		for (uint32_t j =0 ;j<m;j++){
			if (rb.getRandReal(unif)>1-p){
				sparseMat[i].push_back(j);
				denseMat[i].push_back(true);
				numEdges++;
			}
			else{
				denseMat[i].push_back(false);
			}
		}
	}
	cout<<"Matrix generated"<<endl;
}
