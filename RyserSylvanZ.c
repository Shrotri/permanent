#include <sylvan.h>
#include <stdlib.h>
#include <stdio.h>
#include "sylvan_gmpZ.h"
#include <string.h>

struct Mat{
	uint32_t ** beg;
	uint32_t* sizes;
	uint32_t n;
	uint32_t ne;   //num edges
};

typedef struct Mat Mat;

Mat readMat(char* filename){

	char ch;
	FILE *fp;
	
	fp = fopen(filename, "r"); // read mode
	
	if (fp == NULL)
	{
		perror("Error while opening the file.\n");
		exit(EXIT_FAILURE);
	}
	Mat m;
	uint8_t line1 = 1;
	fgetc(fp); // read 1st dummy character
	fscanf(fp,"%u",&m.n);
	printf("n is %u\n",m.n);
	m.beg = (uint32_t**)malloc(sizeof(uint32_t*)*m.n);
	m.sizes = (uint32_t*)malloc(sizeof(uint32_t)*m.n);
	memset(m.sizes,0,m.n);
	m.ne = 0;
	char* lineptr = NULL;
	size_t len;
	if(getline(&lineptr, &len, fp)<0){
		printf("Could not read first line fully. Exiting..");
		exit(1);
	} // read till end of first line
	free(lineptr);
	for (uint32_t i = 0; i<m.n; i++){
		printf("Reading line %u of %u\n",i,m.n);
		lineptr = NULL;
		if(getline(&lineptr, &len, fp)<0){
			printf("Expected %d rows but found %d. Exiting..",m.n,i);
			exit(1);
		}
		
		char *pEnd = lineptr;
		while(1){
			char *pEnd2;
			uint32_t target = strtoul(pEnd,&pEnd2,10);
			if((*pEnd) == '\0' || pEnd == pEnd2){
				break;
			}
			pEnd = pEnd2;
			if (m.sizes[i]==0){
				m.beg[i] = (uint32_t*)malloc(sizeof(uint32_t));
			}
			else{
				m.beg[i] = (uint32_t*)realloc(m.beg[i],sizeof(uint32_t)*(m.sizes[i]+1));
			}
			m.sizes[i]++;
			//printf("%u <-> %u and pEnd is %p and lineptr is %p and *pEnd is %d\n",i,target,pEnd,lineptr,*pEnd);
			m.beg[i][m.sizes[i]-1] = target;
			m.ne++;
		}
		free(lineptr);
	}
	
	fclose(fp);
	return m;
}

int
main(int argc, char** argv){
	if (argc<2){
		printf("Missing argument filename. Exiting..");
		exit(1);
	}
	Mat sMat = readMat(argv[1]);
	for(uint32_t i = 0; i<sMat.n;i++){
		printf("%u ",sMat.sizes[i]);
	}
	printf("\n");
	
	// Init Lace
	lace_init(1, 0); // auto-detect number of workers, use a 1,000,000 size task queue
	lace_startup(0, NULL, NULL); // auto-detect program stack, do not use a callback for startup

	// Lace is initialized, now set local variables
	LACE_ME;

	sylvan_set_limits(8192*1024*1024L, 4, 1);
	sylvan_init_package();
	sylvan_set_granularity(3); // granularity 3 is decent value for this small problem - 1 means "use cache for every operation"
	sylvan_init_mtbdd();
	gmpz_init();
	
	printf("Inited packages!\n");
	mpz_t mg, og, zg;
	mpz_inits(mg,og,zg,NULL);
	mpz_set_si(mg,-1);
	mpz_set_ui(og,1);
	mpz_set_ui(zg,0);
	//int n = atoi(argv[1]);
	MTBDD* vars=(MTBDD*)malloc(sizeof(MTBDD)*sMat.n);
	
	MTBDD m = mtbdd_gmpz(mg);
	MTBDD o = mtbdd_gmpz(og);
	MTBDD z = mtbdd_gmpz(zg);
	
	mtbdd_protect(&m);
	mtbdd_protect(&o);
	mtbdd_protect(&z);
	printf("Created mpqs! Constructing parity..\n");
	//MTBDD cube;
	MTBDD par = mtbdd_false;
	mtbdd_protect(&par);
	for (uint32_t i = 0 ; i<sMat.n; i++){
		vars[i] = mtbdd_ithvar(i);
		mtbdd_protect(&vars[i]);
		par = mtbdd_plus(mtbdd_times(par,mtbdd_comp(vars[i])),mtbdd_times(mtbdd_comp(par),vars[i]));
	}
	par = mtbdd_ite(par,m,o);
	printf("Parity constructed! Constructing intermediate ite's\n");
	MTBDD* r=(MTBDD*)malloc(sizeof(MTBDD)*sMat.n);
	for (uint32_t i = 0; i<sMat.n;i++){
		r[i] = mtbdd_ite(vars[i],o,z);
		mtbdd_protect(&r[i]);
	}
	printf("Constructing prod..\n");
	MTBDD prod = mtbdd_gmpz(og);
	mtbdd_protect(&prod);	
	prod = gmpz_times(prod,par);
	MTBDD sum = mtbdd_gmpz(zg);
	mtbdd_protect(&sum);
	for (uint32_t i = 0; i<sMat.n;i++){
		for (uint32_t j = 0; j<sMat.sizes[i]; j++){
			sum = gmpz_plus(sum,r[sMat.beg[i][j]]);
		}
		prod = gmpz_times(prod, sum);
		sum = mtbdd_gmpz(zg);
		//mtbdd_unprotect(&sum);
	}
	//for (uint32_t i = 0; i<sMat.n;i++){
		//mtbdd_unprotect(&r[i]);
	//}
	//free(r);

	//printf("Constructing prod*parity..\n");
	
	printf("Constructed prod*parity..\n");
	//mtbdd_unprotect(&par);

	
	MTBDD temp = mtbdd_true;
	mtbdd_protect(&temp);
	for (int i = 0; i<sMat.n;i++){
		temp = mtbdd_times(temp,vars[i]);
	}
	MTBDD sup = mtbdd_support(temp);
	
	/*
	while(1){
		printf("%lu ",tpsup);
		tpsup = mtbdd_gethigh(tpsup);
		if (mtbdd_isleaf(tpsup)) break;
	}
	printf("\nSupport vars are..\n");
	*/
	printf("Doing exist abstract..\n");
	MTBDD rs = gmpz_abstract_plus(prod, temp);
	mpz_ptr Rs = (mpz_ptr)(size_t)mtbdd_getvalue(rs);
	gmp_printf("Total wt is %Zd\n",Rs);
	sylvan_quit();
	lace_exit();
}
