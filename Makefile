all: Gray.o RandomBits.o MatrixParser.o MatrixParser_main.o MatrixUtils.o HopcroftKarp.o HopcroftKarp_main.o HopcroftKarp_main2.o Rasmussen.o Ryser.o RyserSylvanD.o RyserSylvanM.o ServedioWan.o
	#RyserADD.o RyserSylvanG.o RyserSylvanZ.o RyserSylvan.o RyserSylvanB.o 
	g++ -std=c++11 -O3 HopcroftKarp_main.o MatrixParser.o HopcroftKarp.o MatrixUtils.o RandomBits.o -o HopcroftKarp
	g++ -std=c++11 -O3 HopcroftKarp_main2.o MatrixParser.o HopcroftKarp.o MatrixUtils.o RandomBits.o -o HopcroftKarp2
	g++ -std=c++11 -O3 RandomBits.o Gray.o Ryser.o MatrixParser.o MatrixUtils.o HopcroftKarp.o -o Ryser -lgmp
	g++ -std=c++11 -O3 RandomBits.o Gray.o ServedioWan.o MatrixParser.o MatrixUtils.o HopcroftKarp.o -o ServedioWan -lgmp
	g++ -std=c++11 -O3 MatrixParser_main.o MatrixParser.o MatrixUtils.o RandomBits.o HopcroftKarp.o -o MatrixParser
	g++ -std=c++11 -O3 Rasmussen.o HopcroftKarp.o MatrixParser.o MatrixUtils.o RandomBits.o -o Rasmussen -lgmp
	#g++ -std=c++11 -O3 RyserADD.o MatrixParser.o MatrixUtils.o RandomBits.o HopcroftKarp.o -o RyserADD -lcudd
	#g++ -std=c++11 RyserSylvan.o RandomBits.o MatrixParser.o -o RyserSylvan -lpthread -llace -lsylvan -lpthread
	#g++ -std=c++11 RyserSylvanB.o RandomBits.o MatrixParser.o -o RyserSylvanB -lpthread -llace -lsylvan -lpthread -lgmp
	g++ -std=c++11 RyserSylvanD.o RandomBits.o MatrixParser.o MatrixUtils.o HopcroftKarp.o -o RyserSylvanD -Wl,-rpath,$(SYLVAN_LIBRARIES) -L $(SYLVAN_LIBRARIES) -lpthread -lsylvan -lpthread -lgmp
	g++ -std=c++11 RyserSylvanM.o RandomBits.o MatrixParser.o MatrixUtils.o HopcroftKarp.o -o RyserSylvanM -Wl,-rpath,$(SYLVAN_LIBRARIES) -L $(SYLVAN_LIBRARIES) -lpthread -lsylvan -lpthread -lgmp
	#gcc -O3 RyserSylvan.c -o CRyserSylvan -lpthread -llace -lsylvan -lpthread -lgmp
	#gcc -O3 -o RyserSylvanG RyserSylvanG.o -lpthread -llace -lsylvan -lpthread -lgmp
	#gcc -O3 -o RyserSylvanZ RyserSylvanZ.o -Wl,-rpath,$(SYLVANZ_LIBRARIES) -L $(SYLVANZ_LIBRARIES) -lpthread -llace -lsylvan -lpthread -lgmp

HK: HopcroftKarp_main.o MatrixParser.o HopcroftKarp.o MatrixUtils.o RandomBits.o
	g++ -std=c++11 -O3 HopcroftKarp_main.o MatrixParser.o HopcroftKarp.o MatrixUtils.o RandomBits.o -o HopcroftKarp
	
Gray.o: Gray.cpp Gray.h
	g++ -std=c++11 -O3 -c Gray.cpp

RandomBits.o: RandomBits.cpp RandomBits.h
	g++ -std=c++11 -O3 -c RandomBits.cpp

Ryser.o: Ryser.cpp Ryser.h
	g++ -std=c++11 -O3 -c Ryser.cpp

ServedioWan.o: ServedioWan.cpp ServedioWan.h
	g++ -std=c++11 -O3 -c ServedioWan.cpp

HopcroftKarp.o: HopcroftKarp.cpp HopcroftKarp.h
	g++ -std=c++11 -O3 -c HopcroftKarp.cpp

HopcroftKarp_main.o: HopcroftKarp_main.cpp HopcroftKarp.cpp HopcroftKarp.h
	g++ -std=c++11 -O3 -c HopcroftKarp_main.cpp

HopcroftKarp_main2.o: HopcroftKarp_main2.cpp HopcroftKarp.cpp HopcroftKarp.h
	g++ -std=c++11 -O3 -c HopcroftKarp_main2.cpp

MatrixParser.o: MatrixParser.cpp MatrixParser.h
	g++ -std=c++11 -O3 -c MatrixParser.cpp

MatrixParser_main.o: MatrixParser_main.cpp MatrixParser.cpp MatrixParser.h
	g++ -std=c++11 -O3 -c MatrixParser_main.cpp

Rasmussen.o: Rasmussen.cpp Rasmussen.h
	g++ -std=c++11 -O3 -c Rasmussen.cpp

MatrixUtils.o: MatrixUtils.cpp MatrixUtils.h HopcroftKarp.o
	g++ -std=c++11 -O3 -c MatrixUtils.cpp -o MatrixUtils.o
	
RyserADD.o: RyserADD.cpp RyserADD.h
	g++ -std=c++11 -O3 -c RyserADD.cpp

RyserSylvan.o: RyserSylvan.cpp RyserSylvan.h
	g++ -std=c++11 -c RyserSylvan.cpp -o RyserSylvan.o

RyserSylvanB.o: RyserSylvanB.cpp RyserSylvanB.h
	g++ -std=c++11 -c RyserSylvanB.cpp -o RyserSylvanB.o

RyserSylvanD.o: RyserSylvanD.cpp RyserSylvanD.h
	g++ -std=c++11 -I $(SYLVAN_INCLUDE) -c RyserSylvanD.cpp -o RyserSylvanD.o

RyserSylvanM.o: RyserSylvanM.cpp RyserSylvanM.h
	g++ -std=c++11 -I $(SYLVAN_INCLUDE) -c RyserSylvanM.cpp -o RyserSylvanM.o

RyserSylvanG.o: RyserSylvanG.c
	gcc -O3 -c RyserSylvanG.c -o RyserSylvanG.o

clean:
	rm *.o
