#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>
#include <set>
#include "ServedioWan.h"
#include "MatrixUtils.h"
#include "RandomBits.h"
#include <gmpxx.h>

using std::endl;
using std::vector;
using std::cout;

uint32_t ServedioWan::computeC(){
	uint32_t C =0;
	for(uint32_t i = 0; i<n; i++){
		for (uint32_t v : sMat[i]){
			C ++;
		}
	}
	return C/n;
}

void ServedioWan::computeMRows(uint32_t M, vector<int32_t> &b, vector<uint32_t>& mRows, vector<uint32_t>& mRowsC){
	vector<std::pair<uint32_t,uint32_t>> sizes;
	for (uint32_t i = 0; i<n; i++){
		sizes.push_back(std::pair<uint32_t,uint32_t>(sMat[i].size(),i));
	}
	std::sort(sizes.begin(),sizes.end());
	for (uint32_t i = M; i<n; i++){
		b[sizes[i].second] = -sMat[sizes[i].second].size();
	}
	
	for (uint32_t i = 0; i<M; i++){
		mRows.push_back(sizes[i].second);
	}
	for(uint32_t i = M; i<n; i++){
		mRowsC.push_back(sizes[i].second);
	}
}

void ServedioWan::count(){
	vector<int32_t> b(n,0);
	uint32_t C = computeC();
	if (M <= 0 ) M = n/(8.0*C);  //this is the 'm' in the paper which is set to n/8C to get the required bounds
	vector<uint32_t> mRows, mRowsC;
	computeMRows(M,b,mRows,mRowsC);
	std::set<uint32_t> T; // holds the set of column indices for which at least one of the mrows have a 1 at
	std::set<uint32_t> Tc; // holds the complement of T i.e.  [n] / T
	for (uint32_t i = 0; i < n; ++i)  	Tc.insert(Tc.end(), i);
	for (uint32_t i : mRows){
		for (uint32_t j=0; j<sMat[i].size(); j++){
			T.insert(sMat[i][j]);
			Tc.erase(sMat[i][j]);
		}
	}
	//find b that minimizes number of non zero subsets S' of T in mRows
	cout<<"find b that minimizes number of non zero subsets S' of T in mRows"<<endl;
	mpz_t minNumNZSets, numNZSets;
	mpz_init(minNumNZSets);   //since we are looking for minimum, minNumNZsets should have been set to max value. Since it is mpz, we instead use the 'first' boolean variable below
	mpz_init(numNZSets);
	vector<int32_t> minBM(M,0); //values of first m rows in b that minimizes NZ sets. 
	bool first = true;
	while(1){
		//compute NumNZSets for current config
		//cout<<"compute NumNZSets for current config "<<T.size()<<" "<<Tc.size()<<" "<<mRows.size()<<" "<<mRowsC.size()<<endl; 
		mpz_set_ui(numNZSets,0);
		Gray g(T.size());
		int32_t bitPos = g.getNextPosition();
		vector<bool> pos(T.size(),false);
		pos[bitPos] = true;
		int32_t rowsum[M];
		bool existsZero = false;
		for (uint32_t i = 0; i<M; i++){
			rowsum[i] = b[mRows[i]];
			if (rowsum[i]==0){
				existsZero = true;
			}
		}
		if (!existsZero) mpz_add_ui(numNZSets,numNZSets,1);
		while(true)
    	{
			//cout<<"computing MatIndex .."<<endl;
			std::set<uint32_t>::iterator setIt = T.begin();
			for(int i = 0; i < bitPos; i++)	setIt++;
			
			uint32_t matIndex = *setIt;
			//cout<<"MatIndex is "<<matIndex<<endl;
			existsZero = false;
			for (uint32_t i = 0; i < M; i++)
			{
				if (pos[bitPos]){
					if(std::find(sMat[mRows[i]].begin(), sMat[mRows[i]].end(), matIndex) != sMat[mRows[i]].end()){
						rowsum[i] += 1;
					}
					if (rowsum[i] == 0){
						existsZero = true;
					}
				}
				else{
					if(std::find(sMat[mRows[i]].begin(), sMat[mRows[i]].end(), matIndex) != sMat[mRows[i]].end()){
						rowsum[i] -= 1;
					}
					if (rowsum[i] == 0){
						existsZero = true;
					}
				}
			}
			if (!existsZero){
				mpz_add_ui(numNZSets,numNZSets,1);
			}        
			bitPos = g.getNextPosition();
			if (bitPos==-1){
				break;
			}
			pos[bitPos] = 1-pos[bitPos];
		}
		// compare with minNumNZSets and change minBM if needed
		//cout<<"compare with minNumNZSets and change minBM if needed"<<endl;
		if (first){
			mpz_set(minNumNZSets,numNZSets);
			for(uint32_t i = 0; i<M; i++){
				minBM[i] = b[mRows[i]];
			}
		}
		else if (numNZSets<minNumNZSets){
			mpz_set(minNumNZSets,numNZSets);
			for(uint32_t i = 0; i<M; i++){
				minBM[i] = b[mRows[i]];
			}
		}
    	//increment current values in b
		cout<<"increment current values in b"<<endl;
		bool allZero = true;
		for(uint32_t i = 0; i<M; i++){
			if(b[mRows[i]]>-sMat[mRows[i]].size()){
				b[mRows[i]] --;
				allZero = false;
				break;
			}
			else{
				b[mRows[i]] = 0;
				continue;
			}
		}
		//if equal to all 0 break
		if (allZero){
			break;
		}
		first = false;
	}
	//set b to be minBM
	cout<<"set b to be minBM"<<endl;
	for (uint32_t i = 0; i<M; i++){
		b[mRows[i]] = minBM[i];
	}
	
	//add the non-zero row sums with the corresponding coefficient -1^R for every possible extension R of S' in [n]/T
	cout<<"add the non-zero row sums with the corresponding coefficient -1^R for every possible extension R of S' in [n]/T"<<endl;
	mpz_t total;
	mpz_init(total);
	Gray gT(T.size());  //used for iterating over subsets of T
	int32_t bitPosT = gT.getNextPosition();
	vector<bool> posT(T.size(),false);
	posT[bitPosT] = true;
	int32_t rowsumT[n];  // holds rowsums for subsets of T but now for all n rows
	bool minusT = false;
	bool existsZero = false;
	mpz_t rowsumprodT;
	mpz_init(rowsumprodT);
	mpz_set_ui(rowsumprodT,1);
	cout<<"initializing 1-M rowsumt"<<endl;
	for (uint32_t i = 0; i<M; i++){
		rowsumT[i] = b[mRows[i]];
		mpz_mul_si(rowsumprodT,rowsumprodT,rowsumT[i]);
		if (rowsumT[i]==0){
			existsZero = true;
		}
	}
	cout<<"initializing M to n rowsumt mRowsC.size()="<<mRowsC.size()<<endl;
	for (uint32_t i = M; i<n; i++){
		rowsumT[i] = b[mRowsC[i-M]];
	}
	cout<<"initialized rowsumt"<<endl;
	if (!existsZero) {
		cout<<"No zero in initial"<<endl;
		Gray gTc(Tc.size());
		int32_t bitPosTc = gTc.getNextPosition();
		vector<bool> posTc(Tc.size(),false);
		posTc[bitPosTc] = true;
		int32_t rowsumTc[n];  // holds rowsums for subsets of Tc for all n rows
		mpz_t rowsumprodTc;
    	mpz_init(rowsumprodTc);
		for (uint32_t i = 0; i<M; i++){
			rowsumTc[i] = rowsumT[i];  // rowsumTc starts with rowsumT 
			mpz_mul_ui(rowsumprodTc,rowsumprodTc,rowsumTc[i]);
		}
		bool minusTc = minusT;
		if(minusTc){
			mpz_sub(total,total,rowsumprodTc);
		}
		else{
			mpz_add(total,total,rowsumprodTc);
		}
		
		cout<<"Performing gray on complement in initial"<<endl;
		while(true){
			minusTc = 1-minusTc;
			mpz_set(rowsumprodTc,rowsumprodT);
			std::set<uint32_t>::iterator setItc = Tc.begin();
			for(int i = 0; i < bitPosTc; i++)	setItc++;
			
			uint32_t matIndexTc = *setItc;
			/*for (uint32_t i = 0; i < M; i++)
			{
				if (posTc[bitPosTc]){
					if(std::find(sMat[mRows[i]].begin(), sMat[mRows[i]].end(), matIndexTc) != sMat[mRows[i]].end()){
						rowsumTc[i] += 1;
					}
				}
				else{
					if(std::find(sMat[mRows[i]].begin(), sMat[mRows[i]].end(), matIndexTc) != sMat[mRows[i]].end()){
						rowsumTc[i] -= 1;
					}
				}
				mpz_mul_si(rowsumprodTc,rowsumprodTc,rowsumTc[i]);
			}*/
			for (uint32_t i = M; i < n; i++)
			{
				if (posTc[bitPosTc]){
					if(std::find(sMat[mRowsC[i-M]].begin(), sMat[mRowsC[i-M]].end(), matIndexTc) != sMat[mRowsC[i-M]].end()){
						rowsumTc[i] += 1;
					}
				}
				else{
					if(std::find(sMat[mRowsC[i-M]].begin(), sMat[mRowsC[i-M]].end(), matIndexTc) != sMat[mRowsC[i-M]].end()){
						rowsumTc[i] -= 1;
					}
				}
				mpz_mul_si(rowsumprodTc,rowsumprodTc,rowsumTc[i]);
			}
			        
			//cout<<minus<<" "<<rowsumprod<<endl;
			if(minusTc){
				mpz_sub(total,total,rowsumprodTc);
			}
			else{
				mpz_add(total,total,rowsumprodTc);
			}
					
			bitPosTc = gTc.getNextPosition();
			if (bitPosTc==-1){
				break;
			}
			posTc[bitPosTc] = 1-posTc[bitPosTc];
		}
		mpz_clear(rowsumprodTc);
	}
	while(true)
	{
		minusT = 1-minusT;
		//cout<<"Performing step of gray on non-complement"<<endl;
		mpz_set_ui(rowsumprodT,1);
		std::set<uint32_t>::iterator setIt = T.begin();
		for(int i = 0; i < bitPosT; i++) setIt++;
		uint32_t matIndexT = *setIt;
		existsZero = false;
		for (uint32_t i = 0; i < M; i++)
		{
			if (posT[bitPosT]){
				if(std::find(sMat[mRows[i]].begin(), sMat[mRows[i]].end(), matIndexT) != sMat[mRows[i]].end()){
					rowsumT[i] += 1;
				}
				if (rowsumT[i] == 0){
					existsZero = true;
				}
			}
			else{
				if(std::find(sMat[mRows[i]].begin(), sMat[mRows[i]].end(), matIndexT) != sMat[mRows[i]].end()){
					rowsumT[i] -= 1;
				}
				if (rowsumT[i] == 0){
					existsZero = true;
				}
			}
			mpz_mul_si(rowsumprodT,rowsumprodT,rowsumT[i]);
		}
		for (uint32_t i = M; i < n; i++)
		{
			if (posT[bitPosT]){
				if(std::find(sMat[mRowsC[i-M]].begin(), sMat[mRowsC[i-M]].end(), matIndexT) != sMat[mRowsC[i-M]].end()){
					rowsumT[i] += 1;
				}
			}
			else{
				if(std::find(sMat[mRowsC[i-M]].begin(), sMat[mRowsC[i-M]].end(), matIndexT) != sMat[mRowsC[i-M]].end()){
					rowsumT[i] -= 1;
				}
			}
		}
		if (!existsZero){
			//cout<<"performing step of gray complement inside non-complement"<<endl;
			Gray gTc(Tc.size());
			int32_t bitPosTc = gTc.getNextPosition();
			vector<bool> posTc(Tc.size(),false);
			posTc[bitPosTc] = true;
			int32_t rowsumTc[n];  // holds rowsums for subsets of Tc for all n rows
			mpz_t rowsumprodTc;
			mpz_init(rowsumprodTc);
			mpz_set_ui(rowsumprodTc,1);
			for (uint32_t i = 0; i<n; i++){
				rowsumTc[i] = rowsumT[i];  // rowsumTc starts with rowsumT 
				mpz_mul_si(rowsumprodTc,rowsumprodTc,rowsumTc[i]);
			}
			bool minusTc = minusT;
			if(minusTc){
				mpz_sub(total,total,rowsumprodTc);
			}
			else{
				mpz_add(total,total,rowsumprodTc);
			}
			while(true){
				minusTc = 1-minusTc;
				mpz_set(rowsumprodTc,rowsumprodT);
				std::set<uint32_t>::iterator setItc = Tc.begin();
				for(int i = 0; i < bitPosTc; i++)	setItc++;
				
				uint32_t matIndexTc = *setItc;
				/*for (uint32_t i = 0; i < M; i++)
				{
					if (posTc[bitPosTc]){
						if(std::find(sMat[mRows[i]].begin(), sMat[mRows[i]].end(), matIndexTc) != sMat[mRows[i]].end()){
							rowsumTc[i] += 1;
						}
					}
					else{
						if(std::find(sMat[mRows[i]].begin(), sMat[mRows[i]].end(), matIndexTc) != sMat[mRows[i]].end()){
							rowsumTc[i] -= 1;
						}
					}
					mpz_mul_si(rowsumprodTc,rowsumprodTc,rowsumTc[i]);
				}*/
				for (uint32_t i = M; i < n; i++)
				{
					if (posTc[bitPosTc]){
						if(std::find(sMat[mRowsC[i-M]].begin(), sMat[mRowsC[i-M]].end(), matIndexTc) != sMat[mRowsC[i-M]].end()){
							rowsumTc[i] += 1;
						}
					}
					else{
						if(std::find(sMat[mRowsC[i-M]].begin(), sMat[mRowsC[i-M]].end(), matIndexTc) != sMat[mRowsC[i-M]].end()){
							rowsumTc[i] -= 1;
						}
					}
					mpz_mul_si(rowsumprodTc,rowsumprodTc,rowsumTc[i]);
				}
						
				//cout<<minus<<" "<<rowsumprod<<endl;
				if(minusTc){
					mpz_sub(total,total,rowsumprodTc);
				}
				else{
					mpz_add(total,total,rowsumprodTc);
				}
						
				bitPosTc = gTc.getNextPosition();
				if (bitPosTc==-1){
					break;
				}
				posTc[bitPosTc] = 1-posTc[bitPosTc];
			}
		}        
		bitPosT = gT.getNextPosition();
		if (bitPosT==-1){
			break;
		}
		posT[bitPosT] = 1-posT[bitPosT];
	}
	mpz_abs(total,total);
    gmp_printf("Permanent is %Zd\n",total);
}

int main(int argc, char* argv[]){
	double startTime = cpuTimeTotal();
	cout<< "Starting ServedioWan at ";
	print_wall_time();
	cout<<endl;
	
	if (!(argc==4)){
		cout<<"Usage: ServedioWan <n> <s/u/r/f/a'filename'/b'filename'/c/k'filename'/t/d> <M>"<<endl<<"where n is size of mat,  s if perfect matching should be present u otherwise r for random matrix f for full or 'a'+'filename' to read from file (a concatenated with filename) 'k'+filename to read from konect file, t for upper triangular d for diag+super-diagonal matrix"<<endl;
		cout<<"M is the number of sparse rows to consider. In the paper this is set to be n/8C but you can enter custom value"<<endl;
		cout<<"If M is entered <= 0 then default value is chosen"<<endl;
		exit(1);
	}
	uint32_t n = stoi(argv[1],NULL);
	std::string f = std::string(argv[2]);
	uint32_t M = stoi(argv[3],NULL);
	cout<<"ServedioWan invoked with n = "<<n<<" sat / unsat = " <<f<<endl<<endl;
	ServedioWan *HK;
	MatrixParser mp;
	uint32_t nedges;
	vector<vector<uint32_t>> mt;
	uint32_t m = n; 
	if(f[0]=='s'){
		//HK = new HopcroftKarp(n,m,true);
		mp.genJustSAT(n,m);
		mt = mp.getSparse(n,m,nedges);
		HK = new ServedioWan(n,mt,M);
	}
	else if (f[0] == 'u'){
		//HK = new HopcroftKarp(n,m,false);
		mp.genJustUNSAT(n,m);
		mt = mp.getSparse(n,m,nedges);
		HK = new ServedioWan(n,mt,M);
	}
	else if (f[0] == 'c'){
		std::string::size_type sz;
		mp.genCorrelatedRows(n,m,stod(f.substr(sz+2),NULL),stod(f.substr(1),&sz));
		mt = mp.getSparse(n,m,nedges);
		cout<<"Generated correlated matrix is.. "<<endl;
		MatrixUtils::printMat(mt);		
		HK = new ServedioWan(n,mt,M);
	}
	else if (f[0] == 'r'){
		//HK = new HopcroftKarp(n,m,false);
		mp.genRandom(n,m,stod(f.substr(1),NULL));
		mt = mp.getSparse(n,m,nedges);
		HK = new ServedioWan(n,mt,M);
	}
	else if (f[0] == 'f') {
		//HK = new HopcroftKarp(n,m,false);
		mp.genFull(n,m);
		mt = mp.getSparse(n,m,nedges);
		HK = new ServedioWan(n,mt,M);
	}
	else if (f[0] == 'a'){
		mp.readDense(f.substr(1));
		mt = mp.getSparse(n,m,nedges);
		HK = new ServedioWan(n,mt,M);
	}
	else if (f[0] == 'b'){
		mp.readSparse(f.substr(1));
		mt = mp.getSparse(n,m,nedges);
		HK = new ServedioWan(n,mt,M);
	}
	else if (f[0] == 'k'){
		mp.readKonect(f.substr(1));
		cout<<"Read graph"<<endl;
		mp.balanceFull();
		uint32_t dummy1, dummy2;
		//mt = mp.getSparse(dummy1,dummy2,nedges);
		mt = mp.getSparse(n,m,nedges);
		HK = new ServedioWan(n,mt,M);
	}
	else{
		cout<<"Could not recognize 2nd argument. Exiting.."<<endl;
		exit(1);
	}
	HK->count();
	cout<<endl<<endl<< "ServedioWan ended at ";
	print_wall_time();
	cout<<endl<<"Time Taken:"<<(cpuTimeTotal()-startTime)<<endl;
	
	return 0;
}