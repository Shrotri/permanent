#include "HopcroftKarp.h"
#include "MatrixParser.h"
#include "MatrixUtils.h"
#include <iostream>

int main(int argc, char* argv[]){
	cout<< "Starting HopcroftKarp at ";
	print_wall_time();
	cout<<endl;
	
	if (!(argc==4)){
		cout<<"Usage: HopcroftKarp <n> <m> <s/u/r/f/a'filename'>"<<endl<<"where n is numRows m is numCols. s if perfect matching should be present u otherwise r for random matrix f for full or 'a'+'filename' to read from file (a concatenated with filename and lastly w+filename to write matrix to filename or n to not write"<<endl;
		exit(1);
	}
	uint32_t n = stoi(argv[1],NULL);
	uint32_t m = stoi(argv[2],NULL);
	std::string f = std::string(argv[3]);
	
	cout<<"HopcroftKarp invoked with n = "<<n<<" sat / unsat = " <<f<<endl<<endl;
	HopcroftKarp *HK;
	MatrixParser mp;
	uint32_t nedges;
	vector<vector<uint32_t>> mt; 
	if(f[0]=='s'){
		//HK = new HopcroftKarp(n,m,true);
		mp.genJustSAT(n,m);
		mt = mp.getSparse(n,m,nedges);
		HK = new HopcroftKarp(n,m,mt);
	}
	else if (f[0] == 'u'){
		//HK = new HopcroftKarp(n,m,false);
		mp.genJustUNSAT(n,m);
		mt = mp.getSparse(n,m,nedges);
		HK = new HopcroftKarp(n,m,mt);
	}
	else if (f[0] == 'r'){
		//HK = new HopcroftKarp(n,m,false);
		mp.genRandom(n,m,stod(f.substr(1),NULL));
		mt = mp.getSparse(n,m,nedges);		
		HK = new HopcroftKarp(n,m,mt);
	}
	else if (f[0] == 'f') {
		//HK = new HopcroftKarp(n,m,false);
		mp.genFull(n,m);
		mt = mp.getSparse(n,m,nedges);
		HK = new HopcroftKarp(n,m,mt);
	}
	else if (f[0] == 'a'){
		mp.readDense(f.substr(1));
		mt = mp.getSparse(n,m,nedges);
		HK = new HopcroftKarp(n,m,mt);
	}
	else{
		cout<<"Could not recognize 2nd argument. Exiting.."<<endl;
		exit(1);
	}
	MatrixUtils::printMat(mt);
	
	unordered_set<uint32_t> sl, sr;
	vector<int32_t> ml = HK->matching(n,sl,sr);
	
	while(true){
		cout<<"matching is "<<endl;
		uint32_t siz = 0;
		for(uint32_t i =0;i<n;i++){
			cout<<i<<"->"<<ml[i]<<" ";
			if (ml[i]!=-1){
				siz++;
			}
		}
		cout<<endl<<"Matching size is "<<siz<<endl;
		cout<<"Continue? [y/n]"<<endl;
		string line; 
		getline(std::cin, line);
		if (line[0]!='y'){
			break;
		}
		cout<<"Enter edges as pair of integers seperated by space -- one on each line. Terminate with empty line"<<endl;
		sl.clear(); sr.clear();
		uint32_t match_ = 0;
		while(true){
			std::getline(std::cin, line);
			if (line ==""){
				break;
			}
			string::size_type sz;
			uint32_t l = stoi(line,&sz,10); //ignore char at beginning of header
			uint32_t r = stoi(line.substr(sz),nullptr,10);
			if (l>=n || r>=m){
				continue;
			}
			assert (sl.find(l)==sl.end());
			assert (sr.find(r)==sr.end());
			sl.insert(l);
			sr.insert(r);
		}
		ml = HK->matching(n, sl,sr);
	}
	cout<<endl<<endl<< "HopcroftKarp ended at ";
	print_wall_time();
	cout<<endl;
	
	return 0;
}

