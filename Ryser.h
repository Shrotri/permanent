#ifndef RYSER_H_
#define RYSER_H_

#include <fstream>
#include <sstream>
#include <iostream>
#include <cassert>

#include "RandomBits.h"
#include "Gray.h"

using std::ifstream;
using std::cout;
using std::endl;
using std::stoi;
using std::stringstream;
using std::pair;
using std::vector;


class Ryser{
	public:
		Ryser(uint32_t n_, vector<vector<bool>>& dMat): g(n_), n(n_), mat(dMat){
		}
		uint32_t getN();
		void calculatePermanent();
		
	private:
		uint32_t n;
		vector<vector<bool>> mat;
		Gray g;
};

#endif
