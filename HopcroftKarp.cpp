/* adapted from https://github.com/foreverbell/acm-icpc-cheat-sheet/blob/master/src/graph-algorithm/hopcroft-karp.cpp
 */
#include <queue>
#include "HopcroftKarp.h"

using std::queue;
void HopcroftKarp::add(uint32_t u, uint32_t v) {
    edge[u].push_back(v);
}

bool HopcroftKarp::dfs(uint32_t u){ 
    vis[u] = true;
    for (vector<uint32_t>::iterator it = edge[u].begin(); it != edge[u].end(); ++it) {
      uint32_t v = mr[*it];
      if (v == -1 || (!vis[v] && level[u] < level[v] && dfs(v))) {
        ml[u] = *it;
        mr[*it] = u;
        return true;
      }
    }
    return false;
}

bool HopcroftKarp::dfs(uint32_t u, unordered_set<uint32_t>& sl,unordered_set<uint32_t>& sr){ 
    vis[u] = true;
    for (vector<uint32_t>::iterator it = edge[u].begin(); it != edge[u].end(); ++it) {
      if (sr.find(*it)!=sr.end()){
		continue;
	  }
      uint32_t v = mr[*it];
      if (v!=-1 && sl.find(v)!=sl.end()){
			continue;
	  }
      if (v == -1 || (!vis[v] && level[u] < level[v] && dfs(v,sl,sr))) {
        ml[u] = *it;
        mr[*it] = u;
        return true;
      }
    }
    return false;
}

uint32_t HopcroftKarp::matching(uint32_t n) { // n for left
	matching_called = true;
    /* initialized in constructor
    mset0(vis); mset0(level);
    mset(ml, -1); mset(mr, -1);
    */
    for (uint32_t match = 0;;) {
      queue<uint32_t> que;
      for (uint32_t i = 0; i < n; ++i) {
        if (ml[i] == -1) {
          level[i] = 0;
          que.push(i);
        } else level[i] = -1;
      }
      while (!que.empty()) {
        uint32_t u = que.front(); 
        que.pop();
        for (vector<uint32_t>::iterator it = edge[u].begin(); it != edge[u].end(); ++it) {
          uint32_t v = mr[*it];
          if (v != -1 && level[v] < 0) {
            level[v] = level[u] + 1;
            que.push(v);
          }
        }
      }
      for (uint32_t i = 0; i < n; ++i) vis[i] = false;
      uint32_t d = 0;
      for (uint32_t i = 0; i < n; ++i) if (ml[i] == -1 && dfs(i)) ++d;
      if (d == 0) return match;
      match += d;
    }
}

vector<int32_t> HopcroftKarp::matching(uint32_t n, unordered_set<uint32_t>& sl,unordered_set<uint32_t>& sr) { // n for left  . sl - nodes to skip on left side sr - skipright
	matching_called = true;
    /* initialized in constructor
    mset0(vis); mset0(level);
    mset(ml, -1); mset(mr, -1);
    */
    

	std::fill(ml.begin(), ml.end(), -1);
	std::fill(mr.begin(), mr.end(), -1);
    for (uint32_t match = 0;;) {
      queue<uint32_t> que;
      for (uint32_t i = 0; i < n; ++i) {
		if (sl.find(i)!=sl.end()){
			continue;
		}
        if (ml[i] == -1) {
          level[i] = 0;
          que.push(i);
        } else level[i] = -1;
      }
      while (!que.empty()) {
        uint32_t u = que.front(); 
        que.pop();
        for (vector<uint32_t>::iterator it = edge[u].begin(); it != edge[u].end(); ++it) {
          if (sr.find(*it)!=sr.end()){
			continue;
		  }
          uint32_t v = mr[*it];
          if (v!=-1 && sl.find(v)!=sl.end()){
			continue;
		  }
          if (v != -1 && level[v] < 0) {
            level[v] = level[u] + 1;
            que.push(v);
          }
        }
      }
      for (uint32_t i = 0; i < n; ++i) vis[i] = false;
      uint32_t d = 0;
      for (uint32_t i = 0; i < n; ++i) {
		  if (sl.find(i)!=sl.end()){
			continue;
		  }
		  if (ml[i] == -1 && dfs(i,sl, sr)) ++d;
      }
      if (d == 0) return ml;
      match += d;
    }
}

uint32_t HopcroftKarp::matching(uint32_t n, unordered_set<uint32_t>& sl,unordered_set<uint32_t>& sr, uint32_t dummy) { // n for left  . sl - nodes to skip on left side sr - skipright
	matching_called = true;
    /* initialized in constructor
    mset0(vis); mset0(level);
    mset(ml, -1); mset(mr, -1);
    */
    

	std::fill(ml.begin(), ml.end(), -1);
	std::fill(mr.begin(), mr.end(), -1);
    for (uint32_t match = 0;;) {
      queue<uint32_t> que;
      for (uint32_t i = 0; i < n; ++i) {
		if (sl.find(i)!=sl.end()){
			continue;
		}
        if (ml[i] == -1) {
          level[i] = 0;
          que.push(i);
        } else level[i] = -1;
      }
      while (!que.empty()) {
        uint32_t u = que.front(); 
        que.pop();
        for (vector<uint32_t>::iterator it = edge[u].begin(); it != edge[u].end(); ++it) {
          if (sr.find(*it)!=sr.end()){
			continue;
		  }
          uint32_t v = mr[*it];
          if (v!=-1 && sl.find(v)!=sl.end()){
			continue;
		  }
          if (v != -1 && level[v] < 0) {
            level[v] = level[u] + 1;
            que.push(v);
          }
        }
      }
      for (uint32_t i = 0; i < n; ++i) vis[i] = false;
      uint32_t d = 0;
      for (uint32_t i = 0; i < n; ++i) {
		  if (sl.find(i)!=sl.end()){
			continue;
		  }
		  if (ml[i] == -1 && dfs(i,sl, sr)) ++d;
      }
      if (d == 0) return match;
      match += d;
    }
}


void HopcroftKarp::dfsVC(uint32_t u){ 
    if (vc[0][u] == false){
		return;
	}
    vc[0][u] = false;  // either u is an unmatched vertex i.e. ml[u] = -1 or is reachable from an unmatched vertex
   
    for (vector<uint32_t>::iterator it = edge[u].begin(); it != edge[u].end(); ++it) {
		if(vc[1][*it] == true){
			continue;          // node already visited
		}
		else{
			vc[1][*it] = true;
			uint32_t v = mr[*it];
			if (v == -1 ) {
				continue;
			}
			if (vc[0][v]==false){  //Node already visited
				continue;
			}
			dfsVC(v);
		}
    }
}

void HopcroftKarp::vertexCover(){
	assert(matching_called);
	vector<bool> t(maxN,true);
	vector<bool> f(maxN,false);
	vc.push_back(t);
	vc.push_back(f);
	for (uint32_t i = 0; i < maxN; i++){
		if (ml[i]==-1){
			dfsVC(i);
		}
	}
	cout<<"Vertex Cover is :"<<endl;
	for(uint32_t i = 0; i<maxN; i++){
		if(vc[0][i]){
			cout<<"l"<<i<<" ";
		}
	}
	cout<<endl;
	for(uint32_t i = 0; i<maxM; i++){
		if(vc[1][i]){
			cout<<"r"<<i<<" ";
		}
	}
	cout<<endl;
}


uint32_t HopcroftKarp::solve(){
	double startTime = cpuTimeTotal();
	cout<<"starting HopcroftKarp"<<endl;
	uint32_t result = matching(maxN);
	cout<<"finished HopcroftKarp. Maximum Matching size is "<<result<<endl<<" edges are "<<endl;
	/*for(uint32_t i = 0; i<maxM; i++){
		cout<<i<<"-"<<mr[i]<<endl;
	}
  cout<<endl;
  for(uint32_t i = 0; i<maxN; i++){
		cout<<ml[i]<<",";
	}*/
  cout<<endl;
	//vertexCover();
	double elapsedTime = cpuTimeTotal() - startTime;
	cout<<"HopcroftKarp took "<<elapsedTime<<" seconds"<<endl;
	return 0;
}
