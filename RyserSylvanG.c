#include <sylvan.h>
#include <stdlib.h>
#include <stdio.h>
#include <sylvan_gmp.h>
#include <string.h>

struct Mat{
	uint32_t ** beg;
	uint32_t* sizes;
	uint32_t n;
	uint32_t ne;   //num edges
};

typedef struct Mat Mat;

/*
Mat readMat(char* filename){

	char ch;
	FILE *fp;
	
	fp = fopen(filename, "r"); // read mode
	
	if (fp == NULL)
	{
		perror("Error while opening the file.\n");
		exit(EXIT_FAILURE);
	}
	Mat m;
	uint8_t line1 = 1;
	fgetc(fp); // read 1st dummy character
	fscanf(fp,"%u",&m.n);
	printf("n is %u\n",m.n);
	m.beg = (uint32_t**)malloc(sizeof(uint32_t*)*m.n);
	m.sizes = (uint32_t*)malloc(sizeof(uint32_t)*m.n);
	memset(m.sizes,0,m.n);
	m.ne = 0;
	char* lineptr = NULL;
	size_t len;
	if(getline(&lineptr, &len, fp)<0){
		printf("Could not read first line fully. Exiting..");
		exit(1);
	} // read till end of first line
	free(lineptr);
	for (uint32_t i = 0; i<m.n; i++){
		printf("Reading line %u of %u\n",i,m.n);
		lineptr = NULL;
		if(getline(&lineptr, &len, fp)<0){
			printf("Expected %d rows but found %d. Exiting..",m.n,i);
			exit(1);
		}
		
		char *pEnd = lineptr;
		while(1){
			char *pEnd2;
			uint32_t target = strtoul(pEnd,&pEnd2,10);
			if((*pEnd) == '\0' || pEnd == pEnd2){
				break;
			}
			pEnd = pEnd2;
			if (m.sizes[i]==0){
				m.beg[i] = (uint32_t*)malloc(sizeof(uint32_t));
			}
			else{
				m.beg[i] = (uint32_t*)realloc(m.beg[i],sizeof(uint32_t)*(m.sizes[i]+1));
			}
			m.sizes[i]++;
			//printf("%u <-> %u and pEnd is %p and lineptr is %p and *pEnd is %d\n",i,target,pEnd,lineptr,*pEnd);
			m.beg[i][m.sizes[i]-1] = target;
			m.ne++;
		}
		free(lineptr);
	}
	
	fclose(fp);
	return m;
}*/

Mat* readMat(FILE **fp, uint32_t n){
	Mat* m = (Mat*) malloc(sizeof(Mat));
	uint8_t line1 = 1;
	m->n = n;
	m->beg = (uint32_t**)malloc(sizeof(uint32_t*)*m->n);
	m->sizes = (uint32_t*)malloc(sizeof(uint32_t)*m->n);
	memset(m->sizes,0,m->n);
	m->ne = 0;
	char* lineptr = NULL;
	size_t len;
	for (uint32_t i = 0; i<m->n; i++){
		//printf("Reading line %u of %u\n",i,m->n);
		lineptr = NULL;
		if(getline(&lineptr, &len, *fp)<0){
			printf("Expected %d rows but found %d. Exiting..",m->n,i);
			exit(1);
		}
		
		char *pEnd = lineptr;
		while(1){
			char *pEnd2;
			uint32_t target = strtoul(pEnd,&pEnd2,10);
			if((*pEnd) == '\0' || pEnd == pEnd2){
				break;
			}
			pEnd = pEnd2;
			if (m->sizes[i]==0){
				m->beg[i] = (uint32_t*)malloc(sizeof(uint32_t));
			}
			else{
				m->beg[i] = (uint32_t*)realloc(m->beg[i],sizeof(uint32_t)*(m->sizes[i]+1));
			}
			m->sizes[i]++;
			//printf("%u <-> %u and pEnd is %p and lineptr is %p and *pEnd is %d\n",i,target,pEnd,lineptr,*pEnd);
			m->beg[i][m->sizes[i]-1] = target;
			m->ne++;
		}
		free(lineptr);
	}
	printf("Matrix read. Returning..\n");
	return m;
}

Mat* readFile(char* filename){
	char ch;
	FILE *fp;
	
	fp = fopen(filename, "r"); // read mode
	
	if (fp == NULL)
	{
		perror("Error while opening the file.\n");
		exit(EXIT_FAILURE);
	}
	uint32_t n;
	uint8_t line1 = 1;
	fgetc(fp); // read 1st dummy character
	fscanf(fp,"%u",&n);
	printf("n is %u\n",n);
	char* lineptr = NULL;
	size_t len;
	if(getline(&lineptr, &len, fp)<0){
		printf("Could not read first line fully. Exiting..");
		exit(1);
	} // read till end of first line
	free(lineptr);
	printf("Reading input matrix..\n");
	Mat* m_pointer = readMat(&fp, n);
	fclose(fp);
	return (m_pointer);
}

Mat* readProcessOutput(char* cmd) {
    FILE *fp;
    if ((fp = popen(cmd, "r")) == NULL) {
        printf("Error opening cnf file to input to addmc_print!\n");
        exit(1);
    }
	char* lineptr = NULL;
	size_t len, bytesRead;
	uint32_t linesRead = 0;
	//printf("Reading 15 lines..\n");
	while((bytesRead = getline(&lineptr, &len, fp))!=-1){
        linesRead++;
		if (linesRead>14){
			//printf("Last line read before processing clusters:\n");
			//printf("%s",lineptr);
			free(lineptr);
			lineptr = NULL;
			break;
		}
		free(lineptr);
		lineptr = NULL;
	}
	uint32_t numClusters;
	fgetc(fp); // read 1st dummy character
	fscanf(fp,"%u",&numClusters);
	//printf("numClusters is %u\n",numClusters);
	if(getline(&lineptr, &len, fp)<0){
		printf("Could not read first line fully. Exiting..");
		exit(1);
	} // read till end of first line
	free(lineptr);
	Mat* m_pointer = readMat(&fp, numClusters);
    if(pclose(fp))  {
        printf("Error while closing process. Command not found or exited with error status. Ignoring..\n");
        //exit(1);
    }
    return m_pointer;
}
int writeCNF(Mat* sMat, char* filename){
	//printf("Writing to %s\n",filename);
	FILE* fp = fopen(filename,"w");
	//printf("Done1\n");
	if (fp == NULL){
		printf("Error opening cnf file for writing!\n");
		exit(1);
	}
	fprintf(fp, "p cnf %d %d\n", sMat->n, sMat->n);
	for (uint32_t i = 0; i<sMat->n;i++){
		//printf("Writing row %d\n",i);
		for (uint32_t j = 0; j<sMat->sizes[i]; j++){
			fprintf(fp, "%d ", (sMat->beg[i][j]+1));  //variables are 1-indexed
		}
		fprintf(fp, "0\n");
	}
	fclose(fp);
	return(1);
}
int
main(int argc, char** argv){
	if (argc<5){
		printf("USAGE: RyserSylvan <sparseMat filename> <clustering procedure> <cluster var order> <diagram var order>\n");
		exit(1);
	}
	Mat* sMat = readFile(argv[1]);
	int32_t cp = atoi(argv[2]);
	int32_t cvo = atoi(argv[3]);
	int32_t dvo = atoi(argv[4]);	

	char *dir = getenv("TMP"); //check if getenv returns null
    if(dir == NULL){
		return;
    }
    char fname[] = "rs_cluster_order_addmc.cnf";
    char *fpath = NULL;//you need char buffer to store string 
    fpath = malloc(strlen(dir) + strlen(fname) + 1);//ENsure hexfile holds full filename
    strcpy(fpath,dir); //assuming you hold path in warea
    strcat(fpath, fname);//Assuming ypu hold filename in hex
    //printf("## %s\n", fpath);
	printf("Writing cnf for obtaining clusters..\n");
	writeCNF(sMat,fpath);
	printf("Cnf written. Running addmc_print..\n");
	/*for(uint32_t i = 0; i<sMat->n;i++){
		printf("%u ",sMat->sizes[i]);
	}*/
	//printf("\n");
	char* exe = "./addmc_print ";
	char options[] = " 1 5 5 4";
	if (cp!=-1){
		options[3] = (char)(cp+'0');
	}
	if (cvo!=-1){
		options[5] = (char)(cvo+'0');
	}
	if (dvo!=-1){
		//options[7] = (char)(dvo+'0');
	}
	char* cmd = malloc(strlen(exe)+strlen(fpath)+strlen(options)+1);
	strcpy(cmd,exe);
	strcat(cmd,fpath);
	strcat(cmd,options);
	printf("Running command %s\n", cmd);
	// *cmd = "./addmc_print $TMP/rs_cluster_order_addmc.cnf 1 5 5 4";    
	Mat* clustering = readProcessOutput(cmd);

	// Init Lace
	lace_init(1, 0); // auto-detect number of workers, use a 1,000,000 size task queue
	lace_startup(0, NULL, NULL); // auto-detect program stack, do not use a callback for startup

	// Lace is initialized, now set local variables
	LACE_ME;

	sylvan_set_limits(8192*1024*1024L, 4, 1);
	sylvan_init_package();
	sylvan_set_granularity(3); // granularity 3 is decent value for this small problem - 1 means "use cache for every operation"
	sylvan_init_mtbdd();
	gmp_init();
	
	printf("Inited packages!\n");
	mpq_t mg, og, zg;
	mpq_inits(mg,og,zg,NULL);
	mpq_set_si(mg,-1,1);
	mpq_set_ui(og,1,1);
	mpq_set_ui(zg,0,1);
	//int n = atoi(argv[1]);
	MTBDD* vars=(MTBDD*)malloc(sizeof(MTBDD)*sMat->n);
	
	MTBDD m = mtbdd_gmp(mg);
	MTBDD o = mtbdd_gmp(og);
	MTBDD z = mtbdd_gmp(zg);
	
	mtbdd_protect(&m);
	mtbdd_protect(&o);
	mtbdd_protect(&z);
	printf("Created mpqs! Constructing parity..\n");
	//MTBDD cube;
	MTBDD par = mtbdd_false;
	mtbdd_protect(&par);
	for (uint32_t i = 0 ; i<sMat->n; i++){
		vars[i] = mtbdd_ithvar(i);
		mtbdd_protect(&vars[i]);
		par = mtbdd_plus(mtbdd_times(par,mtbdd_comp(vars[i])),mtbdd_times(mtbdd_comp(par),vars[i]));
	}
	par = mtbdd_ite(par,m,o);
	printf("Parity constructed! Constructing intermediate ite's\n");
	MTBDD* r=(MTBDD*)malloc(sizeof(MTBDD)*sMat->n);
	for (uint32_t i = 0; i<sMat->n;i++){
		r[i] = mtbdd_ite(vars[i],o,z);
		mtbdd_protect(&r[i]);
	}
	printf("Constructing prod..\n");
	MTBDD prod = mtbdd_gmp(og);
	mtbdd_protect(&prod);	
	prod = gmp_times(prod,par);
	MTBDD sum = mtbdd_gmp(zg);
	mtbdd_protect(&sum);
	for (uint32_t i = 0; i<sMat->n;i++){
		for (uint32_t j = 0; j<sMat->sizes[i]; j++){
			sum = gmp_plus(sum,r[sMat->beg[i][j]]);
		}
		prod = gmp_times(prod, sum);
		sum = mtbdd_gmp(zg);
		//mtbdd_unprotect(&sum);
	}
	//free(r);
	printf("Constructed prod*parity..\n");
	//mtbdd_unprotect(&par);
	MTBDD temp = mtbdd_true;
	mtbdd_protect(&temp);
	for (int i = 0; i<sMat->n;i++){
		temp = mtbdd_times(temp,vars[i]);
	}
	MTBDD sup = mtbdd_support(temp);
	
	/*
	while(1){
		printf("%lu ",tpsup);
		tpsup = mtbdd_gethigh(tpsup);
		if (mtbdd_isleaf(tpsup)) break;
	}
	printf("\nSupport vars are..\n");
	*/
	printf("Doing exist abstract..\n");
	MTBDD rs = gmp_abstract_plus(prod, temp);
	mpq_ptr Rs = (mpq_ptr)(size_t)mtbdd_getvalue(rs);
	mpq_abs(Rs,Rs);
	gmp_printf("Total wt is %Qd\n",Rs);
	sylvan_quit();
	lace_exit();
}
