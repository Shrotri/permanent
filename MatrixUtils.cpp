#include <unordered_set>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <iostream>
#include <random>
#include <iterator>
#include <set>

#include "MatrixUtils.h"

using std::ofstream;
using std::cout;
using std::endl;
using std::vector;
using std::sort;
using std::unordered_set;

bool comp(vector<uint32_t>i,vector<uint32_t>j) {return i.size() < j.size();}

void MatrixUtils::transposeSparse(vector<vector<uint32_t>>& sMat,uint32_t m, vector<vector<uint32_t>>& sMatT){
	sMatT = vector<vector<uint32_t>>(m);
	for(uint32_t i = 0; i < sMat.size(); i++){
		for (uint32_t j = 0; j < sMat[i].size(); j++){
			sMatT[sMat[i][j]].push_back(i);
		}
	}
}
void MatrixUtils::transposeSparseToSet(vector<vector<uint32_t>>& sMat,uint32_t m, vector<unordered_set<uint32_t>>& sMatT){
	sMatT = vector<unordered_set<uint32_t>>(m);
	for(uint32_t i = 0; i < sMat.size(); i++){
		for (uint32_t j = 0; j < sMat[i].size(); j++){
			sMatT[sMat[i][j]].insert(i);
		}
	}
}
uint32_t MatrixUtils::purge(vector<vector<uint32_t>>& sparseMat){
	uint32_t mSize, n = sparseMat.size();
	HopcroftKarp hk(n,n,sparseMat);
	unordered_set<uint32_t> sr,sl;
	uint32_t numErased = 0;
	for (uint32_t i = 0 ; i < n; i++){
		for (uint32_t j = 0; j < sparseMat[i].size();){
			sl.insert(i);
			sr.insert(sparseMat[i][j]);
			mSize = hk.matching(n,sl,sr,0);
			//cout<<"Matching found"<<endl;
			if (mSize<n-1){
				sparseMat[i].erase(sparseMat[i].begin()+j);
				//cout<<"Erased"<<endl;
				numErased ++;
			}
			else{
				j++;
			}
			sl.clear();
			sr.clear();
		}
	}
	//cout<<"New Matrix"<<endl<<endl;
	//MatrixParser::printMat(sparseMat);
	//cout<<endl<<numErased<<" edges erased "<<endl;
	return numErased;
}

void MatrixUtils::sortAsc(vector<vector<uint32_t>>& sparseMat){
	sort(sparseMat.begin(),sparseMat.end(),comp);
}

void MatrixUtils::trim(vector<vector<uint32_t>>& sMat, uint32_t n, uint32_t m, uint32_t nNew){
	sortAsc(sMat);
	sMat.erase(sMat.begin(),sMat.begin()+n-nNew);
	
    std::set<unsigned int> indices;
	while (indices.size() < nNew)
		indices.insert((rand() % static_cast<uint32_t>(m + 1)));
	
	//std::sort(indices.begin(),indices.end());
    for (uint32_t i = 0; i<nNew;i++){
		vector<uint32_t> diff;
		std::set_intersection(sMat[i].begin(), sMat[i].end(), indices.begin(), indices.end(), std::inserter(diff, diff.begin()));
		sMat[i] = diff;
	}
}

void MatrixUtils::printMat(vector<vector<bool>>& dMat){
	for(uint32_t i = 0;i<dMat.size();i++){
		for(uint32_t j = 0 ; j<dMat[i].size();j++){
			if (dMat[i][j]){
				cout<<"1 ";
			}
			else{
				cout<<"0 ";
			}
		}
		cout<<endl;
	}
}

void MatrixUtils::printMat(vector<vector<uint32_t>>& sMat){
	for(uint32_t i = 0;i<sMat.size();i++){
		uint32_t k =0;
		for(uint32_t j = 0 ; j<sMat[i].size();){
			if (sMat[i][j]==k){
				cout<<"1 ";
				k++;
				j++;
			}
			else{
				cout<<"0 ";
				k++;
			}
		}
		while (k<sMat[i].size()){
			cout<<"0 ";
			k++;
		}
		cout<<endl;
	}
}


void MatrixUtils::writeDense(vector<vector<bool>>& dMat, string filename){
	ofstream ofile;
	ofile.open (filename,std::ios::trunc);
	ofile << "p "<<dMat.size()<<" "<<dMat[0].size()<<endl;
	for (uint32_t i = 0 ; i < dMat.size(); i++){
		for(uint32_t j = 0; j < dMat[i].size(); j++){
			if (dMat[i][j]){
				ofile << "1 ";
			} else{
				ofile << "0 ";
			}
		}
		ofile << endl;
	}
	ofile.close();
}

void MatrixUtils::writeDense(vector<vector<uint32_t>>& sMat,uint32_t m, string filename){
	ofstream ofile;
	ofile.open (filename,std::ios::trunc);
	ofile << "p "<<sMat.size()<<" "<<m<<endl;
	for (uint32_t i = 0 ; i < sMat.size(); i++){
		uint32_t k =0;
		for(uint32_t j = 0 ; j<sMat[i].size();){
			if (sMat[i][j]==k){
				ofile<<"1 ";
				k++;
				j++;
			}
			else{
				ofile<<"0 ";
				k++;
			}
		}
		while (k<sMat.size()){
			ofile<<"0 ";
			k++;
		}
		ofile << endl;
	}
	ofile.close();
}

void MatrixUtils::writeSparse(vector<vector<bool>>& dMat, string filename){
	ofstream ofile;
	ofile.open (filename,std::ios::trunc);
	ofile << "p "<<dMat.size()<<" "<<dMat[0].size()<<endl;
	for (uint32_t i = 0 ; i < dMat.size(); i++){
		for(uint32_t j = 0; j < dMat[i].size(); j++){
			if (dMat[i][j]){
				ofile << j << " ";
			}
		}
		ofile << endl;
	}
	ofile.close();
}

void MatrixUtils::writeSparse(vector<vector<uint32_t>>& sMat,uint32_t m, string filename){
	ofstream ofile;
	ofile.open (filename,std::ios::trunc);
	ofile << "p "<<sMat.size()<<" "<<m<<endl;
	for (uint32_t i = 0 ; i < sMat.size(); i++){
		
		for(uint32_t j = 0 ; j<sMat[i].size();j++){
			ofile<<sMat[i][j]<<" ";
		}
		ofile << endl;
	}
	ofile.close();
}

/*
int main(){
	uint32_t n,m,e;
	MatrixParser mp;
	mp.genRandom(10,20,0.5);
	vector<vector<uint32_t>> sMat = mp.getSparse(n,m,e);
	vector<vector<uint32_t>> sMatT;
	MatrixUtils::transposeSparse(sMat,m,sMatT);
	MatrixUtils::printMat(sMat);
	cout<<endl<<endl;
	MatrixUtils::printMat(sMatT);
	return(0);
}*/