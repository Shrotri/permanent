#ifndef RYSERADD_H_
#define RYSERADD_H_
 
#include <cassert>
#include <vector>
#include <iostream>

#include "cuddObj.hh"

using std::cout;
using std::endl;
using std::stoi;
using std::vector;
using std::string;

class RyserADD{
	public:
		RyserADD(uint32_t n_, vector<vector<uint32_t>> sMat_): n(n_), sMat(sMat_), mgr(0,0){
			//mgr.AutodynEnable();
		}
		void count();
		void count2();
		void printDOT();
		static void printADD(ADD, string, string);
	private:
		uint32_t n;
		vector<vector<uint32_t>> sMat;
		Cudd mgr;
};

#endif
