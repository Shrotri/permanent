import sys, os, random, math, uuid
from pysat.formula import CNF, IDPool
from pysat.solvers import Lingeling, Solver
from pysat.card import *
import subprocess, shlex
from datetime import datetime

def constructMatrix(n,r):
	mat = [[] for i in range (n)]
	matt = [[] for i in range (n)]
	m = 0
	for i in range(n):
		for j in range(n):
			if random.random()<r:
				mat[i].append(j)
				matt[j].append(i)
				m = m + 1
	return mat, matt, m

def writeMatrix(mat, n, filename):
	fmat = open(filename, 'w')
	for i in range(n):
		for j in range(n):
			if mat[i][j] == 1:
				fmat.write('1 ')
			else:
				fmat.write('0 ')
		fmat.write('\n')
	fmat.close()	

def readMat(dense, fname):
	mat = None
	matt = None
	f = open(fname,'r')
	i = 0
	n = 0
	m = 0
	if dense==0:
		for line in f:
			if i == 0:
				n = int(line.split()[1])
				mat = [[] for k in range (n)]
				matt = [[] for k in range (n)]
				i += 1
				continue
			l = line.split()
			for j in l:
				#print i
				mat[i-1].append(int(j))
				matt[int(j)].append(i-1)
				m += 1
			i += 1
	else:
		for line in f:
			if i == 0:
				n = int(line.split()[1])
				mat = [[] for k in range (n)]
				matt = [[] for k in range (n)]
				i += 1
				continue
			l = line.split()
			assert(len(l) == n)
			for j in range(n):
				if int(l[j]) == 1:
					mat[i-1].append(j)
					matt[j].append(i-1)
					m += 1
			i += 1
	return mat,matt,m, n

print datetime.now()
if len(sys.argv)<=4:
	print 'USAGE: python countPM.py <sspath 0-scalmc/1-sharpsat/2-d4/3-dsharp/4-countAntom/5-minic2d> <n> <inputType> <encoding> <computeMIS> <MIS timeout> <Count timeout> <other_arguments>'
	print 'inputType: 0+filename - read sparse 1+filename - read dense 2+prob-generate random matrix with prob density [0,1]'
	print 'countAntom is by default passed the following arguments. Use <other_arguments> to change'
	print '--noThreads=8 --memSize=8'
	print 'minic2d is passed following arguments by default; use <other_arguments> to change'
	print '--model_counter -c'
	sys.exit(1)
	
sspath = int(sys.argv[1]) #0-approxmc / 1-sharpSAT / 2-d4
n = int(sys.argv[2]) #size of matrix
itype = sys.argv[3] #0+filename - read sparse 1+filename - read dense 
                        #2+prob-generate random matrix with prob density [0,1]
enc = int(sys.argv[4])
assert(enc in [0,1,4,5,7,8])
#4 is best for probsharpsat
#5 is best for scalmc.
#Use MIS with scalmc but not with probsharpsat for best performance

doMIS = int(sys.argv[5]) #0-dont 1-do
toMIS = int(sys.argv[6])  #MIS timeout
toCount = int(sys.argv[7]) #counter timeout
arguments = ''
if len(sys.argv)>8:
	arguments = sys.argv[8]

mat =[] #sparse representation
matt = [] #transpose of mat
m = 0   #num edges

if itype.startswith('0'):
	mat, matt, m , n = readMat(0,itype[1:])
	print 'Matrix finished reading'
elif itype.startswith('1'):
	mat, matt, m , n = readMat(1,itype[1:])
	print 'Matrix finished reading'
elif itype.startswith('2'):
	mat, matt, m = constructMatrix(n,float(itype[1:]))
	print 'Matrix finished constructing'
else:
	print "Could not recognize inputType. Exiting.."
	sys.exit()

itype_sanitized = itype.replace('/','-')
tmp_file_suffix = 'temp_'+str(sspath)+'_'+str(n)+'_'+itype_sanitized+'_'+str(enc)+'_'+str(uuid.uuid4())+'.cnf'

formula = CNFPlus()
vpool = IDPool(start_from=1)
matr = lambda i, j: vpool.id('mat{0}@{1}'.format(i, j))

maxvar = 0
for i in range(n):
	l = []
	for j in mat[i]:
		l.append(matr(i,j))
	maxvar = max(maxvar,vpool.top)
	am1 = CardEnc.atmost(lits=l,bound=1, encoding=enc, top_id=maxvar)
	formula.extend(am1.clauses)
	maxvar = max(maxvar,am1.nv)
	if maxvar>vpool.top:
		vpool.occupy(vpool.top+1,maxvar)
	formula.append(l)

for j in range(n):
	l = []
	for i in matt[j]:
		l.append(matr(i,j))
	maxvar = max(maxvar,vpool.top)
	am2 = CardEnc.atmost(lits=l,bound=1, encoding=enc, top_id=maxvar)
	formula.extend(am2.clauses)
	maxvar = max(maxvar,am2.nv)
	if maxvar>vpool.top:
		vpool.occupy(vpool.top+1,maxvar)
	formula.append(l)

formula.to_file(os.environ['TMP']+'/'+tmp_file_suffix)

if doMIS==1:
	print 'Computing mis'
	os.system('cd $MIS && python MIS.py -output=$TMP/'+tmp_file_suffix+'.mis -timeout='+str(toMIS)+' $TMP/'+tmp_file_suffix)
	#-timeout=200
	#process = subprocess.Popen(args=['cd','/home/adi/Downloads/mis/mis/','&&', 'python','MIS.py','-timeout=20','-output=temp.mis','temp.cnf'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	#out, err = process.communicate()
	#print out,err

	ifl = open(os.environ['TMP']+'/'+tmp_file_suffix+'.mis','r')
	cnt = ifl.readline()
	ifl.close()
	cmt = ''
	if cnt.startswith('v'):
		cmt = 'c ind '+ cnt[2:-1]

	formula.to_file(os.environ['TMP']+'/'+tmp_file_suffix,[cmt])
	print 'MIS computed.' 

print 'Running counter..'

'''
if sspath == 0:
	arglist=['/home/adi/Downloads/scalmc/scalmc','--threshold=51','--measure=1', '/tmp/temp.cnf']
else:
	#arglist = ['/home/adi/Downloads/probsharpsat/probsharpSAT','-is','/tmp/1temp.mis','/tmp/temp.cnf']
	arglist = ['/home/adi/Downloads/probsharpsat/probsharpSAT','/tmp/temp.cnf']

process = subprocess.Popen(args=arglist, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
out1, err = process.communicate()
print out1, err
'''

if sspath == 0:
	os.system('timeout '+str(toCount)+' $SCALMC '+arguments+' $TMP/'+tmp_file_suffix)
elif sspath == 1:
	if doMIS ==1:
		os.system('timeout '+str(toCount)+' $SHARPSAT -is /tmp/'+tmp_file_suffix+'.mis '+arguments+' $TMP/'+tmp_file_suffix)
	else:
		os.system('timeout '+str(toCount)+' $SHARPSAT '+arguments+' $TMP/'+tmp_file_suffix)
elif sspath == 2:
	os.system('timeout '+str(toCount)+' $D4 '+arguments+' $TMP/'+tmp_file_suffix)
elif sspath == 3:
	os.system('timeout '+str(toCount)+' $DSHARP '+arguments+' $TMP/'+tmp_file_suffix)
elif sspath == 4:
	if arguments == '':
		arguments = '--noThreads=8 --memSize=8'
	os.system('timeout '+str(toCount)+' $COUNTANTOM '+arguments+' $TMP/'+tmp_file_suffix)
elif sspath == 5:
	if arguments == '':
		arguments = '--model_counter'
	print 'Running command ','timeout '+str(toCount)+' $MINIC2D '+arguments+' -c $TMP/'+tmp_file_suffix
	os.system('timeout '+str(toCount)+' $MINIC2D '+arguments+' -c $TMP/'+tmp_file_suffix)
print datetime.now()
