import sys
import numpy as np
import seaborn as sns
import matplotlib.pylab as plt
from collections import defaultdict

lB = int(sys.argv[1]) #num buckets on smaller side. Larger side is automatically calculated
						# using ratio of numNodes on left and right
size = int(sys.argv[2])
inFile = sys.argv[3]     #input file in edgelist representation
outFilePrefix = sys.argv[4]
outputDir = sys.argv[5]

su = 0 #size of left vertex set
sv = 0 #size of right vertex set
ne = 0 #number of edges

f = open(inFile,'r')
i = 0
mat = []
#matt = defaultdict(list)

for line in f:
	if i==0:
		i += 1
		continue
	l = line.split()
	if i==1:
		ne = int(l[1])
		su = int(l[2])
		sv = int(l[3])
		print 'ne, su, sv are ',ne,su,sv
		mat += [[] for i in range(su)]
		print 'Mat len is ', len(mat)
		i += 1
		continue
	s = int(l[0])
	t = int(l[1])
	mat[s-1].append(t-1)
	#matt[t-1].append(s-1)
	i += 1

mat.sort(key = lambda x: len(x), reverse = True)
matt = [[] for i in range(sv)]
for i in range(su):
	for j in mat[i]:
		matt[j].append(i)
for i in range(sv):
	matt[i].sort()
matt.sort(key = lambda x: len(x), reverse = True)

svB = 0
suB = 0

if su>sv:
	svB = lB
	suB = int((float(su)/sv)*lB)
else:
	suB = lB
	svB = int((float(sv)/su)*lB)

data = [[0 for j in range(suB)] for i in range(svB)]

nu = su/suB
nv = sv/svB
print 'nu, nv = ',nu,nv
for i in range(sv):
	k = min(svB-1,i/nv)
	for j in matt[i]:
		l = min(suB-1,j/nu)
		data[k][l] += 1
print data
ax = sns.heatmap(data)
plt.show()

res=[]
for i in range(size):
	res.append([])
	for j in range(size):
		if j in matt[i]:
			res[i].append(j)
	print res[i]

print "Writing to file.."
of = open(outputDir+'/'+outFilePrefix+'_'+str(size)+'_'+'0'+'.txt','w')
of.write('%'+' '+str(size)+ ' '+ str(size)+'\n')				
for i in range(size):
	for j in res[i]:
		of.write(str(j)+' ')
	of.write('\n')
of.close()