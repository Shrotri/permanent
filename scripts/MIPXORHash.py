from gurobipy import *
import random, os,sys, subprocess, shlex
import SparseProbsTolConf
from pysat.formula import CNF, IDPool
from pysat.solvers import Lingeling, Solver
from pysat.card import *

class FeasibleHash:
	def __init__(self, mat, m, n):
		self.model = Model("matching")
		self.m = m
		self.n = n
		x = []
		self.wmap = {}
		self.vsmap = {}
		edgeNo = 0
		for i in range(self.n):
			for j in range(self.n):
				if mat[i][j] == 1:
					x.append((i,j))
					self.wmap[edgeNo] = (i,j)
					self.vsmap[(i,j)] = edgeNo
					edgeNo = edgeNo + 1
		self.vs = self.model.addVars(x,vtype=GRB.BINARY)
		self.model.setObjective(1, GRB.MAXIMIZE)		
		self.model.addConstrs((self.vs.sum(i,'*') == 1 for i in range(self.n)), "row")
		self.model.addConstrs((self.vs.sum('*',j) == 1 for j in range(self.n)), "col")
		self.model.params.poolsearchmode = 2  # find n best sols 
		self.model.params.mipfocus = 1 #focus on finding feasible solutions
		self.model.params.logtoconsole = 0
				
	def initHash(self, num_xor_cls):
		self.AddHash(num_xor_cls,0)
	
	def cutxors(self, consVars, cell):
		xorclauses = []
		auxVarList = []
		cellIncluded = False
		if not consVars:
			print 'Empty consVars'
			return []
		if len(consVars) == 1:
			return [([self.vs[self.wmap[consVars[0]]], 0, cell], True)]
		if len(consVars) == 2:
			return [([self.vs[self.wmap[consVars[0]]], self.vs[self.wmap[consVars[1]]], cell], True)]
		for i in range(0,len(consVars),2):
			x1 = self.vs[self.wmap[consVars[i]]]
			x2 = None
			if i+1 < len(consVars):
				x2 = self.vs[self.wmap[consVars[i+1]]]
			else:
				cellIncluded = True
				x2 = cell
			y = self.model.addVar(vtype=GRB.BINARY)
			xorclauses.append(([x1,x2,y],False))
			auxVarList.append(y)
		if not cellIncluded:
			auxVarList.append(cell)
		#print 'xorclauses: ', xorclauses
		#print 'auxvarlist: ', auxVarList
		while True:
			if not auxVarList:
				return xorclauses
			if len(auxVarList) == 1:
				return xorclauses+[([auxVarList[0], 0, 0], True)]
			if len(auxVarList) == 2:
				return xorclauses+[([auxVarList[0], auxVarList[1], 0], True)]	
			
			nextAuxVarList = []
			for i in range(0,len(auxVarList),2):
				x1 = auxVarList[i]
				x2 = None
				if i+1 < len(auxVarList):
					x2 = auxVarList[i+1]
				else:
					x2 = 0
				y = self.model.addVar(vtype=GRB.BINARY)
				xorclauses.append(([x1,x2,y],False))
				nextAuxVarList.append(y)
			auxVarList = nextAuxVarList
	
	def AddHash(self, num_xor_cls, hC):  #hC is current hash count before adding num_xor_cls more hashes
		for i in range(num_xor_cls):
			consVars = [ j for j in range(self.m) if random.random()<SparseProbsTolConf.getSparseProbs(hC+i+1)]
			print 'Length of XOR is ',len(consVars)
			cell = 0
			if random.random()>0.5:
				cell = 1
			xorclauses = self.cutxors(consVars,cell)
			self.model.update()
			#print xorclauses
			for xcls in xorclauses: # each xcls is a pair ([varlist of gurobi variables],1/0) indicating whether xor of varlist should be 1 or 0
				if xcls[1]:
					print 'yay'
					self.model.addConstr(xcls[0][0]+xcls[0][1]+xcls[0][2] >= 1)
					self.model.addConstr(1-xcls[0][0]+1-xcls[0][1]+xcls[0][2] >= 1)
					self.model.addConstr(xcls[0][0]+1-xcls[0][1]+1-xcls[0][2] >= 1)
					self.model.addConstr(1-xcls[0][0]+xcls[0][1]+1-xcls[0][2] >= 1)
				else:
					self.model.addConstr(1-xcls[0][0]+1-xcls[0][1]+1-xcls[0][2] >= 1)
					self.model.addConstr(1-xcls[0][0]+xcls[0][1]+xcls[0][2] >= 1)
					self.model.addConstr(xcls[0][0]+1-xcls[0][1]+xcls[0][2] >= 1)
					self.model.addConstr(xcls[0][0]+xcls[0][1]+1-xcls[0][2] >= 1)
		
	def getNSols(self, maxSols):
		numSols = 0
		self.model.params.poolsolutions = maxSols
		self.model.optimize()
		if self.model.status == GRB.Status.OPTIMAL:
			numSols = numSols + self.model.solcount
		return numSols


class PBHash:
	def __init__(self, mat, m, n ):
		self.baseStr = ''
		self.numBaseCons = 0
		self.m = m
		self.numVars = m
		self.n = n
		self.target = 0
		self.execPath = '/home/adi/Downloads/cdcl-cutting_planes/src/roundingsat_compiled_with_O3'
		self.wmap = {}
		self.vsmap = {}
		self.xStr = ''  #xor constraint
		self.numXCons = 0
		edgeNo = 1
		for i in range(self.n):
			for j in range(self.n):
				if mat[i][j] == 1:
					self.baseStr = self.baseStr + '+1 x'+str(edgeNo)+' '
					self.wmap[edgeNo] = (i,j)
					self.vsmap[(i,j)] = edgeNo
					edgeNo = edgeNo + 1
			self.baseStr = self.baseStr + '= 1 ;\n'
			self.numBaseCons = self.numBaseCons + 1
		for j in range(self.n):
			for i in range(self.n):
				if mat[i][j] == 1:
					self.baseStr = self.baseStr + '+1 x'+str(self.vsmap[(i,j)])+' '
			self.baseStr = self.baseStr + '= 1 ;\n'
			self.numBaseCons = self.numBaseCons + 1
						
	def initHash(self, num_xor_cls):
		self.AddHash(num_xor_cls,0)
	
	def cutxors(self, consVars, cell):
		xorclauses = []
		auxVarList = []
		cellIncluded = False
		if not consVars:
			print 'Empty consVars'
			return []
		if len(consVars) == 1:
			return [(['x'+str(consVars[0]), 0, cell], True)]
		if len(consVars) == 2:
			return [(['x'+str(consVars[0]), 'x'+str(consVars[1]), cell], True)]
		for i in range(0,len(consVars),2):
			x1 = 'x'+str(consVars[i])
			x2 = None
			if i+1 < len(consVars):
				x2 = 'x'+str(consVars[i+1])
			else:
				cellIncluded = True
				x2 = cell
			y = 'x'+str(self.numVars + 1)
			self.numVars = self.numVars + 1
			xorclauses.append(([x1,x2,y],False))
			auxVarList.append(y)
		if not cellIncluded:
			auxVarList.append(cell)
		#print 'xorclauses: ', xorclauses
		#print 'auxvarlist: ', auxVarList
		while True:
			if not auxVarList:
				return xorclauses
			if len(auxVarList) == 1:
				return xorclauses+[([auxVarList[0], 0, 0], True)]
			if len(auxVarList) == 2:
				return xorclauses+[([auxVarList[0], auxVarList[1], 0], True)]	
			
			nextAuxVarList = []
			for i in range(0,len(auxVarList),2):
				x1 = auxVarList[i]
				x2 = None
				if i+1 < len(auxVarList):
					x2 = auxVarList[i+1]
				else:
					x2 = 0
				y = 'x'+str(self.numVars + 1)
				self.numVars = self.numVars + 1
				xorclauses.append(([x1,x2,y],False))
				nextAuxVarList.append(y)
			auxVarList = nextAuxVarList
	
	def AddHash(self, num_xor_cls, hC):
		for i in range(num_xor_cls):
			consVars = [ j for j in range(1,self.m+1) if random.random()<SparseProbsTolConf.getSparseProbs(hC+i+1)]
			print 'Length of XOR is ',len(consVars)
			#print consVars
			cell = 0
			if random.random()>0.5:
				cell = 1
			xorclauses = self.cutxors(consVars,cell)
			#print xorclauses
			negPattern ={True:[[1,1,1],[0,0,1],[0,1,0],[1,0,0]], False:[[0,0,0],[0,1,1],[1,0,1],[1,1,0]]}
			for xcls in xorclauses: # each xcls is a pair ([varlist of gurobi variables],1/0) indicating whether xor of varlist should be 1 or 0
				if xcls[1]:
					print 'yay'
				for ind1 in range(4):
					leftsum = 0
					for ind2 in range(3):
						if negPattern[xcls[1]][ind1][ind2] == 0:
							leftsum = leftsum + 1       # adding 1 - xcls[0][ind2]
							if xcls[0][ind2] == 1:
								leftsum = leftsum - 1
							elif xcls[0][ind2] == 0:
								leftsum = leftsum #do nothing
							else:
								self.xStr = self.xStr + '-1 '+xcls[0][ind2] + ' '
						else:
							if xcls[0][ind2] == 1:
								leftsum = leftsum + 1
							elif xcls[0][ind2] == 0:
								leftsum = leftsum #do nothing
							else:
								self.xStr = self.xStr + '+1 '+xcls[0][ind2] + ' '
					self.xStr = self.xStr + ' >= '+str(1-leftsum)+' ;\n'
					self.numXCons = self.numXCons + 1
						
	#process = subprocess.Popen(args=arg, stdout=flog, stderr=flog)
	#out, err = process.communicate()	
	def getNSols(self, maxSols):
		numSols = 0
		blockStr = ''
		numBlockCons = 0
		while(numSols<maxSols):
			opStr = '* #variable= '+str(self.numVars)+' #constraint= '+str(self.numBaseCons + self.numXCons + numBlockCons)+'\n' + self.baseStr + self.xStr + blockStr
			ftmp = open('temp.opb','w')
			ftmp.write(opStr)
			ftmp.close()
			process = subprocess.Popen(args=[self.execPath,'temp.opb'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			
			out1, err = process.communicate()
			out = out1.splitlines()
			s = ''
			if out[-2].startswith('s SATISFIABLE'):
				s = out[-1].split()
				#print s
				blockCons = ''
				for j in range(1,self.m+1):  #only block original vars #ignore v at beginning of assignment
					if s[j][0]=='-':
						blockCons = blockCons + '+1 x'+s[j][2:]+' '
					else:
						blockCons = blockCons + '-1 x'+s[j][1:]+' '
				blockCons = blockCons + '>= 1 ;\n'
				#print blockCons
				blockStr = blockStr + blockCons
				numBlockCons = numBlockCons + 1
				numSols = numSols + 1
				#print 'numSols = '+str(numSols) 
			elif out[-1].startswith('s UNSATISFIABLE'):
				break
			elif out[-1].startswith('Aborted'):
				print out+'\n'+err
				sys.exit(1)
			else:
				print 'Unhandled cdcl-cuttingplanes output case.\n Out:\n'+out1+'\nErr:\n'+err
				sys.exit(1)
			 	
			#print opStr
			#os.system(self.execPath+' temp.opb')
			#sys.exit(1)
			
		return numSols
		
class SATHash:
	def __init__(self, mat, m, n, enc=0):
		self.baseStr = ''
		self.numBaseCons = 0
		self.m = m
		self.numVars = m
		self.n = n
		
		self.execp = '/home/adi/Downloads/cosy/solvers/launcher/bliss/launch.sh'
		self.solverp = '/home/adi/Downloads/cosy/solvers/glucose-3.0/core/glucose'
		self.filep = 'temp.cnf'
		self.sopts = '-model'
		self.arglist = [self.execp,self.solverp,self.filep,self.sopts]
		self.vmap={}
		self.rvmap = {}
		k = 0
		for i in range(self.n):
			for j in range(self.n):
				if mat[i][j]==1:
					self.vmap[k] = (i,j)
					self.rvmap[(i,j)] = k
					k = k + 1
		self.numXCons = 0
		
		self.formula = CNFPlus()
		self.vpool = IDPool(start_from=1)
		self.mPool = lambda i, j: self.vpool.id('mat{0}@{1}'.format(i, j))

		self.maxvar = 0
		
		for i in range(self.n):
			l = [self.mPool(i,j) for j in range(self.n) if mat[i][j]==1]
			self.maxvar = max(self.maxvar,self.vpool.top)
			am1 = CardEnc.atmost(lits=l,bound=1, encoding=enc, top_id=self.maxvar)
			self.formula.extend(am1.clauses)
			self.maxvar = max(self.maxvar,am1.nv)
			if self.maxvar>self.vpool.top:
				self.vpool.occupy(self.vpool.top+1,self.maxvar)
			self.formula.append([self.mPool(i,j) for j in range(self.n) if mat[i][j]==1])

		for i in range(self.n):
			l=[self.mPool(j,i) for j in range(self.n) if mat[j][i]==1]
			self.maxvar = max(self.maxvar,self.vpool.top)
			am2 = CardEnc.atmost(lits=l,bound=1, encoding=enc, top_id=self.maxvar)
			self.formula.extend(am2.clauses)
			self.maxvar = max(self.maxvar,am2.nv)
			if self.maxvar>self.vpool.top:
				self.vpool.occupy(self.vpool.top+1,self.maxvar)
			self.formula.append([self.mPool(j,i) for j in range(self.n) if mat[j][i]==1])
		
		l = [self.mPool(self.n,0)] #extra dummy variable that represents the constant 'True'
		self.formula.append(l)
						
	def initHash(self, num_xor_cls):
		self.AddHash(num_xor_cls,0)
	
	def cutxors(self, consVars, cell,hC):
		xorclauses = []
		auxVarList = []
		auxInd=0
		cellIncluded = False
		cellV = self.mPool(self.n,0)
		if cell==0:
			cellV = -cellV
		if not consVars:
			print 'Empty consVars'
			return []
		if len(consVars) == 1:
			return [([consVars[0], -self.mPool(self.n,0), cellV], True)]
		if len(consVars) == 2:
			return [([consVars[0], consVars[1], cellV], True)]
		for i in range(0,len(consVars),2):
			x1 = consVars[i]
			x2 = None
			if i+1 < len(consVars):
				x2 = consVars[i+1]
			else:
				cellIncluded = True
				x2 = cellV
			y = self.mPool(self.n+1+hC,auxInd)
			auxInd = auxInd + 1
			xorclauses.append(([x1,x2,y],False))
			auxVarList.append(y)
		if not cellIncluded:
			auxVarList.append(cellV)
		#print 'xorclauses: ', xorclauses
		#print 'auxvarlist: ', auxVarList
		while True:
			if not auxVarList:
				return xorclauses
			if len(auxVarList) == 1:
				return xorclauses+[([auxVarList[0], -self.mPool(self.n,0), -self.mPool(self.n,0)], True)]
			if len(auxVarList) == 2:
				return xorclauses+[([auxVarList[0], auxVarList[1], -self.mPool(self.n,0)], True)]	
			
			nextAuxVarList = []
			for i in range(0,len(auxVarList),2):
				x1 = auxVarList[i]
				x2 = None
				if i+1 < len(auxVarList):
					x2 = auxVarList[i+1]
				else:
					x2 = -self.mPool(self.n,0)
				y = self.mPool(self.n+1+hC,auxInd)
				auxInd = auxInd + 1
				xorclauses.append(([x1,x2,y],False))
				nextAuxVarList.append(y)
			auxVarList = nextAuxVarList
	
	def AddHash(self, num_xor_cls, hC):  #hC is current hash count before adding num_xor_cls more hashes
		for i in range(num_xor_cls):
			consVars = [ self.mPool(self.vmap[j][0],self.vmap[j][1]) for j in range(self.m) if random.random()<SparseProbsTolConf.getSparseProbs(hC+i+1)]
			print 'Length of XOR is ',len(consVars)
			cell = 0
			if random.random()>0.5:
				cell = 1
			xorclauses = self.cutxors(consVars,cell,hC+i)
			
			#print xorclauses
			negPattern ={True:[[1,1,1],[0,0,1],[0,1,0],[1,0,0]], False:[[0,0,0],[0,1,1],[1,0,1],[1,1,0]]}
			for xcls in xorclauses: # each xcls is a pair ([varlist of gurobi variables],1/0) indicating whether xor of varlist should be 1 or 0
				if xcls[1]:
					print 'yay'
				for ind1 in range(4):
					
					l = []
					for ind2 in range(3):
						if negPattern[xcls[1]][ind1][ind2] == 0:
							l = l + [-xcls[0][ind2]]
						else:
							l = l + [xcls[0][ind2]]
					self.formula.append(l)
		
	#process = subprocess.Popen(args=arg, stdout=flog, stderr=flog)
	#out, err = process.communicate()	
	def getNSols(self, maxSols):
		numSols = 0
		fmla = self.formula.copy()
		while(numSols<maxSols):
			fmla.to_file('temp.cnf')
			process = subprocess.Popen(args=self.arglist, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			
			out1, err = process.communicate()
			out = out1.splitlines()
			s = ''
			if out[-2].startswith('s SATISFIABLE'):
				s = out[-1].split()
				#print s
				blockl = []
				for j in range(self.m):  #only block original vars #ignore v at beginning of assignment
					k = self.mPool(self.vmap[j][0],self.vmap[j][1])
					if s[k][0]=='-':
						assert(s[k]=='-'+str(k))
						blockl = blockl + [k]  #append negated literal
					else:
						assert(s[k]==str(k))
						blockl = blockl + [-k] #append negated literal
				fmla.append(blockl)
				numSols = numSols + 1
				#print 'numSols = '+str(numSols) 
			elif out[-1].startswith('s UNSATISFIABLE'):
				break
			elif out[-1].startswith('Aborted'):
				print out+'\n'+err
				sys.exit(1)
			else:
				print 'Unhandled SAT Solver output case.\n Out:\n'+out1+'\nErr:\n'+err
				sys.exit(1)
			 	
			#print opStr
			#os.system(self.execPath+' temp.opb')
			#if numSols==4:
			#	sys.exit(1)
			
		return numSols
