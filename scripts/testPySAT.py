import sys, os, random
from pysat.formula import CNF, IDPool
from pysat.solvers import Lingeling, Solver
from pysat.card import *

a = int(sys.argv[1])
b = int(sys.argv[2])
enc = int(sys.argv[3])

formula = CNFPlus()

lits = [i for i in range(1,a+1)]

eqb = CardEnc.equals(lits=lits,bound=b, encoding=enc)

formula.extend(eqb.clauses)

comment = 'c ind '
for ind in lits:
	comment = comment + str(ind) + ' '
comment = comment + '0'
formula.to_file('temp.cnf',[comment])
f=open('temp.is','w')
f.write('v '+comment[6:])
f.close()

os.system('~/Downloads/probsharpsat/probsharpSAT -is temp.is temp.cnf')
