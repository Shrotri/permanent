import os,sys
from pysat.formula import CNF, IDPool
from pysat.solvers import Lingeling, Solver
from pysat.card import *

sname = sys.argv[1]
n = int(sys.argv[2])
enc = int(sys.argv[3]) 
'''
0 pairwise         #no conflicts
1 seqcounter
2 sortnetwrk           #wrong count -- do not use
3 cardnetwrk           #wrong count -- do not use   
4 bitwise          #no conflicts
5 ladder
6 totalizer
7 mtotalizer
8 kmtotalizer
9 native
'''

sat = int(sys.argv[4]) #in ['sat','SAT','Sat','True', 'T', 'Y', 'Yes', 'true', '1', 't', 'y', 'yes', 'yeah', 'yup', 'certainly', 'uh-huh']
write = int(sys.argv[5])
filename='pySAT_dimacs.cnf'
if (len(sys.argv)>6):
	filename = sys.argv[6]
if enc==9:
	sname='mc'

formula = CNFPlus()
vpool = IDPool(start_from=1)
mat = lambda i, j: vpool.id('mat{0}@{1}'.format(i, j))

#use atmost and atleast separately instead of equals method since, atleast method uses complicated encoding.
#all encodings besides pairwise require auxiliary vars. top_id is used to specify where to start auxvar numbering. But it doesn't seem to work. Not clear how to tell vpool to skip auxvars used. There is a method for skipping in IDPool class. But not clear how to see how many auxvars or max auxvar used.
maxvar = 0
for i in range(n):
	l = [mat(i,j) for j in range(n)]
	maxvar = max(maxvar,vpool.top)
	am1 = CardEnc.atmost(lits=l,bound=1, encoding=enc, top_id=maxvar)
	if enc==9:
		for atmost in am1.atmosts:
			formula.append(atmost,is_atmost=True)
	else:
		formula.extend(am1.clauses)
	maxvar = max(maxvar,am1.nv)
	if maxvar>vpool.top:
		vpool.occupy(vpool.top+1,maxvar)
	formula.append([mat(i,j) for j in range(n)])

for i in range(n):
	l=[mat(j,i) for j in range(n)]
	maxvar = max(maxvar,vpool.top)
	am2=CardEnc.atmost(lits=l,bound=1, encoding=enc,top_id=maxvar)
	if enc==9:
		for atmost in am2.atmosts:
			formula.append(atmost,is_atmost=True)
	else:
		formula.extend(am2.clauses)
	maxvar = max(maxvar,am2.nv)
	if maxvar>vpool.top:
		vpool.occupy(vpool.top+1,maxvar)
	formula.append([mat(j,i) for j in range(n)])

#sys.exit(1)
falseind = 0
if sat==0:
	for i in range(n/2+1):
		for j in range(n/2+1):
			formula.append([-mat(i,j)])
elif sat==1:
	for i in range(n/2):
		for j in range(n/2):
			formula.append([-mat(i,j)])
#print formula.clauses
#print formula.atmosts

if write==1:
	formula.to_file(filename)
	sys.exit(1)
	
with Solver(name=sname,bootstrap_with=formula.clauses, use_timer=True) as l:
	if enc==9:
		for atmost in formula.atmosts:
			l.add_atmost(atmost[0],atmost[1])
	if l.solve() == False:
		print 'No perfect matching',l.time()
	else:
		print "sat!",l.time() #l.get_model()
