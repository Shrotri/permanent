import sys, os, random, math
from pysat.formula import CNF, IDPool
from pysat.solvers import Lingeling, Solver
from pysat.card import *
import subprocess, shlex
from gurobipy import *


def constructMatrix(n,r):
	mat = [[0]*n for i in range (n)]
	m = 0
	for i in range(n):
		for j in range(n):
			if random.random()>1-r:
				mat[i][j] = 1
				m = m + 1
	return mat,m

def complementMat(mat,n):
	cmat = [[0]*n for i in range(n)]
	for i in range(n):
		for j in range(n):
			if mat[i][j] == 0:
				cmat[i][j] = 1
	return cmat	

def writeMatrix(mat, n, filename):
	fmat = open(filename, 'w')
	for i in range(n):
		for j in range(n):
			if mat[i][j] == 1:
				fmat.write('1 ')
			else:
				fmat.write('0 ')
		fmat.write('\n')
	fmat.close()	
def printMat(mat,n):
	for i in range(n):
		for j in range(n):
			print mat[i][j], 
		print ''
		
def findMaxSatBiCliq(mat,n,m):  #does not give balanced biclique
	vpool = IDPool(start_from=1)
	varmat = lambda i, j: vpool.id('mat{0}@{1}'.format(i, j))
	msPath = '/home/adi/Downloads/open-wbo/open-wbo_static'   #path to maxsat solver
	msStr = ''
	numCls = 0
	softClsWt = 1
	topWt = m*softClsWt + 2  #There are m soft clauses. Therefore topWt (>sum of all softclause wts) is greater than m*softClsWt
	for i in range(n):
		for j in range(n):
			if mat[i][j] != 1:
				continue
			for k in range(n):
				if i == k:
					continue
				for l in range(n):
					if j == l:
						continue
					if mat[k][l] != 1:
						continue
					if mat[i][l] == 1 and mat[k][j] == 1:
						msStr = msStr +str(topWt)+' -'+str(varmat(i,j))+' -'+str(varmat(k,l))+' '+str(varmat(i,l))+' 0\n'  #hard clause
						numCls = numCls + 1
						msStr = msStr +str(topWt)+ ' -'+str(varmat(i,j))+' -'+str(varmat(k,l))+' '+str(varmat(k,j))+' 0\n' #hard clause
						numCls = numCls + 1
					else:
						msStr = msStr + str(topWt)+ ' -'+str(varmat(i,j))+' -'+str(varmat(k,l))+' 0\n'
						numCls = numCls + 1
			msStr = msStr + str(softClsWt) + ' '+str(varmat(i,j))+' 0\n' #soft clause
			numCls = numCls + 1
	headStr = 'p wcnf '+str(m)+' '+str(numCls)+' '+str(topWt)+'\n'
	fMS = open('temp.ms','w')
	fMS.write(headStr)
	fMS.write(msStr)
	fMS.close()
	print 'Starting maxsat solver'
	arglist = [msPath,'temp.ms']
	process = subprocess.Popen(args=arglist, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out1, err = process.communicate()
	print out1
	bqls = out1.splitlines()[-1]
	bq = bqls.split()
	for i in range(1,len(bq)):
		v = int(bq[i])
		if v>0:
			print vpool.obj(int(bq[i]))

def findBiCliq(mat, n, m, lv, rv):
	model = Model("matching")
	x = [i for i in range(1,len(lv)+len(rv)+1)]
	vs = model.addVars(x,vtype=GRB.BINARY)
	model.setObjective(sum(vs[i] for i in range(1,len(lv)+1)), GRB.MAXIMIZE)		
	for i in range(len(lv)):
		for j in range(len(rv)):
			if mat[lv[i]][rv[j]] == 0:
				model.addConstr(vs[i+1]+vs[len(lv)+j+1] <= 1)
	model.addConstr(sum(vs[i] for i in range(1,len(lv)+1)) - sum(vs[i] for i in range(len(lv)+1,len(lv)+len(rv)+1)) == 0)
	#model.params.poolsearchmode = 2  # find n best sols 
	#model.params.mipfocus = 1 #focus on finding feasible solutions
	#model.params.logtoconsole = 0
	model.optimize()
	print 'mOdel status is', model.status
	model.printAttr('X')
	bqv = [a.x for a in model.getVars()]
	nbq = [[lv[i] for i in range(len(lv)) if bqv[i]<1],[rv[i-len(lv)] for i in range(len(lv),len(lv)+len(rv)) if bqv[i]<1]] #complement of biclique
	bq =  [[lv[i] for i in range(len(lv)) if bqv[i]>0],[rv[i-len(lv)] for i in range(len(lv),len(lv)+len(rv)) if bqv[i]>0]]
	return bq,nbq
				
n = int(sys.argv[1])
r = float(sys.argv[2])  #1 for full matrix or no. between 0 and 1 for random matrix which is is probability of being 1
if r == 0:
	print 'density cannot be 0. exiting'
	sys.exit(1)
enc = int(sys.argv[3]) #encoding for exact-1 constraints
if enc not in [0,1,4,5,6,7,8]:
	print 'Valid encodings for exact-1 constraints are 0,1,4,5,6,7,8. Exact-k encoding is fixed to be 1 since 1,7,8 all give similar results. Exiting..'
	sys.exit(1)
encK = 1  #encoding for exact-k constraints is fixed to be 1 (options are 1,7,8) since all perform relatively same

path = int(sys.argv[4])   #path to probsharpsat executable or scalmc executable

verify = int(sys.argv[5])
hkpath = '/home/adi/Downloads/permanent/Gitlab/HopcroftKarp'
mat,m = constructMatrix(n,r) #matrix and number of edges
print mat
print m
print 'Input matrix is '
printMat(mat,n)


bq,nbq = findBiCliq(mat,n,m,[i for i in range(n)],[i for i in range(n)])
print bq
print nbq
while len(nbq[0])>17:
	bq, nbq = findBiCliq(mat,n,m, list(nbq[0]), list(nbq[1]))
	print bq
	print nbq
	
formula = CNFPlus()
vpool = IDPool(start_from=1)
vmat = lambda i, j: vpool.id('mat{0}@{1}'.format(i, j))

maxvar = 0
for i in nbq[0]:
	l = [vmat(i,j) for j in nbq[1] if mat[i][j] == 1]
	if len(l) == 0:
		print 'perm is zero'
		sys.exit(1)
	maxvar = max(maxvar,vpool.top)
	am1 = CardEnc.atmost(lits=l,bound=1, encoding=enc, top_id=maxvar)
	formula.extend(am1.clauses)
	maxvar = max(maxvar,am1.nv)
	if maxvar>vpool.top:
		vpool.occupy(vpool.top+1,maxvar)
	formula.append([vmat(i,j) for j in nbq[1] if mat[i][j] == 1])
for j in nbq[1]:
	l = [vmat(i,j) for i in nbq[0] if mat[i][j] == 1]
	if len(l) == 0:
		print 'perm is zero'
		sys.exit(1)
	maxvar = max(maxvar,vpool.top)
	am2 = CardEnc.atmost(lits=l,bound=1, encoding=enc, top_id=maxvar)
	formula.extend(am2.clauses)
	maxvar = max(maxvar,am2.nv)
	if maxvar>vpool.top:
		vpool.occupy(vpool.top+1,maxvar)
	formula.append([vmat(i,j) for i in nbq[0] if mat[i][j] == 1])		

formula.to_file('temp.cnf')
arglist=[]
if path == 0:
	arglist=['/home/adi/Downloads/scalmc/scalmc','--threshold=51','--measure=3', 'temp.cnf']
else:
	arglist = ['/home/adi/Downloads/probsharpsat/probsharpSAT','temp.cnf']

print 'Starting counting'
process = subprocess.Popen(args=arglist, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
out1, err = process.communicate()
print out1,err
