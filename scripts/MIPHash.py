from gurobipy import *
import random, os,sys, subprocess, shlex

class FeasibleHash:
	def __init__(self, mat, m, n):
		self.model = Model("matching")
		self.W = 1
		self.w = [0]*m
		self.m = m
		self.n = n
		self.target = 0
		x = []
		self.wmap = {}
		self.vsmap = {}
		edgeNo = 0
		for i in range(self.n):
			for j in range(self.n):
				if mat[i][j] == 1:
					x.append((i,j))
					self.wmap[edgeNo] = (i,j)
					self.vsmap[(i,j)] = edgeNo
					edgeNo = edgeNo + 1
		self.vs = self.model.addVars(x,vtype=GRB.BINARY)
		self.model.setObjective(1, GRB.MAXIMIZE)		
		self.model.addConstrs((self.vs.sum(i,'*') == 1 for i in range(self.n)), "row")
		self.model.addConstrs((self.vs.sum('*',j) == 1 for j in range(self.n)), "col")
		self.model.params.poolsearchmode = 2  # find n best sols 
		self.model.params.mipfocus = 1 #focus on finding feasible solutions
		self.model.params.logtoconsole = 0
				
	def initHash(self, num_xor_cls):
		self.AddHash(num_xor_cls,0)
			
	def AddHash(self, num_xor_cls, hC):
		self.W = self.W*2**num_xor_cls
		for i in range(self.m):
			self.w[i] = random.randint(0,self.W-1)
		self.target = random.randint(0,self.W-1)
		
		
	def getNSols(self, maxSols):
		numSols = 0
		i = 0
		while numSols < maxSols:
			i = i + 1
			if i==self.n:
				break
			self.model.params.poolsolutions = maxSols - numSols
			
			if i==1:
				wtCons = self.model.addConstr(sum((self.w[i] * self.vs[self.wmap[i][0],self.wmap[i][1]] for i in range(self.m))) == self.target + self.W*i, 'weight')
				#self.model.update()
				#print 'i==1 and numCons = ',len(self.model.getConstrs())
			else:
				wtCons.setAttr(GRB.Attr.RHS,self.target + self.W*i)
				#self.model.update()
				#print 'i==',i,' and numCons = ',len(self.model.getConstrs())
			self.model.optimize()
			if self.model.status == GRB.Status.OPTIMAL:
				numSols = numSols + self.model.solcount
		self.model.remove(wtCons)
		return numSols

class OptimalHash:
	def __init__(self, mat, m, n):
		self.model = Model("matching")
		self.W = 1
		self.w = [0]*m
		self.m = m
		self.n = n
		self.target = 0
		x = []
		self.wmap = {}
		self.vsmap = {}
		edgeNo = 0
		for i in range(self.n):
			for j in range(self.n):
				if mat[i][j] == 1:
					x.append((i,j))
					self.wmap[edgeNo] = (i,j)
					self.vsmap[(i,j)] = edgeNo
					edgeNo = edgeNo + 1
		self.vs = self.model.addVars(x,vtype=GRB.BINARY)
		#self.model.setObjective(1, GRB.MAXIMIZE)		
		self.model.addConstrs((self.vs.sum(i,'*') == 1 for i in range(self.n)), "row")
		self.model.addConstrs((self.vs.sum('*',j) == 1 for j in range(self.n)), "col")
		self.model.params.poolsearchmode = 2  # find n best sols 
		self.model.params.mipfocus = 2 #focus on finding optimal solutions
		self.model.params.poolgap = 0
		self.model.params.mipgapabs = 0
		self.model.params.logtoconsole = 0
				
	def initHash(self, num_xor_cls):
		self.AddHash(num_xor_cls,0)
			
	def AddHash(self, num_xor_cls, hC):
		self.W = self.W*2**num_xor_cls
		for i in range(self.m):
			self.w[i] = random.randint(0,self.W-1)
		self.target = random.randint(0,self.W-1)
		
		
	def getNSols(self, maxSols):
		numSols = 0
		i = 0
		while numSols < maxSols:
			i = i + 1
			if i==self.n:
				break
			self.model.params.poolsolutions = maxSols - numSols
			
			if i==1:
				wtCons = self.model.addConstr(sum((self.w[i] * self.vs[self.wmap[i][0],self.wmap[i][1]] for i in range(self.m))) <= self.target + self.W*i, 'weight')
				#self.model.update()
				#print 'i==1 and numCons = ',len(self.model.getConstrs())
			else:
				wtCons.setAttr(GRB.Attr.RHS,self.target + self.W*i)
				#self.model.update()
				#print 'i==',i,' and numCons = ',len(self.model.getConstrs())
			self.model.setObjective(sum((self.w[i] * self.vs[self.wmap[i][0],self.wmap[i][1]] for i in range(self.m))), GRB.MAXIMIZE)
			self.model.optimize()
			if self.model.status == GRB.Status.OPTIMAL:
				numSols = numSols + self.model.solcount
		self.model.remove(wtCons)
		return numSols

class PBHash:
	def __init__(self, mat, m, n ):
		self.baseStr = ''
		self.numBaseCons = 0
		self.W = 1
		self.w = [0]*m
		self.m = m
		self.n = n
		self.target = 0
		self.execPath = '/home/adi/Downloads/cdcl-cutting_planes/src/roundingsat'
		self.wmap = {}
		self.vsmap = {}
		edgeNo = 1
		for i in range(self.n):
			for j in range(self.n):
				if mat[i][j] == 1:
					self.baseStr = self.baseStr + '+1 x'+str(edgeNo)+' '
					self.wmap[edgeNo] = (i,j)
					self.vsmap[(i,j)] = edgeNo
					edgeNo = edgeNo + 1
			self.baseStr = self.baseStr + '= 1 ;\n'
			self.numBaseCons = self.numBaseCons + 1
		for j in range(self.n):
			for i in range(self.n):
				if mat[i][j] == 1:
					self.baseStr = self.baseStr + '+1 x'+str(self.vsmap[(i,j)])+' '
			self.baseStr = self.baseStr + '= 1 ;\n'
			self.numBaseCons = self.numBaseCons + 1
						
	def initHash(self, num_xor_cls):
		self.AddHash(num_xor_cls,0)
			
	def AddHash(self, num_xor_cls, hC):
		self.W = self.W*2**num_xor_cls
		for i in range(self.m):
			self.w[i] = random.randint(0,self.W-1)
		self.target = random.randint(0,self.W-1)
	
	
	#process = subprocess.Popen(args=arg, stdout=flog, stderr=flog)
	#out, err = process.communicate()	
	def getNSols(self, maxSols):
		numSols = 0
		i = 0
		while numSols < maxSols:
			i = i + 1
			print 'i = ',i
			if i==self.n:
				break
			wtStr = ''
			for j in range(1,self.m+1):
				if self.w[j-1] != 0:
					wtStr = wtStr + '+'+str(self.w[j-1])+' x'+str(j)+' '
			wtStr = wtStr+ '= '+str(self.target+i*self.W)+' ;\n'
			blockStr = ''
			numBlockCons = 0
			while(numSols<maxSols):
				opStr = '* #variable= '+str(self.m)+' #constraint= '+str(self.numBaseCons + 1 + numBlockCons)+'\n' + self.baseStr + wtStr + blockStr
				ftmp = open('temp.opb','w')
				ftmp.write(opStr)
				ftmp.close()
				process = subprocess.Popen(args=[self.execPath,'temp.opb'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
				
				out1, err = process.communicate()
				out = out1.splitlines()
				s = ''
				if out[-2].startswith('s SATISFIABLE'):
					s = out[-1].split()
					#print s
					blockCons = ''
					for j in range(1,len(s)):  #ignore v at beginning of assignment
						if s[j][0]=='-':
							blockCons = blockCons + '+1 x'+s[j][2:]+' '
						else:
							blockCons = blockCons + '-1 x'+s[j][1:]+' '
					blockCons = blockCons + '>= 1 ;\n'
					#print blockCons
					blockStr = blockStr + blockCons
					numBlockCons = numBlockCons + 1
					numSols = numSols + 1
					print 'numSols = '+str(numSols) 
				elif out[-1].startswith('s UNSATISFIABLE'):
					break
				elif out[-1].startswith('Aborted'):
					print out+'\n'+err
					sys.exit(1)
				else:
					print 'Unhandled cdcl-cuttingplanes output case.\n Out:\n'+out1+'\nErr:\n'+err
					sys.exit(1)
				 	
				#print opStr
				#os.system(self.execPath+' temp.opb')
				#sys.exit(1)
				
		return numSols
