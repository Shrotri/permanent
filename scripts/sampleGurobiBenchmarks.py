import sys, os, math, heapq#,random
import subprocess, shlex
import copy
from gurobipy import *

nLow = int(sys.argv[1])
nHigh =  int(sys.argv[2])
nStep = int(sys.argv[3])
	
instances = int(sys.argv[4]) #its actually the number of attempts at generating instances. If trivial, instance is discarded but still counted as attempt
inFile = sys.argv[5]
outFilePrefix = sys.argv[6]
outputDir = sys.argv[7]
exact = int(sys.argv[8])  #1:solve ilp w/ guarantee of at least one pm or 2:solve simpler ilp w/o guarantee then check against counter
timeoutILP = int(sys.argv[9])
timeoutSAT = int(sys.argv[10])
pcOverlap = float(sys.argv[11]) #0.6 overlap says no two instances may have more than 60% rows/cols in common
pcOverlapTrivial = float(sys.argv[12]) #overlap to use to prevent regenerating trivial instances. 
									#^This value can be larger since we decrease this value with the number of trivials generated
									#so as to avoid regenerating similar trivial instances
maxTrivials = int(sys.argv[13]) #max no. of trivial instances to generate before giving up and going to next value of n
assert(maxTrivials>1)
userInp = int(sys.argv[14]) #1 means program will wait for user input after reading file before constructing model to facilitate mem usage checking for example
method = int(sys.argv[15])
mat = [] #init empty. Vertices are stored 0 to n-1. Input files have vertex nos 1 to n.
         # So we subtract 1 from index while storing
		 
matt = [] #stores transpose for column access

vmap = {} #store map of matrix index to gurobi var no.
rmap = {} #reverse index stores map of gurobi var no to matrix index 
			#we use 0-indexing for gurobi var nos. in the maps
su = 0 #size of left vertex set
sv = 0 #size of right vertex set
ne = 0 #number of edges

def writeMat(of_, matr, n_, m_):
	of_.write('%'+' '+str(n_)+ ' '+ str(m_)+'\n')				
	for i in range(n_):
		for j in matr[i]:
			of_.write(str(j)+' ')
		of_.write('\n')
	of_.close()

#https://stackoverflow.com/questions/352670/weighted-random-selection-with-and-without-replacement
def WeightedSelectionWithoutReplacement(weights, m):
    elt = [(math.log(random.random()) / weights[i], i) for i in range(len(weights))]
    return [x[1] for x in heapq.nlargest(m, elt)]

print 'Reading input file..'
weights = []
f = open(inFile,'r')
lineNo = 0

edgeNo = 0
for line in f:
	if lineNo==0:  #skip first line in KOnect input files
		lineNo += 1
		continue
	l = line.split()
	if lineNo==1:
		ne = int(l[1])
		su = int(l[2])
		sv = int(l[3])
		mat += [[] for i in range(su)]
		matt += [[] for i in range(sv)]
		weights = [0 for i in range(su)]
		lineNo += 1
		continue
	s = int(l[0])
	t = int(l[1])
	mat[s-1].append(t-1)   #adjust for 0 indexed storing
	matt[t-1].append(s-1)
	weights[s-1] += 1
	vmap[(s-1,t-1)] = edgeNo
	rmap[edgeNo] = (s-1,t-1)
	edgeNo += 1  #0-indexed for gurobi
	lineNo += 1

assert(edgeNo==ne)
if userInp==1:
	rsp = raw_input("File read. Continue? [y/n]")
	if (not(rsp.strip() == 'Y')) and (not (rsp.strip()=='y')):
		sys.exit()

print "Constructing model.."

model = Model("dense_graph")
model.Params.Method= method
if exact==1:
	vs = model.addVars([i for i in range(ne+su+sv+ne)],vtype=GRB.BINARY)
	model.setObjective(sum(vs[i] for i in range(ne+su+sv,ne+su+sv+ne)), GRB.MAXIMIZE)
		
	for i in range(su):
		model.addConstr(sum(vs[vmap[(i,j)]] for j in mat[i])-vs[ne+i] == 0)
	for j in range(sv):
		model.addConstr(sum(vs[vmap[(i,j)]] for i in matt[j])-vs[ne+su+j] == 0)

	for i in range(su):
		for j in mat[i]:
			model.addConstr(vs[ne+i]-vs[ne+su+sv+vmap[(i,j)]] >= 0)
	for j in range(sv):
		for i in matt[j]:
			model.addConstr(vs[ne+su+j]-vs[ne+su+sv+vmap[(i,j)]] >= 0)
elif exact==2:
	vs = model.addVars([i for i in range(ne+su+sv+ne)],vtype=GRB.BINARY)
	#model.setObjective(sum(vs[i] for i in range(ne+su+sv,ne+su+sv+ne)), GRB.MAXIMIZE)
		
	for i in range(su):
		model.addConstr(sum(vs[vmap[(i,j)]] for j in mat[i])-vs[ne+i] == 0)
	for j in range(sv):
		model.addConstr(sum(vs[vmap[(i,j)]] for i in matt[j])-vs[ne+su+j] == 0)

	for i in range(su):
		for j in mat[i]:
			model.addConstr(vs[ne+i]-vs[ne+su+sv+vmap[(i,j)]] >= 0)
	for j in range(sv):
		for i in matt[j]:
			model.addConstr(vs[ne+su+j]-vs[ne+su+sv+vmap[(i,j)]] >= 0)

elif exact == 0:
	vs = model.addVars([i for i in range(ne+su+sv)],vtype=GRB.BINARY)
	#model.setObjective(sum(vs[i] for i in range(ne+su+sv,ne+su+sv+ne)), GRB.MAXIMIZE)
	for i in range(su):
		model.addConstr(sum(vs[vmap[(i,j)]] for j in mat[i])-vs[ne+i] >= 0)
	for j in range(sv):
		model.addConstr(sum(vs[vmap[(i,j)]] for i in matt[j])-vs[ne+su+j] >= 0)

	for i in range(su):
		for j in mat[i]:
			model.addConstr(vs[ne+i]-vs[vmap[(i,j)]] >= 0)
	for j in range(sv):
		for i in matt[j]:
			model.addConstr(vs[ne+su+j]-vs[vmap[(i,j)]] >= 0)

#constraint for balanced graph
model.addConstr(sum(vs[i] for i in range(ne,ne+su)) - sum(vs[i] for i in range(ne+su,ne+su+sv)) == 0)

print 'Generating samples..'

for n in range(nLow,nHigh,nStep):
	print 'N = ',n
	if n>nLow:
		model.remove(szCons)
	szCons = model.addConstr(sum(vs[i] for i in range(ne,ne+su)) == n)
	if exact == 0:
		if n>nLow:
			model.remove(densCons)
		#densCons=model.addConstr(sum(vs[i] for i in range(ne))>=3*int(n*math.log(n)))
		densCons=model.addConstr(sum(vs[i] for i in range(ne))>=3*n)
		model.params.mipfocus = 1 #focus on finding feasible solutions
	elif exact == 2:
		if n>nLow:
			model.remove(densCons)
		#densCons=model.addConstr(sum(vs[i] for i in range(ne))>=3*int(n*math.log(n)))
		densCons=model.addConstr(sum(vs[i] for i in range(ne+su+sv,ne+su+sv+ne))>=3*n)
		model.params.mipfocus = 1 #focus on finding feasible solutions

	fileInd = 0  #filename has to be unique. So we keep a list of indices used in above file, and create new files with increased indices
	blockCons = [] #blocking constraints that need to be removed before next outer-loop iteration
	if os.path.isfile(outputDir+'/'+outFilePrefix+'_'+str(n)+'_used_row_cols.txt'):
		print 'Reading existing diversity constraints (file found)'
		rcFile = open(outputDir+'/'+outFilePrefix+'_'+str(n)+'_used_row_cols.txt','r') #append to the file or create if does not exist
		#rcFile.seek(0) #read from beginning of file
		for line in rcFile:
			lsplit = line.split(';')
			rw = [int(rInd) for rInd in lsplit[1].split(',')]
			cl = [int(cInd) for cInd in lsplit[2].split(',')]
			if lsplit[0] == '*': #a trivial instance, therefore not written, but only recorded to avoid regeneration
				#use pcoverlaptrivial for trivial instances
				blockCons.append(model.addConstr(sum(vs[ne+rInd] for rInd in rw)+sum(vs[ne+su+cInd] for cInd in cl)<=int(2*n*pcOverlapTrivial)))
				continue
			else:
				#use pcoverlap for regular instances
				blockCons.append(model.addConstr(sum(vs[ne+rInd] for rInd in rw)+sum(vs[ne+su+cInd] for cInd in cl)<=int(2*n*pcOverlap)))
				fileInd = int(lsplit[0]) + 1
		rcFile.close() #open it later for appending. Avoid keeping open to prevent unsaved work.		
	ins = -1
	trivialCount = 0 #no of trivial instances generated. If no. goes above threshold, break and go to next n
	print 'Generating instances for n =',n
	noSol = False
	while True:
		ins += 1
		if ins>=instances or trivialCount>=maxTrivials or noSol or (ins >=1 and fileInd == 0):
			model.update()
			print 'Removing diversity constraints'
			for cns in blockCons:
				model.remove(cns)
			model.update()
			break
		print 'Doing instance ',ins,' out of ',instances
		#model.params.poolsearchmode = 2  # find n best sols 
		
		#model.params.logtoconsole = 0

		if timeoutILP!=0:
			model.setParam('TimeLimit', timeoutILP)

		print "Optimizing.."
		model.optimize()
		print 'mOdel status is', model.status
		#model.printAttr('X')
		if model.SolCount > 0:
			mvars = model.getVars()
			uvals = [i-ne for i in range(ne,ne+su) if int(mvars[i].x) == 1]
			vvals = [i-ne-su for i in range(ne+su,ne+su+sv) if int(mvars[i].x) == 1]
			#print 'u vertices are ', uvals
			#print 'v vertices are ', vvals
					
			print 'Printing and writing sampled subgraph..'
			res = []
			for i in range(len(uvals)):
				res.append([])
				for j in range(len(vvals)):
					if vvals[j] in mat[uvals[i]]:
						res[i].append(j)
						print '1 ',
					else:
						print '0 ',
				print ''
			of = open(outputDir+'/'+outFilePrefix+'_'+str(n)+'_'+str(fileInd)+'.txt','w')
			writeMat(of,res,n,n)
			#test subgraph for not easy count
			easyCount = False
			if exact==0:
				print "Checking for PM using Hopcroft Karp"
				arglist = [os.environ['PERM']+'/HopcroftKarp',str(su),str(sv),'b'+outputDir+'/'+outFilePrefix+'_'+str(n)+'_'+str(fileInd)+'.txt','n']
				process = subprocess.Popen(args=arglist, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
				out1, err = process.communicate()
				#print out1
				out = out1.splitlines()
				#get matching size
				#match = [int(x) for x in out[-6].split(',') if x!='']
				msize = int(out[7].split()[-1])
				print "Max Match size according to HKarp is ", msize
				easyCount = (msize!=n)

			if easyCount == False:
				print 'Testing count on probsharpsat..'
				arglist = ['python',os.environ['PERMSCRIPTS']+'/countPM.py','1',str(n),'0'+outputDir+'/'+outFilePrefix+'_'+str(n)+'_'+str(fileInd)+'.txt','4','0','0',str(timeoutSAT)]
				process = subprocess.Popen(args=arglist, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
				out1, err = process.communicate()
				out = out1.splitlines()
				#os.system('python countPM.py 1 '+str(n)+' 0'+outputDir+'/'+outFilePrefix+'_'+str(n)+'_'+str(ins)+'.txt 4 0 5 '+str(timeout))
				print out1,err
				outStr = ''
				if len(out) < 9:
					outStr = ''
				else:
					outStr = out[-8]
				print outStr
				easyCount = outStr.startswith('# END')
			opStr = ''
			for uv in uvals:
				opStr += str(uv)+','
			opStr = opStr[:-1]+';'
			for vv in vvals:
				opStr += str(vv)+','
			opStr = opStr[:-1]+'\n'
			if easyCount:
				opStr = '*;'+opStr
				os.remove(outputDir+'/'+outFilePrefix+'_'+str(n)+'_'+str(fileInd)+'.txt')
				trivialCount += 1
			
				#block trivial cons using pcoverlaptrivial-0.1*trivialCount
				print 'Trivial Matrix. Adding trivCons'
				blockCons.append(model.addConstr(sum(vs[ne+rInd] for rInd in uvals)+sum(vs[ne+su+cInd] for cInd in vvals)<=int(2*n*max(0.1,pcOverlapTrivial-0.1*trivialCount)),"trivCons"))
				if exact==0:
					#for inexact, we increase the required density instead of just blocking row/cols
					print 'Also Increasing density since we are doing inexact'
					#model.addConstr(sum(vs[i] for i in range(ne))>=(3+(pcOverlapTrivial*trivialCount))*int(n*math.log(n)))
					model.addConstr(sum(vs[i] for i in range(ne))>=(3+(pcOverlapTrivial*trivialCount))*n)
				if exact==2:
					#for inexact, we increase the required density instead of just blocking row/cols
					print 'Also Increasing density since we are doing inexact'
					#model.addConstr(sum(vs[i] for i in range(ne))>=(3+(pcOverlapTrivial*trivialCount))*int(n*math.log(n)))
					model.addConstr(sum(vs[i] for i in range(ne+su+sv,ne+su+sv+ne))>=(3+(pcOverlapTrivial*trivialCount))*n)
			else:
				opStr = str(fileInd)+';'+opStr
				fileInd += 1
				#block regular cons using pcoverlap
				blockCons.append(model.addConstr(sum(vs[ne+rInd] for rInd in uvals)+sum(vs[ne+su+cInd] for cInd in vvals)<=int(2*n*pcOverlap)))
			rcFile = open(outputDir+'/'+outFilePrefix+'_'+str(n)+'_used_row_cols.txt','a')
			rcFile.write(opStr)
			rcFile.close()
		else:
			print 'No solution found by Gurobi. Skipping to next value of n..'
			noSol = True

		
