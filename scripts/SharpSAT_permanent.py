import sys, os, random, math
from datetime import datetime

print datetime.now()
if len(sys.argv)<=6:
	print 'USAGE: python SharpSAT_permanent.py <sspath> <n> <inputType> <inMode> <probLo> <probHi> <encoding>'
	sys.exit(1)
	
sspath = sys.argv[1] #approxmc / sharpSAT path to executable with options  '~/Downloads/sharpSAT/sharpSAT'  or  '~/Downloads/cryptominisat/kuldeep_new/cryptominisat/build/scalmc --pivotAC=53 --tApproxMC=1' 
n = int(sys.argv[2]) #number of variables
inputType = sys.argv[3] #0-read from file 1-generate random matrix 2-generate full matrix
inMode = sys.argv[4] #file path if read from file. r/f if generate matrix
probLo = int(sys.argv[5]) #sampling probability encoded as problo/probHi where both are integers
probHi = int(sys.argv[6])
encoding = int(sys.argv[7])

opStr = ''
numVars = n*n
numCls = 0

def dummy(varlist):
	print '',
def atleastone(varlist):
	global opStr, numVars, numCls
	for j in varlist:
		opStr = opStr + str(j) + ' '
	numCls = numCls + 1 
	opStr = opStr + ' 0\n'

#amo = atmost 1
def amo_naive(varlist):
	global opStr, numVars, numCls
	for j in range(len(varlist)):
		for k in range(j+1,len(varlist)):
			numCls = numCls + 1
			opStr = opStr + str(-varlist[j]) + ' ' + str(-varlist[k]) + ' 0\n'

def amo_sinz(varlist):
	global opStr, numVars, numCls
	auxbegin = numVars+1
	numVars = numVars + len(varlist) - 1
	auxend = numVars
	
	opStr = opStr + str(-varlist[0]) + ' ' + str(auxbegin) + ' 0\n'
	numCls = numCls + 1
	opStr = opStr + str(-varlist[-1]) + ' ' + str(-auxend) + ' 0\n'
	numCls = numCls + 1
	
	for i in range(1,len(varlist)-1):
		numCls = numCls + 1
		opStr = opStr + str(-varlist[i]) + ' ' + str(auxbegin+i) + ' 0\n'
		numCls = numCls + 1
		opStr = opStr + str(-(auxbegin+i-1)) + ' ' + str(auxbegin+i) + ' 0\n'
		numCls = numCls + 1
		opStr = opStr + str(-varlist[i]) + ' ' + str(-(auxbegin+i-1)) + ' 0\n'

def amo_frisch(varlist):
	l = int(math.ceil(math.log(len(varlist))/math.log(2)))
	global opStr, numVars, numCls
	auxbegin = numVars+1
	numVars = numVars + l
	auxend = numVars
	
	for i in range(len(varlist)):
		a = list("{0:b}".format(i))
		if len(a)<l:
			a = ['0']*(l-len(a)) + a
		for j in range(l):
			numCls = numCls + 1
			if a[j] == '1':
				opStr = opStr + str(-varlist[i]) + ' ' + str(auxbegin+j) + ' 0\n'
			else:
				opStr = opStr + str(-varlist[i]) + ' ' + str(-(auxbegin+j)) + ' 0\n'

def cmdr_exact_one(nested_varlist):
	global opStr, numVars, numCls
	aux = 0
	if type(nested_varlist) == type((1,)):
		aux = numVars+1
		numVars = numVars + 1
		nested_varlist = nested_varlist[0]
	cls_vars = []
	for i in range(len(nested_varlist)):
		if type(nested_varlist[i]) is not list:
			cls_vars = cls_vars + [nested_varlist[i]]
		else:
			cls_vars = cls_vars + [numVars + 1]
			cmdr_exact_one((nested_varlist[i],True)) #call to cmdr_exact_one should use aux (commander) variable
	if aux != 0:
		cls_vars = cls_vars + [-aux]
	amo_naive(cls_vars)
	atleastone(cls_vars)
	
def cmdr_group_vars(varlist,grpSize):
	nv = len(varlist)
	if nv <= grpSize:
		return varlist
	ret = []
	numGrps = nv /grpSize
	for i in range(numGrps):
		ret = ret + [[varlist[j] for j in range(i*nv/numGrps, (i+1)*nv/numGrps)]] 	
	return cmdr_group_vars(ret,grpSize)

def eo_cmdr(varlist): #exact one commander encoding
	return cmdr_exact_one(cmdr_group_vars(varlist,3))
	
"""
	numGrps = int(math.ceil(len(varlist)/numGrpVars))
	grps = [[varlist[j*numGrps+i] for i in range(numGrpVars) if (j*numGrps+i)<len(varlist)] for j in range(numGrps)]
	for grp in grps:
		for j in range(len(grp)):
			for k in range(j+1,len(grp)):
				numCls = numCls + 1
				opStr = opStr + str(-grp[j]) + ' ' + str(-grp[k]) + ' 0\n'
"""				
#todo: amo_heule amo_commander amo_jinghaochen
				
amo_method = None
alo_method = None

if encoding == 1:                 #1-naive 2-sinz 3-frisch 4-commander
	amo_method = amo_naive
	alo_method = atleastone
elif encoding == 2:
	amo_method = amo_sinz
	alo_method = atleastone
elif encoding == 3:
	amo_method = amo_frisch
	alo_method = atleastone
elif encoding == 4:
	amo_method = eo_cmdr
	alo_method = dummy
			
varlists = []
for i in range(n):
		varlist = []
		for j in range(i*n+1,i*n+n+1):
			varlist = varlist + [j]
		varlists = varlists + [varlist]
for i in range(n):
		varlist = []
		for j in range(i+1,i+1+n*n,n):
			varlist = varlist + [j]
		varlists = varlists + [varlist]			

for lst in varlists:
	amo_method(lst)
	alo_method(lst)

prefix = ''

if inMode == 'r':
	for	i in range (1,n*n+1):
		rb = random.randint(0,probHi) 
		if rb < probLo:
			prefix = prefix + str(-i) + ' 0\n'
			numCls = numCls + 1
			
prefix = 'p cnf '+str(numVars) + ' ' + str(numCls) + '\n' + prefix
opStr = prefix + opStr


f = open('temp.cnf','w')
f.write(opStr)
f.close()

print 'done'
os.system(sspath+' temp.cnf')
print datetime.now()
"""
	for i in range(n):
		for j in range(i*n+1,i*n+n+1):
			for k in range(j+1,i*n+n+1):
				numCls = numCls + 1
				opStr = opStr + str(-j) + ' ' + str(-k) + ' 0\n'
		for j in range(i*n+1,i*n+n+1):
			numVars = numVars + 1
			opStr = opStr + str(j) + ' '
		numCls = numCls + 1 
		opStr = opStr + ' 0\n'

	for i in range(n):
		for j in range(i+1,i+1+n*n,n):
			for k in range(j+n,i+1+n*n,n):
				numCls = numCls + 1
				opStr = opStr + str(-j) + ' ' + str(-k) + ' 0\n'
		for j in range(i+1,i+1+n*n,n):
			opStr = opStr + str(j) + ' '
		numCls = numCls + 1 
		opStr = opStr + ' 0\n'
"""
