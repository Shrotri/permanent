import sys, os, random, math, heapq
import subprocess, shlex
import copy
from pyscipopt import *

nLow = int(sys.argv[1])
nHigh =  int(sys.argv[2])
nStep = int(sys.argv[3])
	
instances = int(sys.argv[4])
inFile = sys.argv[5]
outFilePrefix = sys.argv[6]
outputDir = sys.argv[7]
timeout = int(sys.argv[8])

mat = [] #init empty. Vertices are stored 0 to n-1. Input files have vertex nos 1 to n.
         # So we subtract 1 from index while storing
		 
matt = [] #stores transpose for column access

vmap = {} #store map of matrix index to gurobi var no.
rmap = {} #reverse index stores map of gurobi var no to matrix index 
			#we use 0-indexing for gurobi var nos. in the maps
su = 0 #size of left vertex set
sv = 0 #size of right vertex set
ne = 0 #number of edges

def writeMat(of_, matr, n_, m_):
	of_.write('%'+' '+str(n_)+ ' '+ str(m_)+'\n')				
	for i in range(n_):
		for j in matr[i]:
			of_.write(str(j)+' ')
		of_.write('\n')
	of_.close()

#https://stackoverflow.com/questions/352670/weighted-random-selection-with-and-without-replacement
def WeightedSelectionWithoutReplacement(weights, m):
    elt = [(math.log(random.random()) / weights[i], i) for i in range(len(weights))]
    return [x[1] for x in heapq.nlargest(m, elt)]

print 'Reading input file..'
weights = []
f = open(inFile,'r')
lineNo = 0
edgeNo = 0
for line in f:
	if lineNo==0:  #skip first line in KOnect input files
		lineNo += 1
		continue
	l = line.split()
	if lineNo==1:
		ne = int(l[1])
		su = int(l[2])
		sv = int(l[3])
		mat += [[] for i in range(su)]
		matt += [[] for i in range(sv)]
		weights = [0 for i in range(su)]
		lineNo += 1
		continue
	s = int(l[0])
	t = int(l[1])
	mat[s-1].append(t-1)   #adjust for 0 indexed storing
	matt[t-1].append(s-1)
	weights[s-1] += 1
	vmap[(s-1,t-1)] = edgeNo
	rmap[edgeNo] = (s-1,t-1)
	edgeNo += 1  #0-indexed for gurobi
	lineNo += 1

assert(edgeNo==ne)
rsp = raw_input("File read. Continue? [y/n]")
if (not(rsp.strip() == 'Y')) and (not (rsp.strip()=='y')):
	sys.exit()

print "Constructing model.."

model = Model("dense_graph")
#vs = model.addVars([i for i in range(ne+su+sv)],vtype='B')
vs = []
for i in range(ne+su+sv):
	vs.append(model.addVar(str(i),vtype='B'))
print 'Added variables. Setting objective..'
model.setObjective(quicksum(vs[i] for i in range(ne)), "maximize")		
#model.setObjective(1, "maximize")
print 'Objective set. Adding constraints..'
for i in range(su):
	model.addCons(quicksum(vs[vmap[(i,j)]] for j in mat[i])-vs[ne+i] >= 0)
for j in range(sv):
	model.addCons(quicksum(vs[vmap[(i,j)]] for i in matt[j])-vs[ne+su+j] >= 0)
print 'Added 1,2 constraints. Adding 3,4..'
for i in range(su):
	for j in mat[i]:
		model.addCons(vs[ne+i]-vs[vmap[(i,j)]] >= 0)
for j in range(sv):
	for i in matt[j]:
		model.addCons(vs[ne+su+j]-vs[vmap[(i,j)]] >= 0)
print 'Added 3,4 constraints. Adding 5,6..'
model.addCons(quicksum(vs[i] for i in range(ne,ne+su)) - quicksum(vs[i] for i in range(ne+su,ne+su+sv)) == 0)
model.addCons(quicksum(vs[i] for i in range(ne,ne+su)) == nLow)

#model.addCons(quicksum(vs[i] for i in range(ne))>=50)
#model.params.poolsearchmode = 2  # find n best sols 
#model.params.mipfocus = 1 #focus on finding feasible solutions
#model.params.logtoconsole = 0

#model.setParam('TimeLimit', timeout)

print "Optimizing.."
model.optimize()
print 'mOdel status is', model.status
#model.printAttr('X')
mvars = model.getVars()
uvals = [i-ne for i in range(ne,ne+su) if int(mvars[i].x) == 1]
vvals = [i-ne-su for i in range(ne+su,ne+su+sv) if int(mvars[i].x) == 1]

print 'u vertices are ', uvals
print 'v vertices are ', vvals
print "Printing optimal submatrix.."
for i in uvals:
	for j in vvals:
		if j in mat[i]:
			print "1 ",
		else:
			print "0 ",
	print ''