import sys, os, random, math
from pysat.formula import CNF, IDPool
from pysat.solvers import Lingeling, Solver
from pysat.card import *
import subprocess, shlex

#computing rank of incidence matrix i.e. (n - the number of connected components) needs to be implemented
print 'Warning: computing rank of incidence matrix i.e. (n - the number of connected components) needs to be implemented'

def constructMatrix(n,r):
	mat = [[1]*n for i in range (n)]
	if r == 0:
		return mat,n*n
	else:
		m = n*n
		for i in range(n):
			for j in range(n):
				if random.random()>0.5:
					mat[i][j] = 0
					m = m -1
		return mat,m

def writeMatrix(mat, n, filename):
	fmat = open(filename, 'w')
	for i in range(n):
		for j in range(n):
			if mat[i][j] == 1:
				fmat.write('1 ')
			else:
				fmat.write('0 ')
		fmat.write('\n')
	fmat.close()	

def makeOdd(mat , n, m):
	evenA = []
	evenB = []

	newMat = [[0]*(n+2) for i in range (n+2)]
	newM = 0

	numBEdges = [0]*n
	for i in range(n):
		numAEdges = 0
		for j in range(n):
			if mat[i][j]==1:
				numAEdges = numAEdges + 1	
				numBEdges[j] = numBEdges[j] + 1
				newMat[i][j] = 1
				newM = newM + 1
		if numAEdges%2 == 0:
			evenA.append(i)
	for	i in range(n):
		if numBEdges[i]%2 == 0:
			evenB.append(i)
	for i in evenA:
		newMat[i][n] = 1
		newM = newM + 1
	for i in evenB:
		newMat[n][i] = 1
		newM = newM + 1
	newMat[n+1][n] = 1
	newM = newM + 1
	newMat[n][n+1] = 1
	newM = newM + 1
	if len(evenA)%2 == 1:
		newMat[n][n] = 1
		newM = newM + 1
	return newMat, newM	

def readMat(dense, fname):
	mat = None
	matt = None
	f = open(fname,'r')
	i = 0
	n = 0
	m = 0
	if dense==0:
		for line in f:
			if i == 0:
				n = int(line.split()[1])
				mat = [[] for k in range (n)]
				matt = [[] for k in range (n)]
				i += 1
				continue
			l = line.split()
			for j in l:
				#print i
				mat[i-1].append(int(j))
				matt[int(j)].append(i-1)
				m += 1
			i += 1
	else:
		for line in f:
			if i == 0:
				n = int(line.split()[1])
				mat = [[] for k in range (n)]
				matt = [[] for k in range (n)]
				i += 1
				continue
			l = line.split()
			assert(len(l) == n)
			for j in range(n):
				if int(l[j]) == 1:
					mat[i-1].append(j)
					matt[j].append(i-1)
					m += 1
			i += 1
	return mat,matt,m, n

n = int(sys.argv[1])
r = sys.argv[2]  #2 for read from sparse file 1 for random matrix 0 for full matrix

enc = int(sys.argv[3]) #encoding for exact-k constraints
if enc not in [1,7,8]:
	print 'Valid encodings for exact-k constraints are 1,7,8. See testPySAT.py (other encodings give incorrect answers). Exiting..'
	sys.exit(1)
path = int(sys.argv[4])   #path to probsharpsat executable or scalmc executable

verify = int(sys.argv[5])

mat = None
matt = None

if r.startswith('0') or r.startswith('1'):
	mat,m = constructMatrix(n,int(r[0])) #matrix and number of edges
	print mat
	print m
elif r.startswith('2'):
	mat, matt, m, n = readMat(0,r[1:])
else:
	print "Could not recognize 2nd option. Exiting.."
	sys.exit()

if not r.startswith('2'):
	mat, m = makeOdd(mat,n,m) #each node must have odd degree for the cutspace reduction to work
	n = n+2
	print mat
	print m
formula = CNFPlus()
vpool = IDPool(start_from=1)

opStr = ''
numCls = 0
numVars = m + n + n

baseStr = ''

inds = []
if r.startswith('2'):
	for i in range(n):
		for j in mat[i]:
			formula.append([-vpool.id(str(i)+' '+str(j)),vpool.id('a'+str(i)),vpool.id('b'+str(j))])
			formula.append([-vpool.id(str(i)+' '+str(j)),-vpool.id('a'+str(i)),-vpool.id('b'+str(j))])
			formula.append([vpool.id(str(i)+' '+str(j)),-vpool.id('a'+str(i)),vpool.id('b'+str(j))])
			formula.append([vpool.id(str(i)+' '+str(j)),vpool.id('a'+str(i)),-vpool.id('b'+str(j))])
			inds = inds + [vpool.id(str(i)+' '+str(j))]
else:
	for i in range(n):
		for j in range(n):
			if mat[i][j] == 1:
				formula.append([-vpool.id(str(i)+' '+str(j)),vpool.id('a'+str(i)),vpool.id('b'+str(j))])
				formula.append([-vpool.id(str(i)+' '+str(j)),-vpool.id('a'+str(i)),-vpool.id('b'+str(j))])
				formula.append([vpool.id(str(i)+' '+str(j)),-vpool.id('a'+str(i)),vpool.id('b'+str(j))])
				formula.append([vpool.id(str(i)+' '+str(j)),vpool.id('a'+str(i)),-vpool.id('b'+str(j))])
				inds = inds + [vpool.id(str(i)+' '+str(j))]
				'''
				baseStr = baseStr + '-'+str(vpool.id(str(i)+' '+str(j))) + ' ' + str(vpool.id(str(i))) + ' ' + str(vpool.id(str(j))) + ' 0\n'
				baseStr = baseStr + '-'+str(vpool.id(str(i)+' '+str(j))) + ' -' + str(vpool.id(str(i))) + ' -' + str(vpool.id(str(j))) + ' 0\n'
				baseStr = baseStr + str(vpool.id(str(i)+' '+str(j))) + ' -' + str(vpool.id(str(i))) + ' ' + str(vpool.id(str(j))) + ' 0\n'
				baseStr = baseStr + str(vpool.id(str(i)+' '+str(j))) + ' ' + str(vpool.id(str(i))) + ' -' + str(vpool.id(str(j))) + ' 0\n'
				numCls = numCls + 1
				'''

print n,m 
outStr = ['1\n']

comment = 'c ind '
for ind in inds:
	comment = comment + str(ind) + ' '
comment = comment + '0'
formula.to_file('temp.cnf',[comment])
fis = open('temp.is','w')
fis.write('v '+ comment[6:])
fis.close()

if path == 0:
	arglist=['/home/adi/Downloads/scalmc/scalmc','--threshold=91','--measure=3', 'temp.cnf']
	process = subprocess.Popen(args=arglist, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out1, err = process.communicate()
	out = out1.splitlines()
	print out[-6:]
elif path == 1:
	arglist = ['/home/adi/Downloads/probsharpsat/probsharpSAT','-is','temp.is','temp.cnf']
	process = subprocess.Popen(args=arglist, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out1, err = process.communicate()
	out = out1.splitlines()
	print out[-6:]
elif path == 3:	
	os.system('/home/adi/Downloads/probsharpsat/probsharpSAT temp.cnf')
elif path == 4:
	os.system('/home/adi/Downloads/probsharpsat/probsharpSAT -is temp.is temp.cnf')
else:
	os.system('/home/adi/Downloads/scalmc/scalmc --threshold=51 --measure=1 temp.cnf')
	
if verify ==1:
	print 'Calculating exact count..'
	writeMatrix(mat,n,'mat.tmp')
	os.system('../Ryser '+str(n)+' mat.tmp')



