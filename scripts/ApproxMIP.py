import sys,os, time, MIPHash, MIPXORHash, random

def cpuTimeTotal():
	return time.time()

startTime = cpuTimeTotal()

def ApproxMC(pivotApproxMC, startIteration, numHashVars, tApproxMC, assumps, cusp_logf):
	count = [0,0]
	currentNumSolutions = 0
	numHashList = []
	numCountList = []
	assumps.initHash(startIteration)   # also adds startIteration number of hashes
	print "Hash initialized"
	for j in range(tApproxMC):
		print "ApproxMC: j="+str(j)+" tApproxMC = "+str(tApproxMC)
		hashCount = startIteration
		#repeatTry = 0
		while hashCount < numHashVars:
			print "-> Hash Count "+str(hashCount)
			myTime = cpuTimeTotal()
			currentNumSolutions = BoundedSATCount(assumps, pivotApproxMC + 1)

			#print str(currentNumSolutions)+", "+str(pivotApproxMC)
			cusp_logf.write("ApproxMC:0:"+
					str(j)+":"+str(hashCount)+":"+
					str(cpuTimeTotal() - myTime)+":"+
					str(currentNumSolutions == (pivotApproxMC + 1))+":"+
					str(currentNumSolutions)+'\n')
			
			if currentNumSolutions < pivotApproxMC + 1:
				#less than pivotApproxMC solutions
				break
			#Found all solutions needed
			assumps.AddHash(1,hashCount)
			hashCount = hashCount + 1
		assumps.initHash(startIteration)
		numHashList.append(hashCount)
		numCountList.append(currentNumSolutions)
	if len(numHashList) == 0:
		#UNSAT
		return count

	minHash = min(numHashList)
	for it in range(len(numHashList)):
		numCountList[it] = numCountList[it]*pow(2, numHashList[it] - minHash)
	
	medSolCount = sorted(numCountList)[len(numCountList)/2]

	count[0] = medSolCount
	count[1] = minHash
	return count
	
def BoundedSATCount(assumps, maxSolutions):
	cellCount = assumps.getNSols(maxSolutions)
	return cellCount

def constructMatrix(n,r):
	mat = [[1]*n for i in range (n)]
	if r == 0:
		return mat,n*n
	else:
		m = n*n
		for i in range(n):
			for j in range(n):
				if random.random()>0.5:
					mat[i][j] = 0
					m = m -1
		return mat,m

def writeMatrix(mat, n, filename):
	fmat = open(filename, 'w')
	for i in range(n):
		for j in range(n):
			if mat[i][j] == 1:
				fmat.write('1 ')
			else:
				fmat.write('0 ')
		fmat.write('\n')
	fmat.close()	

n = int(sys.argv[1])
r = int(sys.argv[2])
pivotApproxMC = int(sys.argv[3])
tApproxMC = int(sys.argv[4])
verify = int(sys.argv[5])

mat,m = constructMatrix(n,r) #matrix and number of edges
print mat
print m
cusp_logf = open('c.log','w')

assumps = None
if sys.argv[6]=='P':
	print 'Using PB Solver'
	assumps = MIPHash.PBHash(mat,m,n)
elif sys.argv[6] == 'F':
	print 'Using ILP Solver with no objective'
	assumps = MIPHash.FeasibleHash(mat,m,n)
elif sys.argv[6] == 'O':
	print 'Using ILP Solver with objective'
	assumps = MIPHash.OptimalHash(mat,m,n)
elif sys.argv[6] == 'XI':
	print 'Using ILP Solver with XOR constraints'
	assumps = MIPXORHash.FeasibleHash(mat,m,n)
elif sys.argv[6] == 'XP':
	print 'Using PB Solver with XOR constraints'
	assumps = MIPXORHash.PBHash(mat,m,n)
else:
	print 'Using SAT Solver with XOR constraints'
	assumps = MIPXORHash.SATHash(mat,m,n,int(sys.argv[6][2:]))
count = ApproxMC(pivotApproxMC, 1, m, tApproxMC, assumps, cusp_logf)
print 'Approximate count is '+str(count[0])+' x 2^' + str(count[1])
cusp_logf.close()
stopTime = cpuTimeTotal()
print 'Time taken = ',(stopTime - startTime),' seconds'
if verify ==1:
	print 'Calculating exact count..'
	writeMatrix(mat,n,'mat.tmp')
	os.system('../Ryser '+str(n)+' mat.tmp')
#pivotApproxMC = 2*ceil(4.94*(1+(1/epsilon))*(1+(1/epsilon)));	
