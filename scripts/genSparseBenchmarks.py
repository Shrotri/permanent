import sys, os, random


if len(sys.argv)!=13:
	print 'USAGE:    python genSparseBenchmarks.py nLow nHi nStep rType rLow rHi rStep instances sparse ensurePM outDir outPrefix'
	print 'rType:    0 for input as a probability of getting zero, rType 1 for input as integer number of zeros, 2 for r*n 1s, 3 for r*n 0s'
	print 'sparse:   1 for output to be a sparse encoding, 1 for dense'
	print 'ensurePM: 1 make sure there is a perfect matching and abort case if not. 0 - dont check for pm (permanent can be 0)'
	sys.exit(1)

nLow = int(sys.argv[1])
nHi = int(sys.argv[2])
nStep = int(sys.argv[3])

rType = int(sys.argv[4])  #0: probability of zero (0.1 means 10 zeros out of 100) 1:number of zeroes 2: r*n 1's 3: r*n 0's
rLow = sys.argv[5]
rHi = sys.argv[6]
rStep = sys.argv[7]

instances = int(sys.argv[8])
sparse = int(sys.argv[9]) #1 - sparse 0 - dense
ensurePM = int(sys.argv[10]) #1 make sure there is a perfect matching and abort case if not. 0 - dont check for pm (permanent can be 0)
outDir = sys.argv[11]
outPrefix = sys.argv[12]

nameF = open(outDir+'/'+outPrefix+'_names.txt','w')  #meta file that holds list of all file names for easy of access on slurm/bash

for n in range(nLow,nHi,nStep):
	pLow = 0  # we only operate on number of zeros in the matrix. probabilities are first converted to number of zeros
	pHi = 0
	pStep = 0
	if rType == 0:
		pLow = int(float(rLow)*n*n)
		pHi = int(float(rHi)*n*n)
		pStep = int(float(rStep)*n*n)
	elif rType == 1:
		pLow = int(rLow)
		pHi = int(rHi)
		pStep = int(rStep)
	elif rType == 2:
		pLow = int(n*n-float(rHi)*n)
		pHi = int(n*n-float(rLow)*n)
		pStep = int(float(rStep)*n)
	elif rType == 3:
		pLow = int(float(rLow)*n)
		pHi = int(float(rHi)*n)
		pStep = int(float(rStep)*n)
	for p in range(pLow,pHi,pStep):
		if ensurePM == 1:
			if n*n-p < n:
				continue
		for ins in range(instances):
			l = []
			if ensurePM == 0:
				l = [0]*p + [1]*(n*n-p)
				random.shuffle(l)
			else:
				l = [0]*p + [1]*(n*n-p-n)
				random.shuffle(l)
				for dec in range(n):
					l.insert(dec + n*dec,1)

			of = open(outDir+'/'+outPrefix+'_'+str(n)+'_'+str(p)+'_'+str(ins)+'.mat','w')
			of.write('%'+' '+str(n)+ ' '+ str(n)+'\n')				
			if sparse == 1:
				for i in range(n):
					for j in range(n):
						if l[i*n+j] == 1:
							of.write(str(j)+' ')
					of.write('\n')
				of.close()
			else:
				for i in range(n):
					for j in range(n):
						if l[i*n+j] == 1:
							of.write('1 ')
						else:
							of.write('0 ')
					of.write('\n')
				of.close()
			nameF.write("\""+str(n)+'_'+str(p)+'_'+str(ins)+"\" ")
nameF.close()






