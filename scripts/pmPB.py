import sys,os, math
from datetime import datetime

print datetime.now()
if len(sys.argv) != 4:
	print 'USAGE: python pmPB.py n sat sol     where n is size of matrix, sat is bool (y/n) for sat/unsat, sol is 1/2/3/4SolverName for cdcl-cuttingplanes, cutsat, Gurobi, sat4j invoked with SolverName respectively'
	sys.exit(1)
	
n = int(sys.argv[1])
sat = sys.argv[2] in ['sat','SAT','Sat','True', 'T', 'Y', 'Yes', 'true', '1', 't', 'y', 'yes', 'yeah', 'yup', 'certainly', 'uh-huh']
sol = sys.argv[3]

path = ''
if sol=='1':
	path = '~/Downloads/cdcl-cutting_planes/src/roundingsat_compiled_with_O3 '
elif sol=='2':
	path = '~/Downloads/cutsat/cutsat '
elif sol == '3':
	path = '~/Downloads/gurobi/gurobi801/linux64/bin/gurobi_cl '
elif sol.startswith('4'):
	path = 'java -jar /home/adi/Downloads/sat4j/dist/CUSTOM/sat4j-pb.jar '+sol[1:]+' '
elif sol=='5':
	path = '~/Downloads/cdcl-cutting_planes/src/latest/roundingsat_latest_compiled_with_O3 '
numVars = n*n
numCons = 0

opStr = ''

#cutsat cannot work with = constraints; it needs <= and >=
#cdcl-cp can work with <= >= , but I think I recall from talk that it can handle = more efficiently. So keeping different encoding for both solvers

for i in range(0,n):
	if sol != '1':
		for j in range(1,n+1):
			opStr += '+1 x'+str(n*i+j)+' '
		opStr += '>= 1 ;\n'
		numCons = numCons + 1
		for j in range(1,n+1):
			opStr += '-1 x'+str(n*i+j)+' '
		opStr += '>= -1 ;\n'
		numCons = numCons + 1
	else:
		for j in range(1,n+1):
			opStr += '+1 x'+str(n*i+j)+' '
		opStr += '= 1 ;\n'
		numCons = numCons + 1

for j in range(1,n+1):
	if sol != '1':	
		for i in range(0,n):
			opStr += '+1 x'+str(n*i+j)+' '
		opStr += '>= 1 ;\n'
		numCons = numCons + 1
		for i in range(0,n):
			opStr += '-1 x'+str(n*i+j)+' '
		opStr += '>= -1 ;\n'
		numCons = numCons + 1
	else:
		for i in range(0,n):
			opStr += '+1 x'+str(n*i+j)+' '
		opStr += '= 1 ;\n'
		numCons = numCons + 1


add = 0
if not sat:
	add = 1
for i in range(int(math.ceil(n/2.0)-add),n):
	if sol != '1':
		for j in range(n/2+1,n+1):
			opStr += '+1 x'+str(n*i+j)+' >= 0 ;\n'
			numCons = numCons + 1
		for j in range(n/2+1,n+1):
			opStr += '-1 x'+str(n*i+j)+' >= 0 ;\n'
			numCons = numCons + 1
	else:
		for j in range(n/2+1,n+1):
			opStr += '+1 x'+str(n*i+j)+' = 0 ;\n'
			numCons = numCons + 1


opStr = '* #variable= '+str(numVars)+' #constraint= '+str(numCons)+'\n' + opStr
		
f = open('temp.opb','w')

f.write(opStr)
f.close()

os.system(path+'temp.opb')
print datetime.now()
