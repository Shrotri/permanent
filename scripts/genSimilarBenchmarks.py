import sys, os, random


if len(sys.argv)!=15:
	print 'USAGE:    python genSimilarBenchmarks.py nLow nHi nStep flipLow flipHi flipStep rowLow rowHi rowStep instances sparse ensurePM outDir outPrefix'
	print 'row:      row generated with row*n 1s. Same row repeated, then flip applied to matrix'
	print 'flip:     flip*n flips in matrix  '
	print 'sparse:   1 for output to be a sparse encoding, 1 for dense'
	print 'ensurePM: 1 make sure there is a perfect matching and abort case if not. 0 - dont check for pm (permanent can be 0)'
	sys.exit(1)

nLow = int(sys.argv[1])
nHi = int(sys.argv[2])
nStep = int(sys.argv[3])

flipLow = float(sys.argv[4])
flipHi = float(sys.argv[5])
flipStep = float(sys.argv[6])

rowLow = float(sys.argv[7])
rowHi = float(sys.argv[8])
rowStep = float(sys.argv[9])

instances = int(sys.argv[10])
sparse = int(sys.argv[11]) #1 - sparse 0 - dense
ensurePM = int(sys.argv[12]) #1 make sure there is a perfect matching and abort case if not. 0 - dont check for pm (permanent can be 0)
outDir = sys.argv[13]
outPrefix = sys.argv[14]

nameF = open(outDir+'/'+outPrefix+'_names.txt','w')  #meta file that holds list of all file names for easy of access on slurm/bash

for n in range(nLow,nHi,nStep):
	rLow = int(rowLow*n)
	rHi = int(rowHi*n)
	rStep = int(rowStep*n)
	for r in range(rLow,rHi,rStep):
		fLow = int(flipLow*n)
		fHi = int(flipHi*n)
		fStep = int(flipStep*n)
		for f in range(fLow,fHi,fStep):
			if ensurePM == 1:
				if f < n-r:
					continue
			for ins in range(instances):
				lRow = [1]*r+[0]*(n-r)
				lMat = [list(lRow) for i in range(n)]
				if ensurePM == 0:
					fIndices = random.sample(xrange(n*n),f)
					for i in range(n*n):
						if i in fIndices:
							lMat[i/n][i%n] = 1 - lMat[i/n][i%n]
				else:
					fIndices = random.sample(xrange(n*n-n),f-(n-r))
					for i in range(len(fIndices)):
						for j in range(n):
							fIndex = fIndices[i]
							ir = fIndex/n
							ic = fIndex%n
							if ir>= j and ic >= j:
								fIndices[i] += 1 
							else:
								break
					for i in range(r,n):
						if lMat[i][i] == 1:
							print 'Erroneously flipped diagonal bit that was supposed to be already flipped to ensurePM. Exiting..'
							sys.exit(1)
						lMat[i][i] = 1
					for i in range(n*n):
						if i in fIndices:
							lMat[i/n][i%n] = 1 - lMat[i/n][i%n]

				of = open(outDir+'/'+outPrefix+'_'+str(n)+'_'+str(r)+'_'+str(f)+'_'+str(ins)+'.mat','w')
				of.write('%'+' '+str(n)+ ' '+ str(n)+'\n')				
				if sparse == 1:
					for i in range(n):
						for j in range(n):
							if lMat[i][j] == 1:
								of.write(str(j)+' ')
						of.write('\n')
					of.close()
				else:
					for i in range(n):
						for j in range(n):
							if lMat[i][j] == 1:
								of.write('1 ')
							else:
								of.write('0 ')
						of.write('\n')
					of.close()
				nameF.write("\""+str(n)+'_'+str(r)+'_'+str(f)+'_'+str(ins)+"\" ")
nameF.close()






