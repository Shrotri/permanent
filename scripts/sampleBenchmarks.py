import sys, os, random, math, heapq
import subprocess, shlex
import copy

nLow = int(sys.argv[1])
nHigh =  int(sys.argv[2])
nStep = int(sys.argv[3])
	
instances = int(sys.argv[4])
inFile = sys.argv[5]
outFilePrefix = sys.argv[6]
outputDir = sys.argv[7]
timeout = int(sys.argv[8])

mat = [] #init empty. Vertices are stored 0 to n-1. Input files have vertex nos 1 to n.
         # So we subtract 1 from index while storing

su = 0 #size of left vertex set
sv = 0 #size of right vertex set
ne = 0 #number of edges

def writeMat(of_, matr, n_, m_):
	of_.write('%'+' '+str(n_)+ ' '+ str(m_)+'\n')				
	for i in range(n_):
		for j in matr[i]:
			of_.write(str(j)+' ')
		of_.write('\n')
	of_.close()

#https://stackoverflow.com/questions/352670/weighted-random-selection-with-and-without-replacement
def WeightedSelectionWithoutReplacement(weights, m):
    elt = [(math.log(random.random()) / weights[i], i) for i in range(len(weights))]
    return [x[1] for x in heapq.nlargest(m, elt)]

print 'Reading input file..'
weights = []
f = open(inFile,'r')
i = 0
for line in f:
	if i==0:
		i += 1
		continue
	l = line.split()
	if i==1:
		ne = int(l[1])
		su = int(l[2])
		sv = int(l[3])
		mat += [[] for i in range(su)]
		weights = [0 for i in range(su)]
		i += 1
		continue
	s = int(l[0])
	t = int(l[1])
	mat[s-1].append(t-1)   #adjust for 0 indexed storing
	weights[s-1] += 1
	i += 1
#mat.sort(key=lambda x: len(x))
#matt.sort(key=lambda x: len(x[1]))

curmat = copy.deepcopy(mat)

#write to file
print "Writing temp mat to file.."
of = open('temp_mat.txt','w')
writeMat(of,curmat,su, sv)

#run hkarp
print 'Running hopcroft karp..'
arglist = ['/home/adi/Downloads/permanent/Gitlab/HopcroftKarp',str(su),str(sv),'btemp_mat.txt','n']
process = subprocess.Popen(args=arglist, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
out1, err = process.communicate()
print out1
out = out1.splitlines()

#get matching
match = [int(x) for x in out[-6].split(',') if x!='']
msize = int(out[7].split()[-1])

#get weights of matched vertices
matched_weights = [weights[i] for i in range(su) if match[i]!=-1]
#print 'matching is ',match

print 'Generating samples..'
for n in range(nLow,nHigh,nStep):
	print 'N = ',n
	for ins in range(instances):
		print 'Doing instance ',ins,' out of ',instances
		#randomly sample submatching of size n in nlow to nhi
		#sample = random.sample(xrange(msize),n)
		sample = WeightedSelectionWithoutReplacement(matched_weights,n)
		sample.sort()
		print 'Sample is ',sample

		#compute induced subgraph
		lnodes = []
		rnodes = []

		#-1,-1,3,4,2,-1,-1,5
		#[0,1,3]
		li = 0
		si = 0
		mi= -1
		while si<n:
			while mi<sample[si]:
				if match[li] != -1:
					mi += 1
				li += 1
			lnodes.append(li-1)
			rnodes.append(match[li-1])	
			si += 1
		print 'lnodes are ',lnodes
		print 'rnodes are ',rnodes

		print 'Printing and writing sampled subgraph..'
		res = []
		for i in range(n):
			res.append([])
			for j in range(n):
				if rnodes[j] in mat[lnodes[i]]:
					res[i].append(j)
					#print '1 ',
				else:
					#print '0 ',
			#print ''
		of = open(outputDir+'/'+outFilePrefix+'_'+str(n)+'_'+str(ins)+'.txt','w')
		writeMat(of,res,n,n)
		
		#test subgraph for not easy count
		print 'Testing count on probsharpsat..'
		os.system('python countPM.py 1 '+str(n)+' 0'+outputDir+'/'+outFilePrefix+'_'+str(n)+'_'+str(ins)+'.txt 4 0 5 '+str(timeout))
#write subgraph to file along with rows and cols from original file used
#permute the original matrix (with hope that running hkarp will give very different matching)