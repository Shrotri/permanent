#include "HopcroftKarp.h"
#include "MatrixParser.h"
#include "MatrixUtils.h"

int main(int argc, char* argv[]){
	cout<< "Starting HopcroftKarp at ";
	print_wall_time();
	cout<<endl;
	
	if (!(argc==5)){
		cout<<"Usage: HopcroftKarp <n> <m> <s/u/r/f/a'filename'> <n/w'filename'>"<<endl<<"where n is numRows m is numCols. s if perfect matching should be present u otherwise r for random matrix f for full or 'a'+'filename' to read from file (a concatenated with filename and lastly w+filename to write matrix to filename or n to not write"<<endl;
		exit(1);
	}
	uint32_t n = stoi(argv[1],NULL);
	uint32_t m = stoi(argv[2],NULL);
	std::string f = std::string(argv[3]);
	std::string f2 = std::string(argv[4]);
	
	cout<<"HopcroftKarp invoked with n = "<<n<<" sat / unsat = " <<f<<" write / dont write = "<<f2<<endl<<endl;
	HopcroftKarp *HK;
	MatrixParser mp;
	uint32_t nedges;
	vector<vector<uint32_t>> mt; 
	if(f[0]=='s'){
		//HK = new HopcroftKarp(n,m,true);
		mp.genJustSAT(n,m);
		mt = mp.getSparse(n,m,nedges);
		HK = new HopcroftKarp(n,m,mt);
	}
	else if (f[0] == 'u'){
		//HK = new HopcroftKarp(n,m,false);
		mp.genJustUNSAT(n,m);
		mt = mp.getSparse(n,m,nedges);
		HK = new HopcroftKarp(n,m,mt);
	}
	else if (f[0] == 'r'){
		//HK = new HopcroftKarp(n,m,false);
		mp.genRandom(n,m,stod(f.substr(1),NULL));
		mt = mp.getSparse(n,m,nedges);		
		HK = new HopcroftKarp(n,m,mt);
	}
	else if (f[0] == 'f') {
		//HK = new HopcroftKarp(n,m,false);
		mp.genFull(n,m);
		mt = mp.getSparse(n,m,nedges);
		HK = new HopcroftKarp(n,m,mt);
	}
	else if (f[0] == 'a'){
		mp.readDense(f.substr(1));
		mt = mp.getSparse(n,m,nedges);
		HK = new HopcroftKarp(n,m,mt);
	}
	else if (f[0] == 'b'){
		mp.readSparse(f.substr(1));
		mt = mp.getSparse(n,m,nedges);
		HK = new HopcroftKarp(n,m,mt);
	}
	else if (f[0] == 'k'){
		mp.readKonect(f.substr(1));
		mt = mp.getSparse(n,m,nedges);
		HK = new HopcroftKarp(n,m,mt);
	}
	else{
		cout<<"Could not recognize 2nd argument. Exiting.."<<endl;
		exit(1);
	}
	//MatrixUtils::printMat(mt);
	
	if (f2[0] == 'w'){
		MatrixUtils::writeDense(mt,m,f2.substr(1));
	}
	
	int retcode = HK->solve();
	
	cout<<endl<<endl<< "HopcroftKarp ended at ";
	print_wall_time();
	cout<<endl;
	
	return retcode;
}
