#ifndef RYSERSYLVAND_H_
#define RYSERSYLVAND_H_
 
#include <cassert>
#include <vector>
#include <iostream>
#include <gmp.h>
#include <unordered_set>

#include <sylvan_obj.hpp>
#include <sylvan.h>
#include <sylvan_gmp.h>

using std::cout;
using std::endl;
using std::stoi;
using std::vector;
using std::string;
using std::unordered_set;

class RyserSylvanD{
	public:
		RyserSylvanD(uint32_t n_, vector<vector<uint32_t>> sMat_, vector<unordered_set<uint32_t>> sMatT_, int32_t ch_, int32_t cvo_, int32_t dvo_, uint32_t memGB_, uint32_t n_workers_): n(n_), sMat(sMat_), sMatT(sMatT_), ch(ch_),cvo(cvo_),dvo(dvo_), memGB(memGB_),n_workers(n_workers_){
		}
		
		void count();
		//void count2();
		//static void printADD(ADD, string, string);
	private:
		uint32_t n;
		vector<vector<uint32_t>> sMat;
		vector<unordered_set<uint32_t>> sMatT; //original and transpose
		//Cudd mgr;
		uint32_t n_workers = 1; // 1 workers
		size_t deque_size = 0; // default value for the size of task deques for the workers
		size_t program_stack_size = 0; // default value for the program stack of each pthread
		uint32_t memGB = 8;
		int32_t ch,cvo,dvo;
};

#endif
