#include "RyserADD.h"
#include "MatrixUtils.h"
#include "RandomBits.h"

void RyserADD::count(){
	vector<ADD> vars;
	for(uint32_t i = 0; i<n;i++){
		vars.push_back(mgr.addVar());
	}
	MatrixUtils::sortAsc(sMat);
	cout<<"Constructing parity.."<<endl;
	ADD par = mgr.addZero();
	for (uint32_t i =0; i<n;i++){
		par = par.Xor(vars[i]);
	}
	ADD p1 = mgr.addOne();
	ADD m1 = mgr.constant(-1);
	//printADD(m1,"Minus is ","");
	par = par.Ite(m1,p1);
	cout<<"Constructed parity. Constructing prod.."<<endl;
	
	/*int* s = new int[n];
	
	RandomBits rb;
	rb.SeedEngine();
	string bits = rb.GenerateRandomBits(n);
	cout<<"Random assignment is "<<endl;
	for(uint32_t i = 0; i<n;i++){
		if(bits[i]=='0'){
			s[i] = 0;
		}else{
			s[i]= 1;
		}
		cout<<s[i]<<" ";
		
	}
	cout<<endl<<"Parity is ";
	cout<<Cudd_V(par.Eval(s).getNode())<<endl;*/
	ADD cube = mgr.addOne();
	for (uint32_t i =0; i<n;i++){
		cube *= vars[i];
	}
	
	ADD prod = mgr.addOne();
	for (uint32_t i = 0; i<n;i++){
		ADD r = mgr.addZero();
		for (uint32_t j =0; j<sMat[i].size(); j++){
			r += vars[sMat[i][j]];
		}
		prod *= r;
		ADD check = prod;
		ADD res_check = prod.ExistAbstract(cube);
		/*if(n%2 == 1){
			res_check *= m1;
		}*/
		cout<<"Intermediate Count is "<<Cudd_V(res_check.getNode())<<endl;
	}
	
	cout<<"Constructed prod!."<<endl;
	
	prod *= par;
	cout<<"Node count is "<<prod.nodeCount()<<endl;
	cout<<"ApaPrintMinterm is ";
	prod.ApaPrintMintermExp(n);
	cout<<endl<<"CountLeaves is "<<prod.CountLeaves()<<endl;
	cout<<"CountPath is "<<prod.CountPath()<<endl;
	cout<<"Constructed parity+prod. Performing ExistAbstract.."<<endl;
	
	ADD res = prod.ExistAbstract(cube);
	if(n%2 == 1){
		res *= m1;
	}
	cout<<"Count is "<<Cudd_V(res.getNode())<<endl;
}

void writeDd(const Cudd &mgr, const ADD &add, const std::string &filePath) {
  DdManager *gbm = mgr.getManager();
  DdNode *dd = add.getNode();
  const char *filename = filePath.c_str();

  FILE *outfile; /* output file pointer for .dot file*/
  outfile = fopen(filename, "w");
  DdNode **ddnodearray = (DdNode**)malloc(sizeof(DdNode*)); /* initializes function array */
  ddnodearray[0] = dd;
  Cudd_DumpDot(gbm, 1, ddnodearray, NULL, NULL, outfile); /* dumps function to .dot file */
  free(ddnodearray);
  fclose (outfile); /* closes file */
}

void RyserADD::printDOT(){
	vector<ADD> vars;
	for(uint32_t i = 0; i<n;i++){
		vars.push_back(mgr.addVar());
	}
	MatrixUtils::sortAsc(sMat);
	cout<<"Constructing parity.."<<endl;
	ADD par = mgr.addZero();
	for (uint32_t i =0; i<n;i++){
		par = par.Xor(vars[i]);
	}
	ADD p1 = mgr.addOne();
	ADD m1 = mgr.constant(-1);
	//printADD(m1,"Minus is ","");
	par = par.Ite(m1,p1);
	cout<<"Constructed parity. Writing to DOT"<<endl; 
	writeDd(mgr,par,string("parity.dot"));
	cout<<"Constructing prod.."<<endl;
	ADD cube = mgr.addOne();
	for (uint32_t i =0; i<n;i++){
		cube *= vars[i];
	}
	
	ADD prod = mgr.addOne();
	for (uint32_t i = 0; i<n;i++){
		ADD r = mgr.addZero();
		for (uint32_t j =0; j<sMat[i].size(); j++){
			r += vars[sMat[i][j]];
		}
		prod *= r;
		ADD check = prod;
		ADD res_check = prod.ExistAbstract(cube);
		/*if(n%2 == 1){
			res_check *= m1;
		}*/
		//cout<<"Intermediate Count is "<<Cudd_V(res_check.getNode())<<endl;
	}
	
	cout<<"Constructed prod!. Writing to DOT.."<<endl;
	writeDd(mgr,prod,string("prod.dot"));
	prod *= par;
	cout<<"Node count is "<<prod.nodeCount()<<endl;
	cout<<"ApaPrintMinterm is ";
	prod.ApaPrintMintermExp(n);
	cout<<endl<<"CountLeaves is "<<prod.CountLeaves()<<endl;
	cout<<"CountPath is "<<prod.CountPath()<<endl;
	cout<<"Constructed parity+prod. Writing to DOT.."<<endl;
	writeDd(mgr,prod,string("prod_parity.dot"));
	cout<<" Performing ExistAbstract.."<<endl;
	
	ADD res = prod.ExistAbstract(cube);
	if(n%2 == 1){
		res *= m1;
	}
	cout<<"Count is "<<Cudd_V(res.getNode())<<endl;
}

void RyserADD::count2(){
	vector<ADD> vars;
	for(uint32_t i = 0; i<n;i++){
		vars.push_back(mgr.addVar());
	}
	MatrixUtils::sortAsc(sMat);
	cout<<"Constructing parity.."<<endl;
	ADD par = mgr.addZero();
	for (uint32_t i =0; i<n;i++){
		par = par.Xor(vars[i]);
	}
	ADD p1 = mgr.addOne();
	ADD m1 = mgr.constant(-1);
	//printADD(m1,"Minus is ","");
	par = par.Ite(m1,p1);
	cout<<"Constructed parity. Constructing prod.."<<endl;
	
	/*int* s = new int[n];
	
	RandomBits rb;
	rb.SeedEngine();
	string bits = rb.GenerateRandomBits(n);
	cout<<"Random assignment is "<<endl;
	for(uint32_t i = 0; i<n;i++){
		if(bits[i]=='0'){
			s[i] = 0;
		}else{
			s[i]= 1;
		}
		cout<<s[i]<<" ";
		
	}
	cout<<endl<<"Parity is ";
	cout<<Cudd_V(par.Eval(s).getNode())<<endl;*/
	
	ADD prod = mgr.addOne();
	for (uint32_t i = 0; i<n;i++){
		ADD r = mgr.addZero();
		for (uint32_t j =0; j<sMat[i].size(); j++){
			r += vars[sMat[i][j]];
		}
		prod += r.Log();
	}
	cout<<"Constructed prod!."<<endl;
	
	prod *= par;
	cout<<"Node count is "<<prod.nodeCount()<<endl;
	cout<<"ApaPrintMinterm is ";
	prod.ApaPrintMintermExp(n);
	cout<<endl<<"CountLeaves is "<<prod.CountLeaves()<<endl;
	cout<<"CountPath is "<<prod.CountPath()<<endl;
	cout<<"Constructed parity+prod. Performing ExistAbstract.."<<endl;
	/*ADD cube = mgr.addOne();
	for (uint32_t i =0; i<n;i++){
		cube *= vars[i];
	}
	ADD res = prod.ExistAbstract(cube);
	if(n%2 == 1){
		res *= m1;
	}
	cout<<"Count is "<<Cudd_V(res.getNode())<<endl;*/
	
}

void RyserADD::printADD(ADD x,string preamble, string postamble){
	cout<<preamble<<Cudd_V(x.getNode())<<postamble<<endl;
}

int main(int argc, char* argv[]){
	cout<< "Starting RyserADD at ";
	print_wall_time();
	cout<<endl;
	
	if (!(argc==4)){
		cout<<"Usage: RyserADD <n> <logCount> <s/u/r/f/a'filename'/k'filename'/t/d>"<<endl<<"where n is size of mat, logCount is 1 if converting counts into logarithms 0 no conversion 2 if want to print to DOT files,  s if perfect matching should be present u otherwise r for random matrix f for full or 'a'+'filename' to read from file (a concatenated with filename) 'k'+filename to read from konect file, t for upper triangular d for diag+super-diagonal matrix"<<endl;
		exit(1);
	}
	uint32_t n = stoi(argv[1],NULL);
	uint32_t lc = stoi(argv[2],NULL);
	std::string f = std::string(argv[3]);
	
	cout<<"RyserADD invoked with n = "<<n<<" sat / unsat = " <<f<<endl<<endl;
	RyserADD *HK;
	MatrixParser mp;
	uint32_t nedges;
	vector<vector<uint32_t>> mt;
	uint32_t m = n; 
	if(f[0]=='s'){
		//HK = new HopcroftKarp(n,m,true);
		mp.genJustSAT(n,m);
		mt = mp.getSparse(n,m,nedges);
		HK = new RyserADD(n,mt);
	}
	else if (f[0] == 'u'){
		//HK = new HopcroftKarp(n,m,false);
		mp.genJustUNSAT(n,m);
		mt = mp.getSparse(n,m,nedges);
		HK = new RyserADD(n,mt);
	}
	else if (f[0] == 'r'){
		//HK = new HopcroftKarp(n,m,false);
		mp.genRandom(n,m,stod(f.substr(1),NULL));
		mt = mp.getSparse(n,m,nedges);		
		HK = new RyserADD(n,mt);
	}
	else if (f[0] == 'f') {
		//HK = new HopcroftKarp(n,m,false);
		mp.genFull(n,m);
		mt = mp.getSparse(n,m,nedges);
		HK = new RyserADD(n,mt);
	}
	else if (f[0] == 'a'){
		mp.readDense(f.substr(1));
		mt = mp.getSparse(n,m,nedges);
		HK = new RyserADD(n,mt);
	}
	else if (f[0] == 'b'){
		mp.readSparse(f.substr(1));
		mt = mp.getSparse(n,m,nedges);
		HK = new RyserADD(n,mt);
	}
	else if (f[0] == 'k'){
		mp.readKonect(f.substr(1));
		cout<<"Read graph"<<endl;
		mp.balanceFull();
		uint32_t dummy1, dummy2;
		//mt = mp.getSparse(dummy1,dummy2,nedges);
		mt = mp.getSparse(n,m,nedges);
		//MatrixUtils::trim(mt,dummy1,dummy2,n);
		//MatrixUtils::printMat(mt);
		HK = new RyserADD(n,mt);
	}
	else{
		cout<<"Could not recognize 2nd argument. Exiting.."<<endl;
		exit(1);
	}
	if (lc==1)HK->count2();
	else if (lc==0) HK->count();
	else HK->printDOT();
	cout<<endl<<endl<< "RyserADD ended at ";
	print_wall_time();
	cout<<endl;
	
	return 0;
}

void test(){
	int s[][3] = {{0,0,0},{0,0,1},{0,1,0},{0,1,1},{1,0,0},{1,0,1},{1,1,0},{1,1,1}};
	Cudd mgr(0,0);
	ADD x11 = mgr.addVar();
	
	ADD x12 = mgr.addVar();
	ADD x13 = mgr.addVar();  
	ADD r1 = mgr.addZero();
	r1 = r1 + x11 + x12 + x13;
	cout<<Cudd_V(r1.Eval(s[0]).getNode())<<" "<<Cudd_V(r1.Eval(s[1]).getNode())<<" "<<Cudd_V(r1.Eval(s[2]).getNode())<<" "<<Cudd_V(r1.Eval(s[3]).getNode())<<" "<<Cudd_V(r1.Eval(s[4]).getNode())<<" "<<Cudd_V(r1.Eval(s[5]).getNode())<<" "<<Cudd_V(r1.Eval(s[6]).getNode())<<" "<<Cudd_V(r1.Eval(s[7]).getNode())<<" "<<endl;
	ADD r2 = x11 + x13;
	ADD r3 = x11 + x12;
	
	ADD p = r1 * r2 * r3;
	
	cout<<Cudd_V(p.Eval(s[0]).getNode())<<" "<<Cudd_V(p.Eval(s[1]).getNode())<<" "<<Cudd_V(p.Eval(s[2]).getNode())<<" "<<Cudd_V(p.Eval(s[3]).getNode())<<" "<<Cudd_V(p.Eval(s[4]).getNode())<<" "<<Cudd_V(p.Eval(s[5]).getNode())<<" "<<Cudd_V(p.Eval(s[6]).getNode())<<" "<<Cudd_V(p.Eval(s[7]).getNode())<<" "<<endl; 
	ADD cube = x11*x12*x13;
	ADD res = p.ExistAbstract(cube);
}
