/* adapted from https://github.com/foreverbell/acm-icpc-cheat-sheet/blob/master/src/graph-algorithm/hopcroft-karp.cpp
 */
#ifndef HOPCROFTKARP_H_
#define HOPCROFTKARP_H_
 
#include <iostream>
#include <cassert>
#include <vector>
#include <unordered_set>

#include "RandomBits.h"
#include "MatrixParser.h"
#include "Timers.h"

using std::cout;
using std::endl;
using std::vector;
using std::string;
using std::unordered_set;

class HopcroftKarp{
	public:
		HopcroftKarp(uint32_t n, uint32_t m, vector<vector<uint32_t>>& edge_): maxN(n), maxM(m), level(n), vis(n), ml(n,-1), mr(m,-1), edge(edge_){
			cout<<"HopcroftKarp initialized"<<endl;
			cout<<"HopcroftKarp assumes vertices are numbered from 0 to n-1 and 0 to m-1"<<endl;
		}
		
		void add(uint32_t u, uint32_t v);
		uint32_t matching(uint32_t n);
		vector<int32_t> matching(uint32_t n, unordered_set<uint32_t>& sl,unordered_set<uint32_t>& sr);
		uint32_t matching(uint32_t n, unordered_set<uint32_t>& sl,unordered_set<uint32_t>& sr, uint32_t dummy);
		uint32_t solve();
		void vertexCover();
		void dfsVC(uint32_t);
				
	private:
		bool dfs(uint32_t u);
		bool dfs(uint32_t u, unordered_set<uint32_t>& sl,unordered_set<uint32_t>& sr);
		const uint32_t maxN, maxM;
		vector<bool> vis;
		vector<int32_t> level, ml, mr;
		vector<vector<uint32_t>> &edge; // constructing edges for left part only
		bool matching_called = false;
		vector<vector<bool>> vc;
};
#endif
