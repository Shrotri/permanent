#include "MatrixParser.h"
#include "MatrixUtils.h"

int main(int argc, char* argv[]){
	if (!(argc==5)){
		cout<<"Usage: MatrixParser <n> <m> <s/u/r/f/a'filename'/b'filename'/c> <n/s/d'filename'>"<<endl<<"where n is numRows m is numCols. s if perfect matching should be present u otherwise r for random matrix f for full or 'a'+'filename' to read from file (a concatenated with filename and lastly s+filename to write sparse matrix to filename d+filename to write dense matrix to filename or n to not write"<<endl;
		exit(1);
	}
	uint32_t n = stoi(argv[1],NULL);
	uint32_t m = stoi(argv[2],NULL);
	std::string f = std::string(argv[3]);
	std::string f2 = std::string(argv[4]);
	
	cout<<"MatrixParser invoked with n = "<<n<<" sat / unsat = " <<f<<endl<<endl;
	MatrixParser hk;
	if(f[0]=='s'){
		//HK = new HopcroftKarp(n,m,true);
		hk.genJustSAT(n,m);
	}
	else if (f[0] == 'u'){
		//HK = new HopcroftKarp(n,m,false);
		hk.genJustUNSAT(n,m);
	}
	else if (f[0] == 'r'){
		//HK = new HopcroftKarp(n,m,false);
		hk.genRandom(n,m,stod(f.substr(1),NULL));
	}
	else if (f[0] == 'c'){
		std::string::size_type sz;
		hk.genCorrelatedRows(n,m,stod(f.substr(sz+2),NULL),stod(f.substr(1),&sz));
	}
	else if (f[0] == 'f') {
		//HK = new HopcroftKarp(n,m,false);
		hk.genFull(n,m);
	}
	else if (f[0] == 'a'){
		hk.readDense(f.substr(1));
	}
	else if (f[0] == 'b'){
		hk.readSparse(f.substr(1));
	}
	else{
		cout<<"Could not recognize 2nd argument. Exiting.."<<endl;
		exit(1);
	}
	uint32_t ne = 0;
	vector<vector<bool>> dMat = hk.getDense(n,m,ne);
	MatrixUtils::printMat(dMat);
	if (f2[0] == 'd'){
		MatrixUtils::writeDense(dMat,f2.substr(1));
	}
	else if (f2[0] == 's'){
		MatrixUtils::writeSparse(dMat,f2.substr(1));
	}
}

