#include <sylvan.h>
#include <stdlib.h>

int
main(int argc, char** argv){
	// Init Lace
	lace_init(1, 0); // auto-detect number of workers, use a 1,000,000 size task queue
	lace_startup(0, NULL, NULL); // auto-detect program stack, do not use a callback for startup

	// Lace is initialized, now set local variables
	LACE_ME;

	// Init Sylvan
	// Nodes table size of 1LL<<20 is 1048576 entries
	// Cache size of 1LL<<18 is 262144 entries
	// Nodes table size: 24 bytes * nodes
	// Cache table size: 36 bytes * cache entries
	// With 2^20 nodes and 2^18 cache entries, that's 33 MB
	// With 2^24 nodes and 2^22 cache entries, that's 528 MB
	sylvan_set_sizes(1LL<<20, 1LL<<24, 1LL<<18, 1LL<<22);
	sylvan_init_package();
	sylvan_set_granularity(3); // granularity 3 is decent value for this small problem - 1 means "use cache for every operation"
	sylvan_init_mtbdd();
	
	int n = atoi(argv[1]);
	MTBDD* vars=(MTBDD*)malloc(sizeof(MTBDD)*n);
	
	MTBDD m = mtbdd_double(-1.0);
	MTBDD o = mtbdd_double(1.0);
	MTBDD z = mtbdd_double(0.0);
	
	//MTBDD cube;
	MTBDD sum = mtbdd_double(0.0);
	MTBDD prod = mtbdd_double(1.0);
	mtbdd_protect(&prod);
	for (int i = 0; i<n;i++){
		MTBDD v1 = mtbdd_ithvar(i);
		printf("%lu ",v1);
		MTBDD r = mtbdd_ite(v1,o,z);
		sum = mtbdd_plus(sum,r);
		vars[i] = v1;
		//cube = mtbdd_times(cube,v1);
		mtbdd_protect(&r);
	}
	printf("\n");
	mtbdd_protect(&sum);
	for (int i = 0; i<n;i++){
		prod = mtbdd_times(prod,sum);
	}
	MTBDD par = mtbdd_false;
	mtbdd_protect(&par);
	for (int i = 0 ; i<n; i++){
		par = mtbdd_plus(mtbdd_times(par,mtbdd_comp(vars[i])),mtbdd_times(mtbdd_comp(par),vars[i]));
	}
	
	par = mtbdd_ite(par,m,o);
	MTBDD mx = mtbdd_maximum(prod);
	MTBDD mn = mtbdd_minimum(prod);
	printf("Maximum leaf is %f minimum is %f \n",mtbdd_getdouble(mx),mtbdd_getdouble(mn));
	
	printf("Original vars are .. \n");
	MTBDD temp = mtbdd_true;
	for (int i = 0; i<n;i++){
		temp = mtbdd_times(temp,vars[i]);
	}
	MTBDD tempsup = mtbdd_support(temp);
	MTBDD tpsup = tempsup;
	while(1){
		printf("%lu ",tpsup);
		tpsup = mtbdd_gethigh(tpsup);
		if (mtbdd_isleaf(tpsup)) break;
	}
	printf("\nSupport vars are..\n");
	
	MTBDD sup = mtbdd_support(prod);
	MTBDD top = sup;
	while(1){
		printf("%lu ",top);
		top = mtbdd_gethigh(top);
		if (mtbdd_isleaf(top)) break;
	}
	printf("\n");
	prod = mtbdd_times(prod,par);
	MTBDD rs = mtbdd_abstract_plus(prod, sup);
	printf("Total wt is %f \n",mtbdd_getdouble(rs));
	sylvan_quit();
	lace_exit();
}
