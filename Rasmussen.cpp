#include <unordered_set>
#include <algorithm>
#include <random>
#include "Rasmussen.h"
#include <set>

using std::unordered_set;
using std::sort;
using std::uniform_int_distribution;
using std::set_difference;
using std::inserter;
using std::set;
 

void Rasmussen::estimate(uint32_t N){
	cout<<"Estimating.."<<endl;
	mpz_set_ui(total,0);
	mpz_t sampleTotal;
	mpz_init(sampleTotal);
	uint32_t zeros= 0;
	for(uint32_t i = 0; i<N ; i++){
		set<uint32_t> visited;
		mpz_set_ui(sampleTotal,1);
		for(uint32_t j = 0; j<n; j++){
			vector<uint32_t> diff;
			std::set_difference(sparseMat[j].begin(), sparseMat[j].end(), visited.begin(), visited.end(), std::inserter(diff, diff.begin()));
			
			if (diff.size()==0){
				//cout<<"diff is 0, i is "<<i<<" j is "<<j<<endl;
				mpz_set_ui(sampleTotal,0);
				zeros++;
				break;
			}
			else{
				mpz_mul_ui(sampleTotal,sampleTotal,diff.size());
			}
			uniform_int_distribution<uint64_t> uid (0,diff.size()-1);
			uint64_t choice = rb.getRandInt(uid);
			//cout<<"Diff size "<<diff.size()<<" diff choice "<<diff[choice]<<endl;
			visited.insert(diff[choice]);
		}
		mpz_add(total,total,sampleTotal);
	}
	mpz_fdiv_q_ui(total,total,N);
	cout<<"Estimated! No. of samples with zero value were "<<zeros<<endl;	
}

void Rasmussen::estimateWithElim(uint32_t N){
	cout<<"Estimating.."<<endl;
	mpz_set_ui(total,0);
	mpz_t sampleTotal;
	mpz_init(sampleTotal);
	uint32_t zeros= 0;
	for(uint32_t i = 0; i<N ; i++){
		set<uint32_t> visited;
		mpz_set_ui(sampleTotal,1);
		sl.clear();
		sr.clear();
		for(uint32_t j = 0; j<n; j++){
			vector<uint32_t> diff;
			std::set_difference(sparseMat[j].begin(), sparseMat[j].end(), visited.begin(), visited.end(), std::inserter(diff, diff.begin()));
			sl.insert(j);
			for (uint32_t k = 0; k< diff.size();){
				sr.insert(diff[k]);
				/*cout<<"Sl"<<endl;
				for(auto s:sl){
					cout<<s<<" ";
				}
				cout<<endl;
				cout<<"Sr"<<endl;
				for(auto s:sr){
					cout<<s<<" ";
				}
				cout<<endl;*/
				uint32_t sz = hk.matching(n,sl,sr,0);
				if (sz<n-j-1){
					//cout<<"k="<<k<<" diff[k]="<<diff[k]<<" sz="<<sz<<" n="<<n<<" j="<<j<<" i="<<i<<endl;
					sr.erase(diff[k]);
					diff.erase(diff.begin()+k);
					assert(diff.size()>0);
				}
				else{
					//cout<<"k="<<k<<" Not deleted diff[k]="<<diff[k]<<" sz="<<sz<<" n="<<n<<" j="<<j<<" i="<<i<<endl;
					sr.erase(diff[k]);
					k++;
				}
				
			}
			if (diff.size()==0){
				//cout<<"diff is 0, i is "<<i<<" j is "<<j<<endl;
				mpz_set_ui(sampleTotal,0);
				zeros++;
				break;
			}
			else{
				mpz_mul_ui(sampleTotal,sampleTotal,diff.size());
			}
			uniform_int_distribution<uint64_t> uid (0,diff.size()-1);
			uint64_t choice = rb.getRandInt(uid);
			//cout<<"Diff size "<<diff.size()<<" diff choice "<<diff[choice]<<endl;
			visited.insert(diff[choice]);
			sr.insert(diff[choice]);
		}
		mpz_add(total,total,sampleTotal);
	}
	mpz_fdiv_q_ui(total,total,N);
	cout<<"Estimated! No. of samples with zero value were "<<zeros<<endl;	
}

void Rasmussen::printEstimate(){
	gmp_printf("Estimate is : %Zd\n",total);
}

int main(int argc, char* argv[]){
	cout<< "Starting Rasmussen at ";
	print_wall_time();
	cout<<endl;
	
	if (!(argc==4)){
		cout<<"Usage: Rasmussen <n> <s/u/r/f/a'filename'/t/d> <N>"<<endl<<"where n is size of mat, s if perfect matching should be present u otherwise r for random matrix f for full or 'a'+'filename' to read from file (a concatenated with filename t for upper triangular d for diag+super-diagonal matrix N samples"<<endl;
		exit(1);
	}
	uint32_t n = stoi(argv[1],NULL);
	std::string f = std::string(argv[2]);
	uint32_t N = stoi(argv[3],NULL);	
	
	cout<<"Rasmussen invoked with n = "<<n<<" sat / unsat = " <<f<<endl<<endl;
	Rasmussen *HK;
	MatrixParser mp;
	uint32_t nedges;
	vector<vector<uint32_t>> mt;
	uint32_t m = n; 
	if(f[0]=='s'){
		//HK = new HopcroftKarp(n,m,true);
		mp.genJustSAT(n,m);
		mt = mp.getSparse(n,m,nedges);
		HK = new Rasmussen(n,mt);
	}
	else if (f[0] == 'u'){
		//HK = new HopcroftKarp(n,m,false);
		mp.genJustUNSAT(n,m);
		mt = mp.getSparse(n,m,nedges);
		HK = new Rasmussen(n,mt);
	}
	else if (f[0] == 'r'){
		//HK = new HopcroftKarp(n,m,false);
		mp.genRandom(n,m,stod(f.substr(1),NULL));
		mt = mp.getSparse(n,m,nedges);		
		HK = new Rasmussen(n,mt);
	}
	else if (f[0] == 'f') {
		//HK = new HopcroftKarp(n,m,false);
		mp.genFull(n,m);
		mt = mp.getSparse(n,m,nedges);
		HK = new Rasmussen(n,mt);
	}
	else if (f[0] == 'a'){
		mp.readDense(f.substr(1));
		mt = mp.getSparse(n,m,nedges);
		HK = new Rasmussen(n,mt);
	}
	else{
		cout<<"Could not recognize 2nd argument. Exiting.."<<endl;
		exit(1);
	}
	HK->estimateWithElim(N);
	HK->printEstimate();	
	cout<<endl<<endl<< "Rasmussen ended at ";
	print_wall_time();
	cout<<endl;
	
	return 0;
}
