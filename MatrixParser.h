#ifndef MATRIXPARSER_H_
#define MATRIXPARSER_H_
 
#include <fstream>
#include <sstream>
#include <iostream>
#include <cassert>
#include <vector>

#include "RandomBits.h"

using std::ifstream;
using std::cout;
using std::endl;
using std::stoi;
using std::stringstream;
using std::vector;
using std::string;

class MatrixParser{
	public:
		void readDense(string filename);
		void readSparse(string filename);
		void readKonect(string filename);
		//void readSparse(string filename);
		void genFull(uint32_t n, uint32_t m);
		void genRandom(uint32_t n, uint32_t m, double_t p);
		void genJustSAT(uint32_t n, uint32_t m);
		void genJustUNSAT(uint32_t n, uint32_t m);
		void genCorrelatedRows(uint32_t n, uint32_t m,double_t flipProb, double_t startDensity);
		void printMat();
		void balanceFull();
		
		vector<vector<bool>> getDense(uint32_t &n, uint32_t &m, uint32_t &e);
		vector<vector<uint32_t>> getSparse(uint32_t &n, uint32_t &m, uint32_t &e);
	private:
		vector<vector<bool>> denseMat;
		vector<vector<uint32_t>> sparseMat;
		uint32_t numRows, numCols, numEdges;
};
#endif
