#ifndef RYSERSYLVANB_H_
#define RYSERSYLVANB_H_
 
#include <cassert>
#include <vector>
#include <iostream>
#include <gmp.h>

#include <sylvan_obj.hpp>
#include <sylvan.h>
#include <sylvan_gmp.h>

using std::cout;
using std::endl;
using std::stoi;
using std::vector;
using std::string;

class RyserSylvanB{
	public:
		RyserSylvanB(uint32_t n_, vector<vector<uint32_t>> sMat_): n(n_), sMat(sMat_){
		}
		void count();
		//void count2();
		//static void printADD(ADD, string, string);
	private:
		uint32_t n;
		vector<vector<uint32_t>> sMat;
		//Cudd mgr;
		int n_workers = 1; // 1 workers
		size_t deque_size = 0; // default value for the size of task deques for the workers
		size_t program_stack_size = 0; // default value for the program stack of each pthread
};

#endif
